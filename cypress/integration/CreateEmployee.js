describe('Create employee', () => {
  before(() => {
    cy.managerLogin();
  });
  it('Create new employee', () => {
    cy.get('div[data-testid="employeeBar"]')
      .its('length')
      .then((intialLength) => {
        cy.get('button[data-testid="addEmployeeButton"]').click();
        cy.get('input[name="email"]').type(
          `${Math.floor(Math.random() * 1000000)}@gmail.com`,
        );
        cy.get('input[name="name"]').type('Test User');
        cy.get('button[type="submit"]').click().wait(3000);
        cy.get('p[data-testid="employeeName"]').contains('Test User');
        // cy.get('div[data-testid="employeeBar"]').last().trigger('mouseover');
        // cy.get('svg[data-testid="deleteEmployee"]')
        //   .last()
        //   .click({ force: true });
        // cy.get('button[type="submit"]').click();
        // cy.get('div[data-testid="employeeBar"]').should(
        //   'have.length',
        //   intialLength,
        // );
      });
  });
});
