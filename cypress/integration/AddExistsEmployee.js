import translations from '../../src/locales/translation.json';
const modal = translations.errorsHandler;

describe('Adding a new employee that has already exists', () => {
  before(() => {
    cy.managerLogin();
  });

  it('Add Employee that has already exists', () => {
    const email = Math.floor(Math.random() * 1000000);

    cy.get('div[data-testid="employeeBar"]')
      .its('length')
      .then((intialLength) => {
        cy.get('button[data-testid="addEmployeeButton"]').click();
        cy.get('input[name="email"]').type(`${email}@gmail.com`);
        cy.get('input[name="name"]').type('Exists Employee');
        cy.get('button[type="submit"]').click().wait(3000);
        cy.get('p[data-testid="employeeName"]').contains('Exists Employee');
      });

    cy.get('div[data-testid="employeeBar"]')
      .its('length')
      .then((intialLength) => {
        cy.get('button[data-testid="addEmployeeButton"]').click();
        cy.get('input[name="email"]').type(`${email}@gmail.com`);
        cy.get('input[name="name"]').type('Exists Employee');
        cy.get('button[type="submit"]').click().wait(3000);
        cy.get('p[data-testid="emailError"]').should(
          'contain',
          modal.userExistError.en,
        );
      });
  });
});
