describe('Authorization', () => {
  it('Login employee with credentials', () => {
    cy.visit('/');
    cy.get('input[name="login"]').type(Cypress.env('EMPLOYEE_EMAIL'));
    cy.get('input[name="password"]').type(Cypress.env('EMPLOYEE_PASSWORD'));
    cy.get('button').click();
    cy.url().should('include', '/results');
  });
});
