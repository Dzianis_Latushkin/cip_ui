describe('Authorization removed employee', () => {
  it('Login removed employee with credentials', () => {
    cy.visit('/');
    cy.get('input[name="login"]').type('bipowawo@ryteto.me');
    cy.get('input[name="password"]').type('kAFETi8JGt');
    cy.get('button').click();
    cy.url().should('include', '/results');
  });
});
