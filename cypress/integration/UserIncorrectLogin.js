import translations from '../../src/locales/translation.json';
const errorsHandler = translations.errorsHandler;

describe('Authorization', () => {
  it('Login user with incorrect credentials', () => {
    cy.visit('/', {
      onBeforeLoad: (window) => {
        Object.defineProperty(window.navigator, 'language', { value: 'en-GB' });
      },
    });
    cy.get('input[name="login"]').type('22044025@mail.ru');
    cy.get('input[name="password"]').type('WToreIlNmGCF');
    cy.get('button').click();
    cy.wait(1000)
      .get('p[data-testid="loginError"]')
      .should('contain', errorsHandler.invalidCredoError.en);
  });
});
