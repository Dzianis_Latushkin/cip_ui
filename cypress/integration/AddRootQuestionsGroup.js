import translations from '../../src/locales/translation.json';
const modal = translations.questionsPage.groupModal;

describe('Adding a new root questions group', () => {
  before(() => {
    cy.adminLogin().wait(2000);
  });
  const testName = Math.floor(Math.random() * 1000000);
  it('Add Group', () => {
    cy.visit('/questions');
    cy.get('button[data-testid="add-group"]').click({ force: true });

    cy.get('button[type="submit"]').click({ force: true });
    cy.get('p[data-testid="title-error"]').should(
      'contain',
      modal.titleValidationError.en,
    );

    cy.get('input[name="title"]').type('    ');
    cy.get('button[type="submit"]').click({ force: true });
    cy.get('p[data-testid="title-error"]').should(
      'contain',
      modal.titleSpaceError.en,
    );
    cy.get('button').contains('Cancel').click({ force: true });

    cy.get('button[data-testid="add-group"]').click({ force: true });
    cy.get('input[name="title"]').type(`${testName}`);
    cy.get('button[type="submit"]').click({ force: true });
    cy.get('div').contains(`${testName}`);
  });
});
