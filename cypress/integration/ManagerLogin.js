describe('Authorization', () => {
  it('Login manager with credentials', () => {
    cy.visit('/');
    cy.get('input[name="login"]').type(Cypress.env('MANAGER_EMAIL'));
    cy.get('input[name="password"]').type(Cypress.env('MANAGER_PASSWORD'));
    cy.get('button').click();
    cy.url().should('include', '/employees');
  });
});
