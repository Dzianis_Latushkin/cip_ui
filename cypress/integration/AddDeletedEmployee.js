import translations from '../../src/locales/translation.json';
const modal = translations.errorsHandler;

describe('Adding a new employee that has already been deleted', () => {
  before(() => {
    cy.managerLogin();
  });

  it('Add Employee that has already been deleted', () => {
    const email = Math.floor(Math.random() * 1000000);

    cy.get('div[data-testid="employeeBar"]')
      .its('length')
      .then((intialLength) => {
        cy.get('button[data-testid="addEmployeeButton"]').click();
        cy.get('input[name="email"]').type(`${email}@gmail.com`);
        cy.get('input[name="name"]').type('Deleted Employee');
        cy.get('button[type="submit"]').click().wait(3000);
        cy.get('p[data-testid="employeeName"]').contains('Deleted Employee');
      });

    cy.get('div[data-testid="employeeBar"]').last().trigger('mouseover');
    cy.get('svg[data-testid="deleteEmployee"]').last().click({ force: true });
    cy.get('button[type="submit"]').click({ force: true });

    cy.get('div[data-testid="employeeBar"]')
      .its('length')
      .then((intialLength) => {
        cy.get('button[data-testid="addEmployeeButton"]').click();
        cy.get('input[name="email"]').type(`${email}@gmail.com`);
        cy.get('input[name="name"]').type('Deleted Employee');
        cy.get('button[type="submit"]').click().wait(3000);
        cy.get('p[data-testid="emailError"]').should(
          'contain',
          modal.userExistError.en,
        );
      });
  });
});
