describe('Remove employee by manager', () => {
  before(() => {
    cy.managerLogin();
  });

  it('Click Confirm and Cancel button - remove Employee', () => {
    cy.get('div[data-testid="employeeBar"]').last().trigger('mouseover');
    cy.get('svg[data-testid="deleteEmployee"]').last().click({ force: true });
    cy.get('button').contains('Cancel').click();
    cy.get('div[data-testid="employeeBar"]').last().contains('Test User');

    cy.get('div[data-testid="employeeBar"]').last().trigger('mouseover');
    cy.get('svg[data-testid="deleteEmployee"]').last().click({ force: true });
    cy.get('button[type="submit"]').click({ force: true });
    cy.get('.Toastify').contains('Employee was removed');
    cy.get('div[data-testid="employeeBar"]')
      .last()
      .contains('Test User')
      .should('not.exist');
  });
});
