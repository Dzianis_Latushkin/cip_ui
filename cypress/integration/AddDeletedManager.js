import translations from '../../src/locales/translation.json';
const modal = translations.errorsHandler;

describe('Adding a new manager that has already been deleted', () => {
  before(() => {
    cy.adminLogin();
  });

  it('Add Employee that has already been deleted', () => {
    const newManager = Math.floor(Math.random() * 1000000);

    cy.get('div[data-testid="managersTable"]')
      .its('length')
      .then(() => {
        cy.get('[data-testid="addButton"]').click();
        cy.get('input[name="email"]').type(`${newManager}@mail.ru`);
        cy.get('input[name="name"]').type('Cypress Manager');
        cy.get('button[type="submit"]').click({ force: true }).wait(3000);

        cy.get('p[data-testid="managers-email"]').contains(
          `${newManager}@mail.ru`,
        );
        cy.get('p[data-testid="managers-name"]').contains('Cypress Manager');
      });

    cy.get('svg[data-testid="moreIcon"]').last().click({ force: true });
    cy.get('div[data-testid="mark-as-remove"]').last().click({ force: true });
    cy.get('button').contains('Confirm').click({ force: true });

    cy.get('button[type="submit"]').click({ force: true }).wait(3000);

    cy.get('div[data-testid="managersTable"]')
      .its('length')
      .then(() => {
        cy.get('[data-testid="addButton"]').click();
        cy.get('input[name="email"]').type(`${newManager}@mail.ru`);
        cy.get('input[name="name"]').type('Cypress Manager');
        cy.get('button[type="submit"]').click({ force: true }).wait(3000);

        cy.get('p[data-testid="email-error"]').should(
          'contain',
          modal.userDeletedError.en,
        );
      });
  });
});
