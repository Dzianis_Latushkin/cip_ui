import translations from '../../src/locales/translation.json';
const modal = translations.employeesPage.employeesModal;

describe('Create incorrect employee', () => {
  before(() => {
    cy.managerLogin();
  });
  afterEach(() => {
    cy.get('button[data-testid="cancelButton"]').click();
  });
  it('Create new employee with invalid email', () => {
    cy.get('button[data-testid="addEmployeeButton"]').click();
    cy.get('input[name="email"]').type('user@gmail.c');
    cy.get('input[name="name"]').type('Test User');
    cy.get('button[type="submit"]').click();
    cy.get('p[data-testid="emailError"]').should(
      'contain',
      modal.emailValidationError.en,
    );
  });
  it('Create new employee with empty email', () => {
    cy.get('button[data-testid="addEmployeeButton"]').click();
    cy.get('input[name="name"]').type('Test User');
    cy.get('button[type="submit"]').click();
    cy.get('p[data-testid="emailError"]').should(
      'contain',
      modal.emailrequiredError.en,
    );
  });
  it('Create new employee with empty name', () => {
    cy.get('button[data-testid="addEmployeeButton"]').click();
    cy.get('input[name="email"]').type('testMail@gmail.com');
    cy.get('button[type="submit"]').click();
    cy.get('p[data-testid="nameError"]').should(
      'contain',
      modal.nameValidationError.en,
    );
  });
  it('Create new employee with invalid WOD link', () => {
    cy.get('button[data-testid="addEmployeeButton"]').click();
    cy.get('input[name="email"]').type('testMail@gmail.com');
    cy.get('input[name="name"]').type('Test User');
    cy.get('input[name="linkWOD"]').type('wodlink');
    cy.get('button[type="submit"]').click();
    cy.get('p[data-testid="wodError"]').should(
      'contain',
      modal.wodValidationError.en,
    );
  });

  it('Create new employee with invalid SMG link', () => {
    cy.get('button[data-testid="addEmployeeButton"]').click();
    cy.get('input[name="email"]').type('testMail@gmail.com');
    cy.get('input[name="name"]').type('Test User');
    cy.get('input[name="linkSMG"]').type('smglink');
    cy.get('button[type="submit"]').click();
    cy.get('p[data-testid="smgError"]').should(
      'contain',
      modal.smgValidationError.en,
    );
  });
});
