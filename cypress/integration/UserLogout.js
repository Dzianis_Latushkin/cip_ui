describe('Logout', () => {
  it('Login admin with credentials and logout', () => {
    cy.visit('/');
    cy.get('input[name="login"]').type(Cypress.env('ADMIN_EMAIL'));
    cy.get('input[name="password"]').type(Cypress.env('ADMIN_PASSWORD'));
    cy.get('button').click();
    cy.url().should('include', '/managers');
    cy.get('svg[data-testid="LogOutButton"]').click();
    cy.url().should('include', '/login');
  });

  it('Login manager with credentials and logout', () => {
    cy.visit('/');
    cy.get('input[name="login"]').type(Cypress.env('MANAGER_EMAIL'));
    cy.get('input[name="password"]').type(Cypress.env('MANAGER_PASSWORD'));
    cy.get('button').click();
    cy.url().should('include', '/employees');
    cy.get('svg[data-testid="LogOutButton"]').click();
    cy.url().should('include', '/login');
  });
  it('Login employee with credentials and logout', () => {
    cy.visit('/');
    cy.get('input[name="login"]').type(Cypress.env('EMPLOYEE_EMAIL'));
    cy.get('input[name="password"]').type(Cypress.env('EMPLOYEE_PASSWORD'));
    cy.get('button').click();
    cy.url().should('include', '/results');
    cy.get('svg[data-testid="LogOutButton"]').click();
    cy.url().should('include', '/login');
  });
});
