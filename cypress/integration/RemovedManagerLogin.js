import translations from '../../src/locales/translation.json';
const modal = translations.errorsHandler;

describe('Authorization removed manager', () => {
  it('Login removed manager with credentials', () => {
    cy.visit('/');
    cy.get('input[name="login"]').type('tosacy@decabg.eu');
    cy.get('input[name="password"]').type('XkRVHIPDNx');
    cy.get('button').click();
    cy.get('p[data-testid="loginError"]').should(
      'contain',
      modal.userDeletedError.en,
    );
  });
});
