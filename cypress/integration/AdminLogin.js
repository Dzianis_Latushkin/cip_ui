describe('Authorization', () => {
  it('Login admin with credentials', () => {
    cy.visit('/');
    cy.get('input[name="login"]').type(Cypress.env('ADMIN_EMAIL'));
    cy.get('input[name="password"]').type(Cypress.env('ADMIN_PASSWORD'));
    cy.get('button').click();
    cy.url().should('include', '/managers');
  });
});
