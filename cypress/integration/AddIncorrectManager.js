import translations from '../../src/locales/translation.json';
const modal = translations.managersPage.managerModal;

describe('Create manager with incrorrect data', () => {
  before(() => {
    cy.adminLogin();
  });
  it('Add manager', () => {
    cy.get('div[data-testid="managersTable"]')
      .its('length')
      .then(() => {
        cy.get('[data-testid="addButton"]').click();

        cy.get('button[type="submit"]').click({ force: true });
        cy.get('p[data-testid="email-error"]').should(
          'contain',
          modal.requiredEmailValidationError.en,
        );
        cy.get('p[data-testid="name-error"]').should(
          'contain',
          modal.requiredNameValidationError.en,
        );

        cy.get('input[name="email"]').type('wrong-email-format$mail.ru');
        cy.get('button[type="submit"]').click({ force: true });
        cy.get('p[data-testid="email-error"]').should(
          'contain',
          modal.emailValidationError.en,
        );
        cy.get('p[data-testid="name-error"]').should(
          'contain',
          modal.requiredNameValidationError.en,
        );
      });
  });
});
