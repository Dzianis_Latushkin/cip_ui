// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add('login', (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add('drag', { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add('dismiss', { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite('visit', (originalFn, url, options) => { ... })

Cypress.Commands.add('managerLogin', () => {
  cy.visit('/', {
    onBeforeLoad: (window) => {
      Object.defineProperty(window.navigator, 'language', { value: 'en-GB' });
    },
  });
  cy.get('input[name="login"]').type(Cypress.env('MANAGER_EMAIL'));
  cy.get('input[name="password"]').type(Cypress.env('MANAGER_PASSWORD'));
  cy.get('button').click();
});

Cypress.Commands.add('adminLogin', () => {
  cy.visit('/', {
    onBeforeLoad: (window) => {
      Object.defineProperty(window.navigator, 'language', { value: 'en-GB' });
    },
  });
  cy.get('input[name="login"]').type(Cypress.env('ADMIN_EMAIL'));
  cy.get('input[name="password"]').type(Cypress.env('ADMIN_PASSWORD'));
  cy.get('button').click();
});
