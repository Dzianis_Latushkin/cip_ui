import React from 'react';
import { render } from 'react-dom';
import { BrowserRouter } from 'react-router-dom';
import { Provider } from 'react-redux';

import ReactModal from 'react-modal';
import { store } from 'reducers/index';
import './i18n';

import App from './App';

import 'assets/styles/styles.scss';

ReactModal.setAppElement('#cip-app');

render(
  <BrowserRouter>
    <Provider store={store}>
      <App />
    </Provider>
  </BrowserRouter>,
  document.getElementById('cip-app'),
);
