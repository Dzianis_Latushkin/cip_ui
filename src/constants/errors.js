export const getErrors = (t) => {
  return {
    INVALID_CREDO: t('errorsHandler:invalidCredoError'),
    USER_DELETED: t('errorsHandler:userDeletedError'),
    SOMETHING_WRONG: t('errorsHandler:notUpdateUserError'),
    INVALID_TOKEN: t('errorsHandler:invalidTokenError'),
    USER_EXIST: t('errorsHandler:userExistError'),
    USER_NOT_EXIST: t('errorsHandler:userNotExistError'),
    NO_ACCESS: t('errorsHandler:noAccessError'),
  };
};

export const CODE_ERRORS = {
  INVALID_TOKEN: 'INVALID_TOKEN',
};
