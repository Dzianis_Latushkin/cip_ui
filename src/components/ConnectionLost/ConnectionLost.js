import React from 'react';
import { useTranslation } from 'react-i18next';

import { Icon } from 'components/shared/Icon';
import { Typography } from 'components/shared/Typography';

import styles from './ConnectionLost.styles.scss';

const ConnectionLost = () => {
  const { t } = useTranslation('connectionLost');

  return (
    <div className={styles.container}>
      <div className={styles.imgWrap}>
        <Icon name="noWifi" className={styles.img} />
      </div>
      <div className={styles.message}>
        <Typography variant="h1" component="h1" className={styles.messageTitle}>
          {t('title')}
        </Typography>
        <Typography>{t('description')}</Typography>
      </div>
    </div>
  );
};

export default ConnectionLost;
