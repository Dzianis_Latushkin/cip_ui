import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';

import * as api from 'api/requests/requests';

import { showToast } from 'helpers/showToast';

import { convertDataToApi } from './EmployeesModal.helpers';

import EmployeesModal from './EmployeesModal';

const EmployeesModalContainer = ({
  t,
  edit,
  addEmployeeInterviewPage,
  id,
  users,
  editData,
  openConfirm,
  setEmployees,
  openEmployeesCompareModal,
  onClose,
  ...restProps
}) => {
  const [tEnglish] = useTranslation('englishLevels');
  const [tNotifications] = useTranslation('notifications');
  const [loading, setLoading] = useState(false);

  const handleSubmit = (values, actions) => {
    setLoading(true);

    const data = convertDataToApi(values);

    if (!edit && users.some((user) => user.email === values.email)) {
      actions.setFieldError('email', 'You are already have this employee');

      setLoading(false);

      return;
    }

    if (edit) {
      api
        .editEmployee(id, data)
        .then((res) => {
          setLoading(false);

          setEmployees((prevState) => {
            return {
              ...prevState,
              data: {
                users: prevState.data.users.map((user) => {
                  if (user._id === id) {
                    return res.data.user;
                  } else {
                    return user;
                  }
                }),
              },
            };
          });
          showToast(tNotifications('editEmployeeNotification'));

          onClose();
        })
        .catch((err) => {
          setLoading(false);
          actions.setFieldError('email', err);
        });
    }

    if (!edit) {
      api
        .getUser(data.email)
        .then((res) => {
          if (res.data.user && res.data.user.role === 'manager') {
            setLoading(false);
            actions.setFieldError(
              'email',
              'This user has manager role, you cant add him as employee',
            );
          }

          if (res.data.user !== null && res.data.user.managers.length > 0) {
            setLoading(false);
            onClose();
            openEmployeesCompareModal({
              nextUserData: data,
              prevUserData: res.data.user,
              addEmployeeInterviewPage,
            });
          }

          return res.data.user;
        })
        .then((userData) => {
          if (userData === null) {
            api
              .addUser(data)
              .then((res) => {
                setLoading(false);
                setEmployees((prevState) => {
                  return {
                    ...prevState,
                    data: {
                      ...prevState.data,
                      users: addEmployeeInterviewPage
                        ? [
                            ...prevState.data.users,
                            {
                              label: res.data.user.name,
                              option: res.data.user._id,
                            },
                          ]
                        : [...prevState.data.users, res.data.user],
                    },
                  };
                });
                showToast(tNotifications('addEmployeeNotification'));
                onClose();
              })
              .catch((err) => {
                setLoading(false);
                actions.setFieldError('email', err);
              });
          }

          if (
            userData !== null &&
            userData.managers.length === 0 &&
            userData.role !== 'manager'
          ) {
            api
              .editEmployee(userData._id, data)
              .then((res) => {
                setLoading(false);
                setEmployees((prevState) => {
                  return {
                    ...prevState,
                    data: {
                      ...prevState.data,
                      users: addEmployeeInterviewPage
                        ? [
                            ...prevState.data.users,
                            {
                              label: res.data.user.name,
                              option: res.data.user._id,
                            },
                          ]
                        : [...prevState.data.users, res.data.user],
                    },
                  };
                });
                showToast(tNotifications('addEmployeeNotification'));
                onClose();
              })
              .catch((err) => {
                setLoading(false);
                actions.setFieldError('email', err);
              });
          }
        });
    }
  };

  return (
    <EmployeesModal
      t={t}
      tEnglish={tEnglish}
      loading={loading}
      edit={edit}
      editData={editData}
      onSubmit={handleSubmit}
      onClose={onClose}
      {...restProps}
    />
  );
};

export default React.memo(EmployeesModalContainer);
