import React from 'react';
import { Formik, Form, Field } from 'formik';
import cn from 'classnames';

import { getEnglishLevels } from 'helpers/getEnglishLevels';

import { convertDataToForm } from './EmployeesModal.helpers';
import {
  initialValues,
  getValidationSchema,
} from './EmployeesModal.formConfig';

import { Modal } from 'components/shared/Modal';
import { Button } from 'components/shared/Button';
import { Typography } from 'components/shared/Typography';
import { ErrorMessage } from 'components/shared/ErrorMessage';
import { ScrollWrapper } from 'components/shared/ScrollWrapper';

import { InputField, SelectField } from 'components/FormikFields';

import styles from './EmployeesModal.styles.scss';

const EmployeesModal = ({
  t,
  tEnglish,
  loading,
  isOpen,
  edit,
  editData,
  onSubmit,
  onClose,
}) => (
  <Modal open={isOpen} centered className={styles.modal} onClose={onClose}>
    <Typography variant="h1" className={styles.title}>
      {t('employeesModal.title')}
    </Typography>
    <ScrollWrapper translateContentSizeYToHolder trackClassName={styles.track}>
      <Formik
        validationSchema={getValidationSchema(t)}
        validateOnMount
        initialValues={edit ? convertDataToForm(editData) : initialValues}
        onSubmit={onSubmit}
      >
        {({ values, errors }) => (
          <Form className={styles.form}>
            <Field
              label={t('employeesModal.emailLabel')}
              name="email"
              errorId="emailError"
              errorClassName={styles.emailErrorContainer}
              placeholder={t('employeesModal.emailPlaceholder')}
              component={InputField}
              inputClassName={styles.inputField}
              formControlProps={{ className: styles.noMargin }}
            />
            <Typography
              variant="link"
              component="a"
              className={cn(styles.formLink, styles.linkDisable)}
            >
              {t('employeesModal.wodImport')}
            </Typography>
            <Field
              label={t('employeesModal.nameLabel')}
              name="name"
              errorId="nameError"
              placeholder={t('employeesModal.namePlaceholder')}
              component={InputField}
              inputClassName={styles.inputField}
            />
            <Field
              label={t('employeesModal.englishLevelLable')}
              name="englishLevel"
              placeholder={t('employeesModal.englishLevelPlaceholder')}
              component={SelectField}
              options={getEnglishLevels(tEnglish)}
              inputClassName={styles.inputField}
            />

            <Field
              label={t('employeesModal.wodLinkLabel')}
              name="linkWOD"
              errorId="wodError"
              placeholder={t('employeesModal.wodLinkPlaceholder')}
              component={InputField}
              inputClassName={styles.inputField}
            />
            <Field
              label={t('employeesModal.smgLinkLabel')}
              name="linkSMG"
              errorId="smgError"
              placeholder={t('employeesModal.smgLinkPlaceholder')}
              component={InputField}
              inputClassName={styles.inputField}
            />
            <div className={styles.errorContainer}>
              {errors.form && (
                <ErrorMessage className={styles.errorMessage}>
                  {errors.form.map((error) => error + '; ')}
                </ErrorMessage>
              )}
            </div>

            <div className={styles.buttonBar}>
              <Button
                type="submit"
                variant="default"
                loading={loading}
                className={styles.button}
              >
                {t('employeesModal.confirmButton')}
              </Button>
              <Button
                variant="outlined"
                className={styles.button}
                onClick={onClose}
                data-testid="cancelButton"
              >
                {t('employeesModal.cancelButton')}
              </Button>
            </div>
          </Form>
        )}
      </Formik>
    </ScrollWrapper>
  </Modal>
);

export default EmployeesModal;
