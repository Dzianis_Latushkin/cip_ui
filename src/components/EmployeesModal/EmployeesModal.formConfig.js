import * as Yup from 'yup';

export const getValidationSchema = (t) =>
  Yup.object().shape({
    name: Yup.string()
      .required(t('employeesModal.nameValidationError'))
      .test('isValid', t('employeesModal.nameSpacesError'), (value) => {
        if (value) {
          return value.replace(/\s/g, '').length;
        }

        return true;
      }),
    linkSMG: Yup.string().url(t('employeesModal.smgValidationError')),
    linkWOD: Yup.string().url(t('employeesModal.wodValidationError')),
    englishLevel: Yup.object(),
    email: Yup.string()
      .required(t('employeesModal.emailrequiredError'))
      .matches(
        /^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,3})+$/, //https://www.w3resource.com/javascript/form/email-validation.php
        t('employeesModal.emailValidationError'),
      ),
  });

export const initialValues = {
  name: '',
  linkSMG: '',
  englishLevel: { value: 'Unknown', label: 'Unknown' },
  linkWOD: '',
  email: '',
};
