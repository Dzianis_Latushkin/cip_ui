export const convertDataToForm = (editData) => ({
  name: editData.name,
  linkSMG: editData.SMGlink || '',
  englishLevel: {
    value: editData.engLevel,
    label: editData.engLevel,
  },
  linkWOD: editData.WODlink || '',
  email: editData.email,
});

export const convertDataToApi = (values) => ({
  email: values.email,
  name: values.name,
  engLevel: values.englishLevel.value,
  SMGlink: values.linkSMG || '',
  WODlink: values.linkWOD || '',
});
