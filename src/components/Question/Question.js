import React from 'react';
import cn from 'classnames';
import { Tooltip } from 'react-tippy';

import { Icon } from 'components/shared/Icon';
import { Typography } from 'components/shared/Typography';

import styles from './Question.styles.scss';

const Question = ({
  withoutActions,
  id,
  groupId,
  question,
  comment,
  setFieldError,
  onEditQuestionClick,
  onDeleteQuestionClick,
  onAddQuestionClick,
  isInterviewModal,
  isSelected,
  className,
}) => (
  <div className={cn(styles.question, className)}>
    <div className={styles.questionContainer}>
      <Icon name="question" className={styles.questionIcon} />
      <Typography variant="body" className={styles.questionContent}>
        {question}
      </Typography>
    </div>
    <div className={styles.buttonContainer}>
      {comment && (
        <Tooltip
          html={
            <div className={styles.tooltipComent}>
              <Typography className={styles.tooltipText}> {comment}</Typography>
            </div>
          }
          position="top-end"
          trigger="click"
        >
          <Icon name="comment" className={styles.iconButton} />
        </Tooltip>
      )}
      {isInterviewModal ? (
        <>
          {isSelected ? (
            <>
              <Icon
                name="checkCircle"
                className={cn(styles.iconButton, styles.iconButtonCheck)}
              />
              <Icon
                name="delete"
                className={cn(styles.iconButton, styles.removeButton)}
                onClick={() => {
                  onDeleteQuestionClick(id, groupId);
                }}
              />
            </>
          ) : (
            <Icon
              name="plusCircle"
              onClick={() => {
                onAddQuestionClick(id, setFieldError);
              }}
              className={cn(styles.iconButton, styles.iconButtonAdd)}
            />
          )}
        </>
      ) : (
        <>
          {!withoutActions && (
            <>
              <Icon
                name="pencil"
                className={styles.iconButton}
                onClick={() => onEditQuestionClick(id, groupId)}
              />
              <Icon
                name="delete"
                className={cn(styles.iconButton, styles.removeButton)}
                onClick={() => {
                  onDeleteQuestionClick(id, groupId);
                }}
              />
            </>
          )}
        </>
      )}
    </div>
  </div>
);

export default Question;
