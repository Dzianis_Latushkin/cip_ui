import React from 'react';
import {
  SortableContainer,
  SortableElement,
  sortableHandle,
} from 'react-sortable-hoc';

import { Icon } from 'components/shared/Icon';

import styles from './SortableList.styles.scss';

const SortableItem = SortableElement(({ item, renderItem, ...restProps }) => (
  <div className={styles.storableItem}>
    <DragHandle />
    {renderItem({ item, ...restProps })}
  </div>
));

const DragHandle = sortableHandle(() => (
  <Icon name="draggable" className={styles.dragHandleIcon} />
));

const List = SortableContainer(({ items, ...restProps }) => (
  <div>
    {items.map((item, index) => (
      <SortableItem
        key={item?._id}
        index={index}
        sortIndex={index}
        item={item}
        {...restProps}
      />
    ))}
  </div>
));

const SortableList = ({ items, ...restProps }) => (
  <List items={items} {...restProps} />
);

export default React.memo(SortableList);
