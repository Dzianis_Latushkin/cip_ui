import React from 'react';

import cn from 'classnames';

import { Logo } from 'components/shared/Logo';
import { Menu } from './components/Menu';
import { Icon } from 'components/shared/Icon';
import { Button } from 'components/shared/Button';
import { Typography } from 'components/shared/Typography';

import styles from './SideBar.styles.scss';

const SideBar = ({
  userName,
  menuItems,
  t,
  onLogoutClick,
  onAddAppFeedbackClick,
}) => (
  <div className={styles.sideBar}>
    <Logo />
    <Menu menuItems={menuItems} />
    <div className={styles.SideBarFeedback}>
      <Button
        className={styles.sideBarButton}
        title={t('leaveFeedbackButton')}
        onClick={onAddAppFeedbackClick}
      >
        <div className={styles.sideBarButtonIconsWrap}>
          <Icon
            name="thumbsDown"
            className={cn(styles.sideBarButtonIcon, styles.thumbsDown)}
          />
          <Icon
            name="thumbsUp"
            className={cn(styles.sideBarButtonIcon, styles.thumbsUp)}
          />
        </div>
        <Typography className={styles.sideBarButtonText}>
          {t('leaveFeedbackButton')}
        </Typography>
      </Button>
    </div>
    <div className={styles.sideBarFooter}>
      <div className={styles.footerUser}>
        <Icon name="user" className={styles.footerUserIcon} />
        <Typography>{userName}</Typography>
      </div>
      <div className={styles.footerExit}>
        <Icon data-testid="logout-button" name="exit" onClick={onLogoutClick} />
      </div>
    </div>
  </div>
);

export default React.memo(SideBar);
