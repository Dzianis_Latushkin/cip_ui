import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigate } from 'react-router-dom';

import { userSelector } from 'reducers/selectors';
import { logoutUser } from 'reducers/userReducer';

import { getMenuItems } from 'helpers/getMenuItems';

import SideBar from './SideBar';
import { useTranslation } from 'react-i18next';

const SideBarContainer = ({ openAppFeedbackModal }) => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const { t } = useTranslation('sideBar');

  const user = useSelector(userSelector);

  const userName = user.currentUser?.name ? `${user.currentUser.name}` : 'user';

  const menuItems = getMenuItems({ t, role: user?.currentUser.role });

  const handleLogoutClick = () => {
    logoutUser(dispatch);
    navigate('/login');
  };

  const handleAddAppFeedbackClick = () => {
    openAppFeedbackModal();
  };

  return (
    <SideBar
      t={t}
      userName={userName}
      menuItems={menuItems}
      onLogoutClick={handleLogoutClick}
      onAddAppFeedbackClick={handleAddAppFeedbackClick}
    />
  );
};

export default React.memo(SideBarContainer);
