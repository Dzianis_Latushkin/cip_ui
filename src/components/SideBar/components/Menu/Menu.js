import React from 'react';
import cn from 'classnames';
import { NavLink } from 'react-router-dom';

import { Icon } from 'components/shared/Icon';
import { Typography } from 'components/shared/Typography';

import styles from './Menu.styles.scss';

const Menu = ({ menuItems }) => {
  return (
    <div className={styles.menu}>
      {menuItems.map((item) => (
        <NavLink
          key={item.key}
          className={({ isActive }) =>
            isActive
              ? cn(styles.menuItemActive, styles.menuItem)
              : styles.menuItem
          }
          to={item.to}
        >
          <Icon name={item.icon} className={styles.menuIcon} />
          <Typography variant="subtitle" className={styles.menuItemText}>
            {item.title}
          </Typography>
        </NavLink>
      ))}
    </div>
  );
};

export default React.memo(Menu);
