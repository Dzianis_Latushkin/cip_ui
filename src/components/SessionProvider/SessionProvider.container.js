import React, { useState, useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { useNavigate, useLocation } from 'react-router-dom';
import { useTranslation } from 'react-i18next';

import * as api from 'api/requests/requests';

import { ROUTES } from 'constants/routes';
import { CIP_TOKEN_KEY } from 'constants/token';

import { setUser } from 'reducers/userReducer';

import SessionProvider from './SessionProvider';

const SessionProviderContainer = (props) => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const location = useLocation();

  const { t } = useTranslation('errorsHandler');

  const token = localStorage.getItem(CIP_TOKEN_KEY);

  const [loading, setLoading] = useState(!!token);

  const successCallback = () => {
    setLoading(false);
    if (location.pathname === ROUTES.LOGIN_PAGE) {
      navigate(ROUTES.ROOT);
    }
  };

  const errorCallback = () => {
    setLoading(false);
  };

  useEffect(() => {
    if (token) {
      api
        .authUser()
        .then((res) => {
          setUser(dispatch, res.data);
          successCallback();
        })
        .catch((err) => {
          if (err === t('invalidTokenError')) {
            localStorage.removeItem(CIP_TOKEN_KEY);
          }
          errorCallback();
        });
    } else {
      errorCallback();
    }
  }, []);

  return <SessionProvider {...props} loading={loading} />;
};

export default React.memo(SessionProviderContainer);
