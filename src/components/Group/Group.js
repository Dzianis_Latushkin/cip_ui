import React from 'react';
import cn from 'classnames';
import Collapsible from 'react-collapsible';

import { Trigger } from './components/Trigger';

import styles from './Group.styles.scss';

const Group = ({
  isInterviewModal,
  id,
  groupName,
  children,
  onAddQuestionClick,
  onAddFolderClick,
  onEditFolderClick,
  onDeleteFolderClick,
  openedClassName,
  closedClassName,
}) => {
  return (
    <Collapsible
      className={cn(styles.group, closedClassName)}
      openedClassName={cn(styles.openedGroup, openedClassName)}
      onDeleteFolderClick={onDeleteFolderClick}
      trigger={
        <Trigger
          isInterviewModal={isInterviewModal}
          id={id}
          groupName={groupName}
          onAddQuestionClick={onAddQuestionClick}
          onAddFolderClick={onAddFolderClick}
          onEditFolderClick={onEditFolderClick}
          onDeleteFolderClick={onDeleteFolderClick}
        />
      }
      triggerWhenOpen={
        <Trigger
          isTriggerOpen
          isInterviewModal={isInterviewModal}
          id={id}
          groupName={groupName}
          onAddQuestionClick={onAddQuestionClick}
          onAddFolderClick={onAddFolderClick}
          onEditFolderClick={onEditFolderClick}
          onDeleteFolderClick={onDeleteFolderClick}
        />
      }
    >
      <div className={styles.questionContainer}>{children}</div>
    </Collapsible>
  );
};

export default Group;
