import React from 'react';
import { useTranslation } from 'react-i18next';

import { Icon } from 'components/shared/Icon';
import { Button } from 'components/shared/Button';
import { Typography } from 'components/shared/Typography';

import styles from './Trigger.styles.scss';

const Trigger = ({
  isTriggerOpen,
  id,
  groupName,
  children,
  onAddQuestionClick,
  onAddFolderClick,
  onEditFolderClick,
  onDeleteFolderClick,
  isInterviewModal,
}) => {
  const { t } = useTranslation('trigger');

  return (
    <div className={styles.trigger}>
      <div className={styles.foldersNameContainer}>
        <Icon
          name={isTriggerOpen ? 'folderOpen' : 'folder'}
          className={styles.folderIcon}
        />

        <Typography variant="subtitle" className={styles.groupName}>
          {groupName}
        </Typography>
      </div>

      {!isInterviewModal && isTriggerOpen && (
        <div className={styles.buttonContainer} data-testid="button-container">
          <Button
            onClick={(e) => {
              e.stopPropagation();
              onAddQuestionClick(id);
            }}
            variant="outlined"
            size="small"
            className={styles.button}
          >
            {t('addQuestionButton')}
          </Button>
          <Button
            onClick={(e) => {
              e.stopPropagation();
              onAddFolderClick(id);
            }}
            variant="outlined"
            size="small"
            className={styles.button}
          >
            {t('addGroupButton')}
          </Button>
          <Button
            onClick={(e) => {
              e.stopPropagation();
              onEditFolderClick(id);
            }}
            variant="outlined"
            size="small"
            className={styles.button}
          >
            {t('editGroupButton')}
          </Button>
          <Button
            variant="outlined"
            size="small"
            className={styles.button}
            onClick={(e) => {
              e.stopPropagation();
              onDeleteFolderClick(id);
            }}
            data-testid="button-remove"
          >
            {t('removeGroupButton')}
          </Button>
        </div>
      )}
    </div>
  );
};

export default Trigger;
