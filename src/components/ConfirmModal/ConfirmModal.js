import React from 'react';
import { useTranslation } from 'react-i18next';

import { Modal } from 'components/shared/Modal';
import { Button } from 'components/shared/Button';
import { Typography } from 'components/shared/Typography';

import styles from './ConfirmModal.styles.scss';

const ConfirmModal = ({
  loading,
  isOpen,
  withCloseIcon,
  titleText,
  contentText,
  confirmButtonText,
  cancelButtonText,
  onCancelClick,
  onConfirmClick,
  onClose,
}) => {
  const { t } = useTranslation('confirmModal');

  return (
    <Modal
      open={isOpen}
      withCloseIcon={withCloseIcon}
      onClose={onClose}
      centered
      className={styles.modal}
    >
      <Typography variant="h1" className={styles.title}>
        {titleText || t('defaultTitle')}
      </Typography>
      {contentText && (
        <Typography variant="body" className={styles.subTitle}>
          {contentText}
        </Typography>
      )}
      <div className={styles.buttonBar}>
        <Button
          onClick={onConfirmClick}
          type="submit"
          variant="default"
          loading={loading}
          className={styles.button}
        >
          {confirmButtonText || t('confirmButton')}
        </Button>

        <Button
          variant="alert"
          className={styles.button}
          onClick={onCancelClick}
        >
          {cancelButtonText || t('cancelButton')}
        </Button>
      </div>
    </Modal>
  );
};

ConfirmModal.defaultProps = {
  isOpen: false,
};

export default React.memo(ConfirmModal);
