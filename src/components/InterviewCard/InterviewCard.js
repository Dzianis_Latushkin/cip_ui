import React from 'react';
import cn from 'classnames';
import { Tooltip } from 'react-tippy';

import { INTERVIEW_CARD_TYPE } from 'constants/common';

import { timeEstimation } from 'helpers/timeEstimation';
import { elapsedTime } from 'helpers/elapsedTime';

import { Icon } from 'components/shared/Icon';
import { Button } from 'components/shared/Button';
import { Typography } from 'components/shared/Typography';

import styles from './InterviewCard.styles.scss';

const InterviewCard = ({
  t,
  variant,
  navigate,
  interview,
  onDuplicateInterviewClick,
  onRemoveInterviewClick,
  onArchiveInterviewClick,
  onResultsClick,
  onStartInterviewClick,
}) => (
  <div className={styles.card}>
    <div className={styles.cardHeader}>
      <div className={styles.cardTitleWrap}>
        <Typography
          variant="h2"
          className={styles.cardTitle}
          title={interview.title}
        >
          {interview.title}
        </Typography>
      </div>
      <div className={styles.cardMenu}>
        <Tooltip
          html={
            <div className={styles.tooltip}>
              {variant === INTERVIEW_CARD_TYPE.PLANNED && (
                <div
                  className={styles.tooltipRow}
                  onClick={() => {
                    navigate(`/interview/${interview._id}/edit`);
                  }}
                >
                  <Icon name="pencil" className={styles.tooltipIcon} />
                  <Typography className={styles.tooltipItem}>
                    {t('interviewCard.editInterviewButton')}
                  </Typography>
                </div>
              )}
              {variant === INTERVIEW_CARD_TYPE.COMPLETED && (
                <div
                  className={styles.tooltipRow}
                  onClick={() => onArchiveInterviewClick(interview._id)}
                >
                  <Icon name="archive" className={styles.tooltipIcon} />
                  <Typography className={styles.tooltipItem}>
                    {t('interviewCard.archiveInterviewButton')}
                  </Typography>
                </div>
              )}
              {variant === INTERVIEW_CARD_TYPE.PLANNED && (
                <div
                  className={styles.tooltipRow}
                  onClick={() => onRemoveInterviewClick(interview._id)}
                >
                  <Icon name="crossCircle" className={styles.tooltipIcon} />
                  <Typography className={styles.tooltipItem}>
                    {t('interviewCard.cancelInterviewButton')}
                  </Typography>
                </div>
              )}
              <div
                className={styles.tooltipRow}
                onClick={() => onDuplicateInterviewClick(interview._id)}
              >
                <Icon name="copy" className={styles.tooltipIcon} />
                <Typography className={styles.tooltipItem}>
                  {t('interviewCard.duplicateInterviewButton')}
                </Typography>
              </div>
            </div>
          }
          position="bottom-end"
          trigger="click"
        >
          <Icon className={cn(styles.icon, styles.clickable)} name="more" />
        </Tooltip>
      </div>
    </div>
    <div className={styles.cardMain}>
      <hr />
      <div className={styles.infoBlock}>
        <Tooltip
          html={
            <div className={styles.nameTooltip}>
              <Typography className={styles.nameTooltipText}>
                {interview.employee?.name}
              </Typography>
            </div>
          }
          position="top-start"
          trigger="mouseenter"
          delay={[200, 1]}
        >
          <div className={cn(styles.tooltipRow, styles.noHoverLightning)}>
            <Icon
              className={cn(styles.icon, styles.iconUser, styles.iconIndent)}
              name="user"
            />
            <Typography className={styles.userName}>
              {interview.employee?.name}
            </Typography>
          </div>
        </Tooltip>
        <Tooltip
          html={
            <div className={styles.timeEstimationTooltip}>
              <Typography className={styles.tooltipText}>
                {t('interviewCard.timeDescriptionTooltip')}
              </Typography>
            </div>
          }
          position="top-start"
          trigger="mouseenter"
          delay={[200, 1]}
        >
          <div className={cn(styles.tooltipRow, styles.noHoverLightning)}>
            <Icon
              className={cn(styles.icon, styles.iconIndent)}
              name="question"
            />
            <Typography>
              {interview.questions.length}
              {t('interviewCard.questionsLettering')}
              {variant !== INTERVIEW_CARD_TYPE.PLANNED
                ? elapsedTime(interview.feedback.elapsedTime)
                : timeEstimation(interview.questions)}
            </Typography>
          </div>
        </Tooltip>
      </div>
    </div>
    <div className={styles.cardFooter}>
      <div
        className={
          variant !== INTERVIEW_CARD_TYPE.PLANNED
            ? styles.buttonRowContainer
            : cn(styles.buttonRowContainer, styles.buttonAlignRight)
        }
      >
        {variant !== INTERVIEW_CARD_TYPE.PLANNED && (
          <div className={cn(styles.infoRow, styles.date)}>
            <Icon
              className={cn(styles.icon, styles.iconIndent)}
              name="calendar"
            />
            <Typography>
              {new Date(Number(interview.updatedAt)).toLocaleDateString()}
            </Typography>
          </div>
        )}

        {variant !== INTERVIEW_CARD_TYPE.PLANNED ? (
          <Button
            variant="default"
            className={styles.button}
            onClick={() => onResultsClick(interview.feedback._id)}
          >
            {t('interviewCard.resultsButton')}
          </Button>
        ) : (
          <Button
            variant="default"
            className={styles.button}
            onClick={() => onStartInterviewClick(interview._id)}
          >
            {t('interviewCard.startInterviewButton')}
          </Button>
        )}
      </div>
    </div>
  </div>
);

export default InterviewCard;
