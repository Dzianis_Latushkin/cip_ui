import React from 'react';
import { useTranslation } from 'react-i18next';

import { Modal } from 'components/shared/Modal';
import { Button } from 'components/shared/Button';
import { Typography } from 'components/shared/Typography';

import styles from './AlertModal.styles.scss';

const AlertModal = ({
  isOpen,
  titleText,
  contentText,
  confirmButtonText,
  onClose,
}) => {
  const { t } = useTranslation('alertModal');

  return (
    <Modal open={isOpen} onClose={onClose} centered className={styles.modal}>
      <Typography variant="h1" className={styles.title}>
        {titleText || t('defaultTitle')}
      </Typography>
      {contentText && (
        <Typography variant="body" className={styles.subTitle}>
          {contentText}
        </Typography>
      )}
      <div className={styles.buttonBar}>
        <Button onClick={onClose} variant="default" className={styles.button}>
          {confirmButtonText || t('closeButton')}
        </Button>
      </div>
    </Modal>
  );
};

AlertModal.defaultProps = {
  isOpen: false,
};

export default React.memo(AlertModal);
