import React from 'react';
import { useSelector } from 'react-redux';
import { Navigate } from 'react-router-dom';

import { CIP_TOKEN_KEY } from 'constants/token';

import { userSelector } from 'reducers/selectors';

import { getPrimaryRoute } from 'helpers/getPrimaryRoute';

const AuthRoute = ({ children, ...restProps }) => {
  const user = useSelector(userSelector);

  const isAuthenticated = localStorage.getItem(CIP_TOKEN_KEY);

  return (
    <>
      {isAuthenticated ? (
        <Navigate to={{ pathname: getPrimaryRoute(user.currentUser.role) }} />
      ) : (
        children
      )}
    </>
  );
};

export default AuthRoute;
