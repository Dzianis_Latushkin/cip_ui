import React from 'react';
import ErrorMessage from './ErrorMessage';

describe('ErrorMessage component', () => {
  it('should render ErrorMessage component', () => {
    const component = shallow(<ErrorMessage children="Test error" />);
    expect(component).toMatchSnapshot();
  });
});
