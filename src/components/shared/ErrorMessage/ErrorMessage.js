import React from 'react';
import cn from 'classnames';

import { Typography } from 'components/shared/Typography';

import styles from './ErrorMessage.styles.scss';

const ErrorMessage = ({ className, errorId, children }) => (
  <Typography
    className={cn(styles.message, className)}
    variant="body"
    data-testid={errorId}
  >
    {children}
  </Typography>
);

export default React.memo(ErrorMessage);
