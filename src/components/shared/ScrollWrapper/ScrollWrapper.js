import React from 'react';
import cn from 'classnames';
import Scrollbar from 'react-scrollbars-custom';

import styles from './ScrollWrapper.styles.scss';

const ScrollWrapper = ({
  className,
  contentClassName,
  thumbClassName,
  trackClassName,
  scrollbarRef,
  children,
  scrolling,
  onScrollStart,
  onScrollStop,
  ...restProps
}) => (
  <Scrollbar
    ref={scrollbarRef}
    noDefaultStyles
    removeTrackXWhenNotUsed
    removeTrackYWhenNotUsed
    className={cn(styles.scrollWrapper, className)}
    wrapperProps={{
      renderer: (props) => {
        const { elementRef, ...otherProps } = props;

        return (
          <div {...otherProps} ref={elementRef} className={styles.wrapper} />
        );
      },
    }}
    contentProps={{
      renderer: (props) => {
        const { elementRef, ...otherProps } = props;

        return (
          <div
            {...otherProps}
            ref={elementRef}
            className={cn(styles.content, contentClassName)}
          />
        );
      },
    }}
    trackYProps={{
      renderer: (props) => {
        const { elementRef, ...otherProps } = props;

        return (
          <div
            {...otherProps}
            ref={elementRef}
            className={cn(
              styles.track,
              {
                [styles.trackScrolling]: true,
              },
              trackClassName,
            )}
          />
        );
      },
    }}
    thumbYProps={{
      renderer: (props) => {
        const { elementRef, ...otherProps } = props;

        return (
          <div
            {...otherProps}
            ref={elementRef}
            className={cn(styles.thumb, thumbClassName)}
          />
        );
      },
    }}
    onScrollStart={onScrollStart}
    onScrollStop={onScrollStop}
    {...restProps}
  >
    {children}
  </Scrollbar>
);

export default React.memo(ScrollWrapper);
