import React from 'react';
import cn from 'classnames';

import { Loading } from 'components/shared/Loading';
import { Typography } from 'components/shared/Typography';

import { Cell } from './components/Cell';

import cellStyles from './components/Cell/Cell.styles.scss';

import styles from './Table.styles.scss';

const Table = ({
  className,
  loading,
  noHeader,
  data,
  columns,
  params,
  cellRenderer,
  onRowClick,
  ...restProps
}) => (
  <div className={cn(styles.table, className)} {...restProps}>
    {!noHeader && (
      <div className={styles.header}>
        {columns.map((column) => (
          <div
            key={column.key}
            className={cellStyles.cell}
            style={{ minWidth: column.minWidth, maxWidth: column.maxWidth }}
          >
            <Typography className={styles.headerItem}>
              {column.label}
            </Typography>
          </div>
        ))}
      </div>
    )}
    {loading ? (
      <Loading />
    ) : (
      <div className={styles.body}>
        {data.map((item) => (
          <div
            key={item.id}
            className={styles.row}
            onClick={(e) => onRowClick(item, e)}
          >
            {columns.map((column) =>
              cellRenderer ? (
                cellRenderer(
                  { ...column, item, className: cellStyles.cell },
                  Cell,
                )
              ) : (
                <Cell
                  minWidth={column.minWidth}
                  maxWidth={column.maxWidth}
                  key={column.key}
                >
                  {item[column.key]}
                </Cell>
              ),
            )}
          </div>
        ))}
      </div>
    )}
  </div>
);

Table.defaultProps = {
  params: {},
  onRowClick: () => {},
};

export default React.memo(Table);
