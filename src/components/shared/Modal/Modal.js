import React from 'react';
import ReactModal from 'react-modal';
import cn from 'classnames';

import { Icon } from 'components/shared/Icon';

import styles from './Modal.styles.scss';

const Modal = ({
  open,
  withCloseIcon,
  centered,
  landing,
  className,
  children,
  closeTimeoutMS,
  onClose,
}) => (
  <ReactModal
    portalClassName={styles.portal}
    overlayClassName={cn(styles.overlay, {
      [styles.overlayCentered]: centered,
    })}
    className={cn(styles.modal, { [styles.landing]: landing }, className)}
    isOpen={open}
    closeTimeoutMS={closeTimeoutMS}
    onRequestClose={onClose}
  >
    {withCloseIcon && (
      <div className={styles.iconWrapper} onClick={onClose}>
        <Icon name="cross" className={styles.icon} />
      </div>
    )}
    {children}
  </ReactModal>
);

Modal.defaultProps = {
  open: false,
  centered: false,
  closeTimeoutMS: 300,
  variant: 'default',
};

export default React.memo(Modal);
