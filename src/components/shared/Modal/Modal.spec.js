import React from 'react';

import Modal from './Modal';

const setUp = (props) => shallow(<Modal {...props} />);

describe('Modal component', () => {
  it('Should render Modal component with open prop', () => {
    const open = true;
    const component = setUp({ open });
    expect(component.exists('.modal')).toBe(true);
  });

  it('Should render Modal component with children', () => {
    const children = <p className="text">My modal</p>;
    const component = setUp({ children });
    expect(component.contains(<p className="text">My modal</p>)).toBe(true);
  });
});
