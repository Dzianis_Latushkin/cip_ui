import React from 'react';
import cn from 'classnames';

import { Loading } from 'components/shared/Loading';

import styles from './Button.styles.scss';

const VARIANTS = {
  default: styles.default,
  outlined: styles.outlined,
  alert: styles.alert,
  iconButton: styles.iconButton,
};

const SIZES = {
  large: styles.btnLarge,
  medium: styles.btnMedium,
  small: styles.btnSmall,
};

const Button = ({
  loading,
  children,
  className,
  startIcon,
  customIcon,
  endIcon,
  size,
  variant,
  ...restProps
}) => (
  <button
    className={cn(styles.button, VARIANTS[variant], SIZES[size], className)}
    {...restProps}
  >
    {loading ? (
      <Loading variant="default" className={styles.loading} />
    ) : (
      <>
        {startIcon && (
          <div className={cn(styles.startIcon, customIcon)}>{startIcon}</div>
        )}
        {children}
        {endIcon && <div className={styles.endIcon}>{endIcon}</div>}
      </>
    )}
  </button>
);

Button.defaultProps = {
  type: 'button',
  variant: 'default',
  size: 'medium',
};

export default React.memo(Button);
