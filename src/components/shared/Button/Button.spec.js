import React from 'react';

import Button from './Button';

describe('Button component', () => {
  it('Should render Button component without props', () => {
    const component = shallow(<Button />);
    expect(component).toMatchSnapshot();
  });

  it('Should render Button component with props', () => {
    const component = shallow(<Button type="submit" />);
    expect(component).toMatchSnapshot();
  });

  it('Button with Loading', () => {
    const component = shallow(<Button loading={true} />);

    expect(component.find('.loading')).toHaveLength(1);
  });
});
