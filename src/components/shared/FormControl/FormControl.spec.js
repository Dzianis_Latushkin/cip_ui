import React from 'react';
import FormControl from './FormControl';

import { Icon } from 'components/shared/Icon';

const setUp = (props) => shallow(<FormControl {...props} />);

describe('FormControl component', () => {
  it('Should render FormControl component without props', () => {
    const component = setUp();

    expect(component.exists('.control')).toBe(true);
    expect(component.exists('.header')).toBe(false);
    expect(component.exists('.error')).toBe(false);
  });

  it('Should render FormControl component with label', () => {
    const component = setUp({ label: 'test label' });

    expect(component.exists('.label')).toBe(true);
  });

  it('Should render FormControl component with endAdornment of label', () => {
    const component = setUp({
      label: 'test label',
      endAdornment: <Icon name="add" />,
    });

    expect(component.exists('.endAdornment')).toBe(true);
  });

  it('Should render FormControl component with error', () => {
    const component = setUp({
      error: 'test error',
      touched: true,
    });

    expect(component.exists('.error')).toBe(true);
  });
});
