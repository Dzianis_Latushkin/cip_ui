import React from 'react';
import cn from 'classnames';

import { Typography } from 'components/shared/Typography';
import { ErrorMessage } from 'components/shared/ErrorMessage';

import styles from './FormControl.styles.scss';

const FormControl = ({
  className,
  error,
  touched,
  errorId,
  errorClassName,
  children,
  label,
  endAdornment,
  ...restProps
}) => (
  <div className={cn(styles.control, className)} {...restProps}>
    {label && (
      <div className={styles.header}>
        <Typography variant="label" weight="medium" className={styles.label}>
          {label}
        </Typography>
        {endAdornment && (
          <div className={styles.endAdornment}>{endAdornment}</div>
        )}
      </div>
    )}
    <div className={styles.inner}>
      {children}
      {!!error && touched && (
        <ErrorMessage
          errorId={errorId}
          className={cn(styles.error, errorClassName)}
        >
          {error}
        </ErrorMessage>
      )}
    </div>
  </div>
);

export default React.memo(FormControl);
