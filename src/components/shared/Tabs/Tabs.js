import React from 'react';
import cn from 'classnames';

import Tab from './Tab';

import styles from './Tabs.styles.scss';

const Tabs = ({ className, activeTab, children, onTabClick, size }) => (
  <div className={cn(styles.tabs, className)}>
    <div className={styles.header}>
      {children.map((child) => {
        const { label, disabled } = child.props;

        return (
          <Tab
            key={label}
            label={label}
            disabled={disabled}
            active={activeTab === label}
            onClick={() => !disabled && onTabClick(label)}
            size={size}
          />
        );
      })}
    </div>
    <div className={styles.inner}>
      {children.map((child, index) => {
        if (child.props.label !== activeTab) {
          return null;
        }

        return child.props.children;
      })}
    </div>
  </div>
);

Tabs.defaultProps = {
  size: 'small',
};

export default React.memo(Tabs);
