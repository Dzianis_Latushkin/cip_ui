import React from 'react';
import cn from 'classnames';
import { Tooltip } from 'react-tippy';

import { Typography } from 'components/shared/Typography';

import styles from './Tabs.styles.scss';

const SIZES = {
  large: styles.tabLarge,
  small: styles.tabSmall,
};

const Tab = ({ className, active, disabled, label, onClick, size }) => (
  <Tooltip
    html={
      <div className={styles.tooltip}>
        <Typography className={styles.tooltipText}>
          This functionality is not available yet
        </Typography>
      </div>
    }
    position="top-end"
    disabled={!disabled}
    trigger="mouseenter"
    delay={[1000, 1]}
  >
    <Typography
      className={cn(
        styles.tab,
        { [styles.tabActive]: active, [styles.tabDisabled]: disabled },
        className,
        SIZES[size],
      )}
      onClick={() => onClick(label)}
    >
      {label}
    </Typography>
  </Tooltip>
);

export default React.memo(Tab);
