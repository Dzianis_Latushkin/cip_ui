import React, { useState } from 'react';

import Tabs from './Tabs';

const TabsContainer = ({ children, onTabChange, ...restProps }) => {
  const [activeTab, setActiveTab] = useState(children[0].props.label);

  const handleTabClick = (nextTab) => {
    setActiveTab(nextTab);

    if (onTabChange) {
      onTabChange(nextTab);
    }
  };

  return (
    <Tabs
      activeTab={activeTab}
      children={children}
      onTabClick={handleTabClick}
      {...restProps}
    />
  );
};

export default React.memo(TabsContainer);
