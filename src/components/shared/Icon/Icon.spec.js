import React from 'react';
import Icon from './Icon';

describe('Icon component', () => {
  it('Should render Icon component without props', () => {
    const component = shallow(<Icon />);

    expect(component.exists('.icon')).toBe(false);
  });

  it('Should render Icon component with icon name', () => {
    const component = shallow(<Icon name="apple" />);

    expect(component.exists()).toBeTruthy();
  });
});
