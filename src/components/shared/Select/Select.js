import React from 'react';
import cn from 'classnames';
import ReactSelect from 'react-select';

import { Typography } from 'components/shared/Typography';

import styles from './Select.styles.scss';

const MODES = {
  regular: 'regular',
  portal: 'portal',
};

const Control = React.memo(
  ({
    children,
    innerProps,
    menuIsOpen,
    innerRef,
    variant,
    haveError,
    isFocused,
    ...restProps
  }) => {
    return (
      <div
        {...innerProps}
        ref={innerRef}
        className={cn(styles.control, {
          [styles.controlFocused]: isFocused,
          [styles.controlHaveError]: haveError,
          [styles.activeControl]: menuIsOpen,
        })}
      >
        {children}
      </div>
    );
  },
);

const Placeholder = React.memo(({ innerProps, children }) => (
  <Typography {...innerProps} variant="body2" className={styles.placeholder}>
    {children}
  </Typography>
));

const ValueContainer = React.memo(({ innerProps, children, ...restProps }) => (
  <div {...innerProps} className={styles.valueContainer}>
    {children}
  </div>
));

const Menu = React.memo(({ innerProps, children }) => (
  <div {...innerProps} className={styles.menu}>
    {children}
  </div>
));

const MenuList = React.memo(({ innerProps, children }) => (
  <div {...innerProps} className={styles.menuList}>
    {children}
  </div>
));

const Option = React.memo(({ innerProps, children, isSelected, isMulti }) => (
  <Typography {...innerProps} variant="body2" className={styles.option}>
    {children}
  </Typography>
));

const NoOptionsMessage = React.memo(({ noOption, innerProps }) => {
  if (noOption) {
    return <div className={styles.noOption}>{noOption}</div>;
  }

  return <Typography {...innerProps}>No Options</Typography>;
});

const Select = ({
  className,
  multi,
  placeholder,
  options,
  variant,
  haveError,
  defaultMenuIsOpen,
  mode,
  isEmployee,
  noOption,
  ...props
}) => {
  const { selectRef } = props;

  const components = {
    Control: (controlProps) => (
      <Control variant={variant} haveError={haveError} {...controlProps} />
    ),
    NoOptionsMessage: (controlProps) => (
      <NoOptionsMessage {...controlProps} noOption={noOption} />
    ),
    Placeholder,
    Option,
    Menu,
    MenuList,
    ValueContainer,
    IndicatorSeparator: null,
  };
  const commonProps = {
    isMulti: multi,
    placeholder: placeholder,
    options: options,
    components,
    defaultMenuIsOpen: defaultMenuIsOpen,
  };

  switch (MODES[mode]) {
    case MODES.regular:
      return (
        <ReactSelect
          {...commonProps}
          {...props}
          className={cn(styles.container, className)}
        />
      );
    case MODES.portal:
      return (
        <ReactSelect
          {...commonProps}
          {...props}
          menuPortalTarget={document.body}
          menuPosition="fixed"
          ref={selectRef}
          className={cn(styles.container, className)}
          styles={{
            menuPortal: (base) => ({
              ...base,
              zIndex: 999,
            }),
          }}
        />
      );
  }
};

Select.defaultProps = {
  mode: 'portal',
};

export default React.memo(Select);
