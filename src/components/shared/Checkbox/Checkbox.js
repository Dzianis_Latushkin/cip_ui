import React from 'react';
import cn from 'classnames';

import { Typography } from 'components/shared/Typography';

import styles from './Checkbox.styles.scss';

const Checkbox = ({
  checked,
  label,
  className,
  classNameCheckboxField,
  id,
  ...restProps
}) => (
  <div className={cn(styles.checkboxField, classNameCheckboxField)}>
    <input
      type="checkbox"
      id={id}
      className={styles.customCheckbox}
      {...restProps}
    />
    <label htmlFor={id}>
      <div className={styles.iconWrapper}>
        <div
          className={cn(
            styles.iconDisabled,
            { [styles.icon]: checked },
            className,
          )}
        />
      </div>
    </label>

    {label && (
      <Typography
        component={'label'}
        variant="subtitle"
        htmlFor={id}
        className={styles.label}
      >
        {label}
      </Typography>
    )}
  </div>
);

export default React.memo(Checkbox);
