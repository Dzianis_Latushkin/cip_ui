import React from 'react';

import styles from './Logo.styles.scss';

const Logo = ({ className, innerClassName }) => (
  // <div className={styles.logo}>CIP</div>
  <div className={styles.logoWrap}>
    <div className={styles.logoSquare}>CIP</div>
    <div className={styles.logoName}>Customer interview preparation</div>
  </div>
);

export default React.memo(Logo);
