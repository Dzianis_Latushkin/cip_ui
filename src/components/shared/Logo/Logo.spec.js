import React from 'react';

import Logo from './Logo';

describe('Logo component', () => {
  it('Should render Login component', () => {
    const component = shallow(<Logo />);
    expect(component.exists('.logoWrap')).toBe(true);
    expect(component.exists('.logoSquare')).toBe(true);
    expect(component.exists('.logoName')).toBe(true);
  });
});
