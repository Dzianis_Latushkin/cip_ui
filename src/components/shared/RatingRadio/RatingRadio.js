import React from 'react';
import cn from 'classnames';

import { Typography } from 'components/shared/Typography';

import styles from './RatingRadio.styles.scss';

const RatingRadio = ({
  hover,
  className,
  inputClassName,
  checked,
  value,
  ...restProps
}) => (
  <div className={styles.radio}>
    <input className={styles.input} type="radio" value={value} {...restProps} />
    <Typography
      className={cn(
        styles.radioWrapper,
        { [styles.checked]: checked },
        inputClassName,
      )}
    >
      {value}
    </Typography>
  </div>
);

export default React.memo(RatingRadio);
