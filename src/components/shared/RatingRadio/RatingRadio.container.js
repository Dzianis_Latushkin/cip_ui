import React, { useState } from 'react';

import RatingRadio from './RatingRadio';

const RatingRadioContainer = ({ ...restProps }) => {
  const [hover, setHover] = useState(false);

  const onMouseOver = () => {
    setHover(true);
  };

  const onMouseOut = () => {
    setHover(false);
  };

  return (
    <RatingRadio
      {...restProps}
      hover={hover}
      onMouseOver={onMouseOver}
      onMouseOut={onMouseOut}
    />
  );
};

export default React.memo(RatingRadioContainer);
