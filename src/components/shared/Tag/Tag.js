import React from 'react';
import cn from 'classnames';

import { Typography } from 'components/shared/Typography';

import styles from './Tag.styles.scss';

const VARIANTS = {
  default: styles.default,
  success: styles.success,
  error: styles.error,
  info: styles.info,
};

const Tag = ({ className, children, variant }) => (
  <div className={cn(styles.tag, VARIANTS[variant], className)}>
    <Typography variant="label" className={styles.text}>
      {children}
    </Typography>
  </div>
);

Tag.defaultProps = {
  variant: 'default',
};

export default React.memo(Tag);
