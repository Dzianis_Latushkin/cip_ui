import React from 'react';
import cn from 'classnames';

import styles from './Typography.styles.scss';

const VARIANTS = {
  h1: styles.h1,
  h2: styles.h2,
  h3: styles.h3,
  h4: styles.h4,
  subtitle: styles.subtitle,
  subtitle1: styles.subtitle1,
  body: styles.body,
  link: styles.link,
  label: styles.label,
};

const Typography = ({
  className,
  children,
  variant,
  component,
  title,
  family,
  ...restProps
}) => {
  const Component = component || 'p';

  return (
    <Component
      title={title}
      className={cn(styles.typography, VARIANTS[variant], className)}
      {...restProps}
    >
      {children}
    </Component>
  );
};

Typography.defaultProps = {
  variant: 'body',
};

export default React.memo(Typography);
