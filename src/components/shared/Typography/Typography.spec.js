import React from 'react';
import Typography from './Typography';

describe('Typography component', () => {
  it('should render Typography component default props', () => {
    const component = shallow(<Typography children="Test text" />);
    expect(component).toMatchSnapshot();
  });
  it('should render Typography component with props', () => {
    const component = shallow(
      <Typography variant="h3" component="h3" children="Test h3 title" />,
    );
    expect(component).toMatchSnapshot();
  });
});
