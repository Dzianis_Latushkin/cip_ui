import React from 'react';

import Loading from './Loading';

const setUp = (props) => shallow(<Loading {...props} />);

describe('Loading component', () => {
  it('Should render Loading component without props', () => {
    const component = setUp();
    expect(component.exists('.loading')).toBe(true);
  });

  it('Should have 4 inner divs', () => {
    const component = setUp();
    const innerDivs = component.find('.inner');
    expect(innerDivs.length).toBe(4);
  });

  it('Should render Loading component with props', () => {
    const color = '#ffffff';
    const size = '50px';
    const component = setUp({ size, color });

    const loader = component.find('.loading');
    expect(loader.prop('style')).toHaveProperty('width', `${size}`);

    const innerDiv = component.find('.inner').first();
    expect(innerDiv.prop('style')).toHaveProperty(
      'border',
      `2px solid ${color}`,
    );
  });
});
