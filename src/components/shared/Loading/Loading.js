import React from 'react';
import cn from 'classnames';

import { OFFSET } from './Loading.constants';

import styles from './Loading.styles.scss';

const VARIANTS = {
  primary: styles.primary,
  default: styles.default,
};

const Loading = ({ className, size, variant, innerClassName }) => (
  <div
    className={cn(styles.loading, className)}
    style={{ width: size, height: size }}
  >
    <div
      className={cn(styles.inner, innerClassName, VARIANTS[variant])}
      style={{
        width: size - OFFSET * 2,
        height: size - OFFSET * 2,
        margin: OFFSET,
      }}
    />
    <div
      className={cn(styles.inner, innerClassName, VARIANTS[variant])}
      style={{
        width: size - OFFSET * 2,
        height: size - OFFSET * 2,
        margin: OFFSET,
      }}
    />
    <div
      className={cn(styles.inner, innerClassName, VARIANTS[variant])}
      style={{
        width: size - OFFSET * 2,
        height: size - OFFSET * 2,
        margin: OFFSET,
      }}
    />
    <div
      className={cn(styles.inner, innerClassName, VARIANTS[variant])}
      style={{
        width: size - OFFSET * 2,
        height: size - OFFSET * 2,
        margin: OFFSET,
      }}
    />
  </div>
);

Loading.defaultProps = {
  size: 20,
  variant: 'default',
};

export default React.memo(Loading);
