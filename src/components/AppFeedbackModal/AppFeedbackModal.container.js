import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';

import * as api from 'api/requests/requests';

import AppFeedbackModal from './AppFeedbackModal';

const AppFeedbackModalContainer = ({ onClose, ...restProps }) => {
  const { t } = useTranslation('appFeedbackModal');
  const [loading, setLoading] = useState(false);

  const handleSubmit = (values, actions) => {
    setLoading(true);
    api
      .postReport({ message: values.feedback })
      .then((res) => {
        setLoading(false);
        onClose();
      })
      .catch((err) => {
        setLoading(false);
        actions.setFieldError('feedback', err);
      });
  };

  return (
    <AppFeedbackModal
      loading={loading}
      t={t}
      onClose={onClose}
      onSubmit={handleSubmit}
      {...restProps}
    />
  );
};

export default React.memo(AppFeedbackModalContainer);
