import * as Yup from 'yup';

export const getValidationSchema = (t) =>
  Yup.object().shape({
    feedback: Yup.string()
      .required(t('feedbackRequiredError'))
      .test('isValid', t('feedbackValidationError'), (value) => {
        if (value) {
          return value.replace(/\s/g, '').length;
        }

        return true;
      }),
  });

export const initialValues = {
  feedback: '',
};
