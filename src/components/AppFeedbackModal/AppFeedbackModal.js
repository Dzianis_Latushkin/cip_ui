import React from 'react';
import { Formik, Form, Field } from 'formik';

import {
  initialValues,
  getValidationSchema,
} from './AppFeedbackModal.formConfig';

import { Modal } from 'components/shared/Modal';
import { Button } from 'components/shared/Button';
import { Typography } from 'components/shared/Typography';

import { InputField } from 'components/FormikFields';

import styles from './AppFeedbackModal.styles.scss';

const AppFeedbackModal = ({
  loading,
  t,
  onClose,
  onSubmit,
  isAppFeedbackModalOpen,
}) => (
  <Modal
    open={isAppFeedbackModalOpen}
    centered
    className={styles.modal}
    onClose={onClose}
  >
    <Typography variant="h1" className={styles.title}>
      {t('title')}
    </Typography>
    <Formik
      validationSchema={getValidationSchema(t)}
      validateOnMount
      initialValues={initialValues}
      onSubmit={onSubmit}
    >
      {({ values, errors, touched }) => (
        <Form className={styles.form}>
          <Field
            label={t('text')}
            name="feedback"
            placeholder={t('namePlaceholder')}
            component={InputField}
            multiline={true}
            inputClassName={styles.feedbackField}
          />
          <div className={styles.buttonBar}>
            <Button type="submit" loading={loading} className={styles.button}>
              {t('sendButton')}
            </Button>
            <Button
              variant="outlined"
              onClick={onClose}
              className={styles.button}
            >
              {t('cancelButton')}
            </Button>
          </div>
        </Form>
      )}
    </Formik>
  </Modal>
);

AppFeedbackModal.defaultProps = {
  isOpen: false,
};

export default React.memo(AppFeedbackModal);
