export const convertDataToApi = (values) => {
  return (
    values && {
      email: values.email,
      name: values.name,
      engLevel: values.engLevel,
      SMGlink: values.SMGlink || '',
      WODlink: values.WODlink || '',
    }
  );
};
