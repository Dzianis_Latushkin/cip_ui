import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';

import { DATA_VARIANTS } from './EmployeesCompareModal.constants';

import * as api from 'api/requests/requests';

import { showToast } from 'helpers/showToast';

import { convertDataToApi } from './EmployeesCompareModal.helpers';

import EmployeesCompareModal from './EmployeesCompareModal';

const EmployeesCompareModalContainer = ({
  addEmployeeInterviewPage,
  prevUserData,
  nextUserData,
  setEmployees,
  onClose,
  ...restProps
}) => {
  const { t } = useTranslation('employeesPage');
  const [tEnglish] = useTranslation('englishLevels');
  const [tNotifications] = useTranslation('notifications');

  const [loading, setLoading] = useState({
    prevDataLoading: false,
    nextDataLoading: false,
  });

  const handleSaveClick = (variant) => {
    const data = convertDataToApi(
      variant === DATA_VARIANTS.NEXT ? nextUserData : prevUserData,
    );

    setLoading({
      prevDataLoading: variant === DATA_VARIANTS.PREV,
      nextDataLoading: variant === DATA_VARIANTS.NEXT,
    });

    api
      .editEmployee(prevUserData._id, data)
      .then((res) => {
        setLoading({
          prevDataLoading: false,
          nextDataLoading: false,
        });

        setEmployees((prevState) => {
          if (addEmployeeInterviewPage) {
            return {
              ...prevState,
              data: {
                ...prevState.data,
                users: [
                  ...prevState.data.users,
                  {
                    label: res.data.user.name,
                    option: res.data.user._id,
                  },
                ],
              },
            };
          } else {
            return {
              ...prevState,
              data: {
                users: [
                  ...prevState.data.users.filter(
                    (user) => user._id !== res.data.user._id,
                  ),
                  res.data.user,
                ],
              },
            };
          }
        });
        showToast(tNotifications('editEmployeeNotification'));

        onClose();
      })
      .catch((err) => {
        console.log(err);
      });
  };

  return (
    <EmployeesCompareModal
      t={t}
      tEnglish={tEnglish}
      loading={loading}
      prevUserData={prevUserData}
      nextUserData={nextUserData}
      onSaveClick={handleSaveClick}
      onClose={onClose}
      {...restProps}
    />
  );
};

export default React.memo(EmployeesCompareModalContainer);
