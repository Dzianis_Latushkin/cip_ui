import React from 'react';

import { DATA_VARIANTS } from './EmployeesCompareModal.constants';

import { Modal } from 'components/shared/Modal';
import { Button } from 'components/shared/Button';
import { Typography } from 'components/shared/Typography';

import styles from './EmployeesCompareModal.styles.scss';

export const renderEmployeeData = (data, t) => (
  <>
    <div className={styles.dataRow}>
      <Typography className={styles.dataItem}>
        {t('compareEmployeesModal.emailLabel')}
      </Typography>
      <Typography>{data.email}</Typography>
    </div>
    <div className={styles.dataRow}>
      <Typography className={styles.dataItem}>
        {t('compareEmployeesModal.nameLabel')}
      </Typography>
      <Typography>{data.name}</Typography>
    </div>
    <div className={styles.dataRow}>
      <Typography className={styles.dataItem}>
        {t('compareEmployeesModal.englishLevelLable')}
      </Typography>
      <Typography>{data.engLevel}</Typography>
    </div>
    <div className={styles.dataRow}>
      <Typography className={styles.dataItem}>
        {t('compareEmployeesModal.wodLinkLabel')}
      </Typography>
      <Typography>{data.WODlink}</Typography>
    </div>
    <div className={styles.dataRow}>
      <Typography className={styles.dataItem}>
        {t('compareEmployeesModal.smgLinkLabel')}
      </Typography>
      <Typography>{data.SMGlink}</Typography>
    </div>
  </>
);

const EmployeesCompareModal = ({
  t,
  tEnglish,
  loading,
  isOpen,
  prevUserData,
  nextUserData,
  onSaveClick,
  onClose,
}) => (
  <Modal open={isOpen} centered className={styles.modal} onClose={onClose}>
    <Typography variant="h1" className={styles.title}>
      {t('compareEmployeesModal.title')}
    </Typography>
    <div className={styles.managersList}>
      <Typography variant="h3">
        {t('compareEmployeesModal.managersList')}
      </Typography>
      <div className={styles.managers}>
        {prevUserData &&
          prevUserData.managers.map((manager) => (
            <Typography
              variant="h4"
              key={manager._id}
              className={styles.managerName}
            >
              {manager.name};
            </Typography>
          ))}
      </div>
    </div>

    <div className={styles.employeesData}>
      <div className={styles.newData}>
        <Typography variant="subtitle" className={styles.employeeTitle}>
          {t('compareEmployeesModal.newData')}
        </Typography>
        {nextUserData && renderEmployeeData(nextUserData, t)}
      </div>
      <div className={styles.oldData}>
        <Typography variant="subtitle" className={styles.employeeTitle}>
          {t('compareEmployeesModal.currentData')}
        </Typography>
        {prevUserData && renderEmployeeData(prevUserData, t)}
      </div>
    </div>
    <div className={styles.buttonBar}>
      <div className={styles.buttonBarWrap}>
        <Button
          variant="default"
          disabled={loading.prevDataLoading}
          loading={loading.nextDataLoading}
          onClick={() => onSaveClick(DATA_VARIANTS.NEXT)}
          className={styles.button}
        >
          {t('compareEmployeesModal.updateButton')}
        </Button>
      </div>
      <div className={styles.buttonBarWrap}>
        <Button
          variant="default"
          disabled={loading.nextDataLoading}
          loading={loading.prevDataLoading}
          onClick={() => onSaveClick(DATA_VARIANTS.PREV)}
          className={styles.button}
        >
          {t('compareEmployeesModal.currentDataButton')}
        </Button>
      </div>
    </div>
    <Button
      variant="outlined"
      disabled={loading.nextDataLoading || loading.prevDataLoading}
      className={styles.button}
      onClick={onClose}
      data-testid="cancelButton"
    >
      {t('employeesModal.cancelButton')}
    </Button>
  </Modal>
);

export default EmployeesCompareModal;
