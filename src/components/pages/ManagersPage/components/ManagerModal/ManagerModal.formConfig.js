import * as Yup from 'yup';

export const getValidationSchema = (t) =>
  Yup.object().shape({
    email: Yup.string()
      .email(t('managerModal.emailValidationError'))
      .required(t('managerModal.requiredEmailValidationError')),
    name: Yup.string()
      .required(t('managerModal.requiredNameValidationError'))
      .test('isValid', t('managerModal.nameSpacesError'), (value) => {
        if (value) {
          return value.replace(/\s/g, '').length;
        }

        return true;
      }),
  });

export const initialValues = {
  email: '',
  name: '',
};
