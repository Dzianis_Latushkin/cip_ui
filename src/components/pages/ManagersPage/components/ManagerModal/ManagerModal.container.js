import React, { useState } from 'react';

import * as api from 'api/requests/requests';

import { showToast } from 'helpers/showToast';

import ManagerModal from './ManagerModal';
import { useTranslation } from 'react-i18next';

const ManagerModalContainer = ({
  t,
  setManagers,
  onClose,
  openConfirm,
  ...restProps
}) => {
  const [loading, setLoading] = useState(false);
  const [tNotifications] = useTranslation('notifications');

  const handleSubmit = (values, actions) => {
    setLoading(true);

    api
      .addUser({ email: values.email, name: values.name })
      .then((res) => {
        setLoading(false);

        setManagers((prevState) => {
          return {
            ...prevState,
            data: {
              ...prevState.data,
              users: [...prevState.data.users, res.data.user],
            },
          };
        });

        showToast(tNotifications('addManagerNotification'));

        onClose();
      })
      .catch((err) => {
        setLoading(false);
        actions.setFieldError('email', err);
      });
  };

  return (
    <ManagerModal
      t={t}
      loading={loading}
      onSubmit={handleSubmit}
      onClose={onClose}
      {...restProps}
    />
  );
};

export default React.memo(ManagerModalContainer);
