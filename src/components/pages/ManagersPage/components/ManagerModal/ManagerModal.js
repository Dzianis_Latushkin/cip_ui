import React from 'react';
import { Formik, Form, Field } from 'formik';

import { getValidationSchema, initialValues } from './ManagerModal.formConfig';

import { Modal } from 'components/shared/Modal';
import { Button } from 'components/shared/Button';
import { Typography } from 'components/shared/Typography';

import { InputField } from 'components/FormikFields';

import styles from './ManagerModal.styles.scss';

const ManagerModal = ({ loading, isOpen, t, onSubmit, onClose }) => (
  <Modal open={isOpen} centered className={styles.modal} onClose={onClose}>
    <Typography variant="h1" className={styles.title}>
      {t('managerModal.title')}
    </Typography>
    <Formik
      validateOnMount
      onSubmit={onSubmit}
      validationSchema={getValidationSchema(t)}
      initialValues={initialValues}
    >
      {({ values, errors }) => (
        <Form className={styles.form}>
          <Field
            label={t('managerModal.emailLabel')}
            name="email"
            errorId={'email-error'}
            placeholder={t('managerModal.emailPlaceholder')}
            component={InputField}
            inputClassName={styles.inputField}
          />
          <Field
            label={t('managerModal.nameLabel')}
            name="name"
            errorId={'name-error'}
            placeholder={t('managerModal.namePlaceholder')}
            component={InputField}
            inputClassName={styles.inputField}
          />
          <div className={styles.buttonBar}>
            <Button
              type="submit"
              variant="default"
              loading={loading}
              className={styles.button}
            >
              {t('managerModal.createButton')}
            </Button>
            <Button
              variant="outlined"
              className={styles.button}
              onClick={onClose}
            >
              {t('managerModal.cancelButton')}
            </Button>
          </div>
        </Form>
      )}
    </Formik>
  </Modal>
);

export default ManagerModal;
