import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';

import * as api from 'api/requests/requests';

import { showToast } from 'helpers/showToast';

import { useModal } from 'hooks/useModal';
import { useFetch } from 'hooks/useFetch';

import { MainLayout } from 'components/Layout';

import { ManagerModal } from './components/ManagerModal';
import { ConfirmModal } from 'components/ConfirmModal';

import ManagersPage from './ManagersPage';

const ManagersPageContainer = () => {
  const { t } = useTranslation('managersPage');
  const [tNotifications] = useTranslation('notifications');

  const [confirmLoading, setConfrimLoading] = useState(false);

  const [isManagerModalOpen, openManagerModal, closeManagerModal] = useModal(
    {},
  );
  const [isConfirmModalOpen, openConfirmModal, closeConfirmModal, modalData] =
    useModal({});

  const {
    data: { users },
    loading,
    setState: setManagers,
  } = useFetch({ defaultData: [], fetcher: api.getUsers, immediate: true }, {});

  const managers = users || [];

  const handleAddManagerClick = () => {
    openManagerModal();
  };

  const handleRemoveManagerClick = (id) => {
    openConfirmModal({
      titleText: t('confirmModalTitle'),
      contentText: t('confirmModalContent'),
      onConfirmClick: () => handleRemoveManager(id),
      onCancelClick: () => closeConfirmModal(),
    });
  };

  const handleRemoveManager = (id) => {
    setConfrimLoading(true);

    api
      .deleteUser(id)
      .then((res) => {
        setManagers((prevState) => {
          return {
            ...prevState,
            data: {
              ...prevState.data,
              users: prevState.data.users.filter((user) => user._id !== id),
            },
          };
        });
        setConfrimLoading(false);
        closeConfirmModal();

        showToast(tNotifications('deleteManagerNotification'));
      })
      .catch(() => {
        setConfrimLoading(false);
      });
  };

  return (
    <MainLayout loading={loading} pageTitle={t('title')}>
      <ManagersPage
        t={t}
        managers={managers}
        onAddManagerClick={handleAddManagerClick}
        onRemoveManagerClick={handleRemoveManagerClick}
      />
      <ConfirmModal
        {...modalData}
        loading={confirmLoading}
        isOpen={isConfirmModalOpen}
        onClose={closeConfirmModal}
      />
      <ManagerModal
        t={t}
        isOpen={isManagerModalOpen}
        setManagers={setManagers}
        onClose={closeManagerModal}
      />
    </MainLayout>
  );
};

export default React.memo(ManagersPageContainer);
