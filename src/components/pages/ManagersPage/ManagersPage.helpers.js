import _upperFirst from 'lodash/upperFirst';

export const normalizeData = (users) => {
  return users.map((user) => ({
    id: user._id,
    unit: user.unit,
    name: `${_upperFirst(user.name)} ${_upperFirst(user.surname)}`,
    email: user.email,
    employeeCount: user.employees.length,
  }));
};
