export const getTableColumns = (t) => [
  {
    key: 'name',
    label: t('tableNameLabel'),
    minWidth: '254px',
    maxWidth: '254px',
    sortable: false,
  },
  {
    key: 'email',
    label: t('tableEmailLabel'),
    minWidth: '320px',
    maxWidth: '320px',
    sortable: false,
  },
  {
    key: 'unit',
    label: t('tableUnitLabel'),
    minWidth: '170px',
    maxWidth: '170px',
    sortable: false,
  },
  {
    key: 'employeeCount',
    label: t('tableEmployeesCountLabel'),
    minWidth: '100px',
    maxWidth: '100px',
    sortable: false,
  },
  {
    key: 'controls',
    minWidth: '50px',
    maxWidth: '50px',
    sortable: false,
  },
];
