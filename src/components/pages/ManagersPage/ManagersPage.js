import React from 'react';
import cn from 'classnames';
import { Tooltip } from 'react-tippy';

import { normalizeData } from './ManagersPage.helpers';

import { getTableColumns } from './ManagersPage.config';

import { Icon } from 'components/shared/Icon';
import { Table } from 'components/shared/Table';
import { Button } from 'components/shared/Button';
import { Typography } from 'components/shared/Typography';

import styles from './ManagersPage.styles.scss';

const cellRenderer = (
  { item, key, minWidth, maxWidth, onSortClick },
  DefaultCell,
  { onRemoveManagerClick },
  t,
) => {
  switch (key) {
    case 'unit':
    case 'name': {
      return (
        <div className={styles.item} key={key} style={{ minWidth, maxWidth }}>
          <Typography
            data-testid="managers-name"
            title={item[key]}
            className={styles.itemText}
          >
            {item[key]}
          </Typography>
        </div>
      );
    }
    case 'employeeCount': {
      return (
        <div className={styles.item} key={key} style={{ minWidth, maxWidth }}>
          <Typography title={item[key]} className={styles.itemText}>
            {item[key]}
          </Typography>
        </div>
      );
    }
    case 'email': {
      return (
        <div className={styles.item} key={key} style={{ minWidth, maxWidth }}>
          <Typography
            data-testid="managers-email"
            title={item[key]}
            className={styles.itemText}
          >
            {item[key]}
          </Typography>
        </div>
      );
    }
    case 'controls': {
      return (
        <div
          className={cn(styles.item, styles.iconItem)}
          key={key}
          style={{ minWidth, maxWidth }}
        >
          <Tooltip
            html={
              <div className={styles.tooltip}>
                <div
                  data-testid="mark-as-remove"
                  className={styles.tooltipItem}
                  onClick={() => onRemoveManagerClick(item.id)}
                >
                  <Icon name="delete" className={styles.tooltipItemIcon} />
                  <Typography variant="body" className={styles.tooltipItemText}>
                    {t('removeEmployeeTooltip')}
                  </Typography>
                </div>
              </div>
            }
            position="bottom-end"
            trigger="click"
          >
            <Icon data-testid="moreIcon" name="more" />
          </Tooltip>
        </div>
      );
    }
    default: {
      return (
        <DefaultCell key={key} minWidth={minWidth} maxWidth={maxWidth}>
          {item[key] ? item[key] : '-'}
        </DefaultCell>
      );
    }
  }
};

const ManagersPage = ({
  t,
  managers,
  onManagerClick,
  onAddManagerClick,
  onRemoveManagerClick,
}) => (
  <div className={styles.page}>
    <Button
      data-testid="addButton"
      onClick={onAddManagerClick}
      className={styles.button}
      endIcon={<Icon name="plus" />}
    >
      {t('addButton')}
    </Button>
    <Table
      data-testid="managersTable"
      className={styles.table}
      columns={getTableColumns(t)}
      data={normalizeData(managers)}
      cellRenderer={(props, DefaultCell) =>
        cellRenderer(props, DefaultCell, { onRemoveManagerClick }, t)
      }
    />
  </div>
);

export default React.memo(ManagersPage);
