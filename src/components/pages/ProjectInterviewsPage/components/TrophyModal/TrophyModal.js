import React from 'react';

import { Modal } from 'components/shared/Modal';
import { Button } from 'components/shared/Button';
import { Typography } from 'components/shared/Typography';

import styles from './TrophyModal.styles.scss';

const TrophyModal = ({ t, isOpen, onClose, navigate }) => (
  <Modal open={isOpen} centered className={styles.modal} onClose={onClose}>
    <Typography variant="h1" className={styles.title}>
      {'Thanks for interview creating'}
    </Typography>
    <Button
      variant="outlined"
      className={styles.button}
      onClick={() => {
        onClose();
        navigate('/project-interviews');
      }}
    >
      {t('trophyModal.continueButton')}
    </Button>
  </Modal>
);

export default TrophyModal;
