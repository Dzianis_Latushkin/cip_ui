import React from 'react';
import { useNavigate } from 'react-router-dom';

import TrophyModal from './TrophyModal';

const TrophyModalContainer = ({ t, onClose, isOpen }) => {
  const navigate = useNavigate();

  return (
    <TrophyModal t={t} isOpen={isOpen} onClose={onClose} navigate={navigate} />
  );
};

export default React.memo(TrophyModalContainer);
