function toInterviewDate(interviewDate) {
  const date = new Date(+interviewDate);
  let day = date.getDate();
  let month = date.getMonth() + 1;
  let year = date.getFullYear();

  if (day < 10) {
    day = `0${day}`;
  }

  if (month < 10) {
    month = `0${month}`;
  }

  return `${day}.${month}.${year}`;
}

export const normalizeData = (interviews, t) => {
  return interviews.map((interview) => ({
    id: interview._id,
    status: interview.status,
    title: interview.title,
    employee: interview.creator.name,
    project: interview.project?.title || t('noProjectTitle'),
    date: toInterviewDate(interview.createdAt),
  }));
};
