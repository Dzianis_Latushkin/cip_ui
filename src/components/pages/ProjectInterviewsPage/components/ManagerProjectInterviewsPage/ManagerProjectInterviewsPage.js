import React from 'react';
import cn from 'classnames';
import { Tooltip } from 'react-tippy';

import { normalizeData } from './ManagerProjectInterviewsPage.helpers';

import { getTableColumns } from './ManagerProjectInterviewsPage.config';

import { Tag } from 'components/shared/Tag';
import { Icon } from 'components/shared/Icon';
import { Table } from 'components/shared/Table';
import { Tabs, Tab } from 'components/shared/Tabs';
import { Typography } from 'components/shared/Typography';
import { MainLayout } from 'components/Layout';

import styles from './ManagerProjectInterviewsPage.styles.scss';

const renderStatusTag = (status, t) => {
  switch (status) {
    case 'pending':
      return <Tag variant="info">{t('pendingTag')}</Tag>;
    case 'failed':
      return <Tag variant="error">{t('failedTag')}</Tag>;
    case 'successful':
      return <Tag variant="success">{t('successfulTag')}</Tag>;
    case 'dismissed':
      return <Tag variant="default">{t('dismissedTag')}</Tag>;
  }
};

const cellRenderer = (
  { item, key, minWidth, maxWidth, onSortClick },
  DefaultCell,
  { onEditInterviewClick, onViewInterviewClick },
  t,
) => {
  switch (key) {
    case 'status': {
      return (
        <div className={styles.item} key={key} style={{ minWidth, maxWidth }}>
          {renderStatusTag(item[key], t)}
        </div>
      );
    }
    case 'title': {
      return (
        <div className={styles.item} key={key} style={{ minWidth, maxWidth }}>
          <Typography title={item[key]} className={styles.itemText}>
            {item[key]}
          </Typography>
        </div>
      );
    }
    case 'employee': {
      return (
        <div className={styles.item} key={key} style={{ minWidth, maxWidth }}>
          <Typography title={item[key]} className={styles.itemText}>
            {item[key]}
          </Typography>
        </div>
      );
    }
    case 'project': {
      return (
        <div className={styles.item} key={key} style={{ minWidth, maxWidth }}>
          <Typography title={item[key]} className={styles.itemText}>
            {item[key]}
          </Typography>
        </div>
      );
    }
    case 'date': {
      return (
        <div className={styles.item} key={key} style={{ minWidth, maxWidth }}>
          <Typography title={item[key]} className={styles.itemText}>
            {item[key]}
          </Typography>
        </div>
      );
    }
    case 'controls': {
      return (
        <div
          className={cn(styles.item, styles.iconItem)}
          key={key}
          style={{ minWidth, maxWidth }}
        >
          <Tooltip
            html={
              <div className={styles.tooltip}>
                <div
                  className={styles.tooltipItem}
                  onClick={() => onViewInterviewClick(item.id)}
                >
                  <Icon
                    name="viewTemplate"
                    className={styles.tooltipItemIcon}
                  />
                  <Typography variant="body" className={styles.tooltipItemText}>
                    {t('viewInterviewTooltip')}
                  </Typography>
                </div>

                <div
                  className={styles.tooltipItem}
                  onClick={() => onEditInterviewClick(item.id)}
                >
                  <Icon name="pencil" className={styles.tooltipItemIcon} />
                  <Typography variant="body" className={styles.tooltipItemText}>
                    {t('editInterviewTooltip')}
                  </Typography>
                </div>
              </div>
            }
            position="bottom-end"
            trigger="click"
          >
            <Icon data-testid="moreIcon" name="more" />
          </Tooltip>
        </div>
      );
    }
    default: {
      return (
        <DefaultCell key={key} minWidth={minWidth} maxWidth={maxWidth}>
          {item[key] ? item[key] : '-'}
        </DefaultCell>
      );
    }
  }
};

const noProjectInterviewsMessage = (message, t) => (
  <div className={styles.infoMessage}>
    <div className={styles.infoMessageIconWrap}>
      <Icon name="info" className={styles.infoMessageIcon} />
    </div>
    <Typography variant="h3" component="h3" className={styles.infoMessageText}>
      {t(message)}
    </Typography>
  </div>
);

const ManagerProjectInterviewsPage = ({
  t,
  loading,
  privateInterviews,
  publishedInterviews,
  onEditInterviewClick,
  onViewInterviewClick,
}) => {
  return (
    <MainLayout loading={loading} pageTitle={t('title')}>
      <Tabs>
        <Tab label={t('privateTab')}>
          {privateInterviews.length ? (
            <Table
              className={styles.table}
              onRowClick={(item, e) => {
                e.target.tagName !== 'svg' && e.target.tagName !== 'path'
                  ? onViewInterviewClick(item.id)
                  : null;
              }}
              columns={getTableColumns(t)}
              data={normalizeData(privateInterviews, t)}
              cellRenderer={(props, DefaultCell) =>
                cellRenderer(
                  props,
                  DefaultCell,
                  { onEditInterviewClick, onViewInterviewClick },
                  t,
                )
              }
            />
          ) : (
            noProjectInterviewsMessage('noProjectInterviewsText', t)
          )}
        </Tab>
        <Tab label={t('publishedTab')}>
          {publishedInterviews.length ? (
            <Table
              className={styles.table}
              onRowClick={(item, e) => {
                e.target.tagName !== 'svg' && e.target.tagName !== 'path'
                  ? onViewInterviewClick(item.id)
                  : null;
              }}
              columns={getTableColumns(t)}
              data={normalizeData(publishedInterviews, t)}
              cellRenderer={(props, DefaultCell) =>
                cellRenderer(
                  props,
                  DefaultCell,
                  { onEditInterviewClick, onViewInterviewClick },
                  t,
                )
              }
            />
          ) : (
            noProjectInterviewsMessage('noProjectInterviewsText', t)
          )}
        </Tab>
      </Tabs>
    </MainLayout>
  );
};

export default React.memo(ManagerProjectInterviewsPage);
