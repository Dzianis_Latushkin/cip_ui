import React from 'react';
import { useNavigate } from 'react-router-dom';
import { useTranslation } from 'react-i18next';

import * as api from 'api/requests/requests';

import { useFetch } from 'hooks/useFetch';

import ManagerProjectInterviewsPage from './ManagerProjectInterviewsPage';

const ManagerProjectInterviewsPageContainer = () => {
  const { t } = useTranslation('managerProjectInterviewsPage');

  const navigate = useNavigate();

  const {
    data: { projectInterviews },
    loading,
  } = useFetch(
    { defaultData: [], fetcher: api.getProjectInterviews, immediate: true },
    {},
  );

  let privateInterviews = projectInterviews || [];
  let publishedInterviews = projectInterviews || [];

  privateInterviews = privateInterviews.filter(
    (interview) => interview.published === false,
  );

  publishedInterviews = publishedInterviews.filter(
    (interview) => interview.published === true,
  );

  const handleEditInterviewClick = (id) => {
    navigate(`/project-interview/${id}/edit`);
  };

  const handleViewInterviewClick = (id) => {
    navigate(`/project-interview/${id}/view`);
  };

  return (
    <ManagerProjectInterviewsPage
      t={t}
      loading={loading}
      privateInterviews={privateInterviews}
      publishedInterviews={publishedInterviews}
      onEditInterviewClick={handleEditInterviewClick}
      onViewInterviewClick={handleViewInterviewClick}
    />
  );
};

export default React.memo(ManagerProjectInterviewsPageContainer);
