import React from 'react';
import cn from 'classnames';
import { Tooltip } from 'react-tippy';

import { normalizeData } from './EmployeeProjectInterviewsPage.helpers';

import { getTableColumns } from './EmployeeProjectInterviewsPage.config';

import { Tag } from 'components/shared/Tag';
import { Icon } from 'components/shared/Icon';
import { Table } from 'components/shared/Table';
import { Button } from 'components/shared/Button';
import { Typography } from 'components/shared/Typography';

import { MainLayout } from 'components/Layout';

import styles from './EmployeeProjectInterviewsPage.styles.scss';

const renderStatusTag = (status, t) => {
  switch (status) {
    case 'pending':
      return <Tag variant="info">{t('pendingTag')}</Tag>;
    case 'failed':
      return <Tag variant="error">{t('failedTag')}</Tag>;
    case 'successful':
      return <Tag variant="success">{t('successfulTag')}</Tag>;
    case 'dismissed':
      return <Tag variant="default">{t('dismissedTag')}</Tag>;
  }
};

const cellRenderer = (
  { item, key, minWidth, maxWidth },
  DefaultCell,
  { onRemoveInterviewClick, onEditInterviewClick, onViewInterviewClick },
  t,
) => {
  switch (key) {
    case 'status': {
      return (
        <div className={styles.item} key={key} style={{ minWidth, maxWidth }}>
          {renderStatusTag(item[key], t)}
        </div>
      );
    }
    case 'title': {
      return (
        <div className={styles.item} key={key} style={{ minWidth, maxWidth }}>
          <Typography title={item[key]} className={styles.itemText}>
            {item[key]}
          </Typography>
        </div>
      );
    }
    case 'project': {
      return (
        <div className={styles.item} key={key} style={{ minWidth, maxWidth }}>
          <Typography title={item[key]} className={styles.itemText}>
            {item[key]}
          </Typography>
        </div>
      );
    }
    case 'date': {
      return (
        <div className={styles.item} key={key} style={{ minWidth, maxWidth }}>
          <Typography title={item[key]} className={styles.itemText}>
            {item[key]}
          </Typography>
        </div>
      );
    }
    case 'controls': {
      return (
        <div
          className={cn(styles.item, styles.iconItem)}
          key={key}
          style={{ minWidth, maxWidth }}
        >
          <Tooltip
            html={
              <div className={styles.tooltip}>
                <div
                  className={styles.tooltipItem}
                  onClick={() => onViewInterviewClick(item.id)}
                >
                  <Icon
                    name="viewTemplate"
                    className={styles.tooltipItemIcon}
                  />
                  <Typography variant="body" className={styles.tooltipItemText}>
                    {t('viewInterviewTooltip')}
                  </Typography>
                </div>

                {!item.published && (
                  <>
                    <div
                      className={styles.tooltipItem}
                      onClick={() => onEditInterviewClick(item.id)}
                    >
                      <Icon name="pencil" className={styles.tooltipItemIcon} />
                      <Typography
                        variant="body"
                        className={styles.tooltipItemText}
                      >
                        {t('editInterviewTooltip')}
                      </Typography>
                    </div>

                    <div
                      className={styles.tooltipItem}
                      onClick={() => onRemoveInterviewClick(item.id)}
                    >
                      <Icon name="delete" className={styles.tooltipItemIcon} />
                      <Typography
                        variant="body"
                        className={styles.tooltipItemText}
                      >
                        {t('removeInterviewTooltip')}
                      </Typography>
                    </div>
                  </>
                )}
              </div>
            }
            position="bottom-end"
            trigger="click"
          >
            <Icon data-testid="moreIcon" name="more" />
          </Tooltip>
        </div>
      );
    }
    default: {
      return (
        <DefaultCell key={key} minWidth={minWidth} maxWidth={maxWidth}>
          {item[key] ? item[key] : '-'}
        </DefaultCell>
      );
    }
  }
};

const noProjectInterviewsMessage = (message, t) => (
  <div className={styles.infoMessage}>
    <div className={styles.infoMessageIconWrap}>
      <Icon name="info" className={styles.infoMessageIcon} />
    </div>
    <Typography variant="h3" component="h3" className={styles.infoMessageText}>
      {t(message)}
    </Typography>
  </div>
);

const EmployeeProjectInterviewsPage = ({
  t,
  interviews,
  loading,
  onAddInterviewClick,
  onRemoveInterviewClick,
  onEditInterviewClick,
  onViewInterviewClick,
}) => {
  return (
    <MainLayout loading={loading} pageTitle={'Projects Interviews'}>
      <Button
        data-testid="addButton"
        onClick={onAddInterviewClick}
        className={styles.button}
        endIcon={<Icon name="plus" />}
      >
        {t('addButton')}
      </Button>

      {interviews.length ? (
        <Table
          className={styles.table}
          onRowClick={(item, e) => {
            e.target.tagName !== 'svg' && e.target.tagName !== 'path'
              ? onViewInterviewClick(item.id)
              : null;
          }}
          columns={getTableColumns(t)}
          data={normalizeData(interviews)}
          cellRenderer={(props, DefaultCell) =>
            cellRenderer(
              props,
              DefaultCell,
              {
                onRemoveInterviewClick,
                onEditInterviewClick,
                onViewInterviewClick,
              },
              t,
            )
          }
        />
      ) : (
        noProjectInterviewsMessage('noProjectInterviewsText', t)
      )}
    </MainLayout>
  );
};

export default React.memo(EmployeeProjectInterviewsPage);
