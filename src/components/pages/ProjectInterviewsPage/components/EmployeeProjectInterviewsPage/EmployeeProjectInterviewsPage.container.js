import React, { useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import { useQuery } from 'hooks/useQuery';

import * as api from 'api/requests/requests';

import { showToast } from 'helpers/showToast';

import { useModal } from 'hooks/useModal';
import { useFetch } from 'hooks/useFetch';

import { ConfirmModal } from 'components/ConfirmModal';
import { TrophyModal } from '../TrophyModal';

import EmployeeProjectInterviewsPage from './EmployeeProjectInterviewsPage';

const EmployeeProjectInterviewsPageContainer = () => {
  const { t } = useTranslation('employeeProjectInterviewsPage');
  const [tNotifications] = useTranslation('notifications');

  const query = useQuery();
  const isTrophyModal = query.get('trophyModal');

  const [confirmLoading, setConfrimLoading] = useState(false);

  const [isConfirmModalOpen, openConfirmModal, closeConfirmModal, modalData] =
    useModal({});

  const [isTrophyModalOpen, openTrophyModal, closeTrophyModal] = useModal({});

  const navigate = useNavigate();

  const {
    data: { projectInterviews },
    loading,
    setState: setProjectInterviews,
  } = useFetch(
    { defaultData: [], fetcher: api.getMyProjectInterviews, immediate: true },
    {},
  );

  const interviews = projectInterviews || [];

  const handleAddInterviewClick = () => {
    navigate('/project-interview/create');
  };

  const handleEditInterviewClick = (id) => {
    navigate(`/project-interview/${id}/edit`);
  };

  const handleViewInterviewClick = (id) => {
    navigate(`/project-interview/${id}/view`);
  };

  const handleRemoveInterview = (id) => {
    setConfrimLoading(true);

    api
      .deleteProjectInterviews(id)
      .then((res) => {
        setProjectInterviews((prevState) => {
          return {
            ...prevState,
            data: {
              ...prevState.data,
              projectInterviews: prevState.data.projectInterviews.filter(
                (interview) => interview._id !== id,
              ),
            },
          };
        });
        setConfrimLoading(false);
        closeConfirmModal();

        showToast(tNotifications('deleteProjectInterviewNotification'));
      })
      .catch(() => {
        setConfrimLoading(false);
      });
  };

  const handleRemoveInteviewClick = (id) => {
    openConfirmModal({
      titleText: t('confirmModalTitle'),
      contentText: t('confirmModalContent'),
      onConfirmClick: () => handleRemoveInterview(id),
      onCancelClick: () => closeConfirmModal(),
    });
  };

  useEffect(() => {
    const timeOut = setTimeout(() => {
      navigate('/project-interviews', { replace: true });
    }, 5000);
    isTrophyModal && openTrophyModal();

    return () => clearTimeout(timeOut);
  });

  return (
    <>
      <EmployeeProjectInterviewsPage
        t={t}
        loading={loading}
        interviews={interviews}
        onAddInterviewClick={handleAddInterviewClick}
        onEditInterviewClick={handleEditInterviewClick}
        onRemoveInterviewClick={handleRemoveInteviewClick}
        onViewInterviewClick={handleViewInterviewClick}
      />
      <ConfirmModal
        {...modalData}
        loading={confirmLoading}
        isOpen={isConfirmModalOpen}
        onClose={closeConfirmModal}
      />
      <TrophyModal
        t={t}
        isOpen={isTrophyModalOpen}
        onClose={closeTrophyModal}
      />
    </>
  );
};

export default React.memo(EmployeeProjectInterviewsPageContainer);
