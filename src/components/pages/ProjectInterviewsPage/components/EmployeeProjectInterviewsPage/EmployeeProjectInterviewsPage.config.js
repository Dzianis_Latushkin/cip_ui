export const getTableColumns = (t) => [
  {
    key: 'status',
    label: t('tableStatusLabel'),
    minWidth: '120px',
    maxWidth: '120px',
    sortable: false,
  },
  {
    key: 'title',
    label: t('tableInterviewLabel'),
    minWidth: '320px',
    maxWidth: '320px',
    sortable: false,
  },
  {
    key: 'project',
    label: t('tableProjectLabel'),
    minWidth: '320px',
    maxWidth: '320px',
    sortable: false,
  },
  {
    key: 'date',
    label: t('tableDateLabel'),
    minWidth: '100px',
    maxWidth: '100px',
    sortable: false,
  },
  {
    key: 'controls',
    minWidth: '50px',
    maxWidth: '50px',
    sortable: false,
  },
];
