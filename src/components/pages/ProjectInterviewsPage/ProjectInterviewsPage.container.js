import React from 'react';
import { useSelector } from 'react-redux';

import { ROLES } from 'constants/common';

import { userSelector } from 'reducers/selectors';

import { ManagerProjectInterviewsPage } from './components/ManagerProjectInterviewsPage';
import { EmployeeProjectInterviewsPage } from './components/EmployeeProjectInterviewsPage';

const ProjectInterviewsPageContainer = () => {
  const user = useSelector(userSelector);
  const role = user.currentUser.role;

  switch (role) {
    case ROLES.MANAGER: {
      return <ManagerProjectInterviewsPage />;
    }
    case ROLES.EMPLOYEE: {
      return <EmployeeProjectInterviewsPage />;
    }
  }
};

export default React.memo(ProjectInterviewsPageContainer);
