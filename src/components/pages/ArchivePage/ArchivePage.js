import React from 'react';

import { INTERVIEW_CARD_TYPE } from 'constants/common';

import { Icon } from 'components/shared/Icon';
import { Typography } from 'components/shared/Typography';

import { InterviewCard } from 'components/InterviewCard';

import styles from './ArchivePage.styles.scss';

const ArchivePage = ({
  t,
  archivedInterviews,
  onDuplicateInterviewClick,
  onResultsClick,
}) => (
  <div className={styles.container}>
    <div className={styles.interviewsGroup}>
      {archivedInterviews.length > 0 ? (
        <div className={styles.interviews}>
          {archivedInterviews.reverse().map((interview) => (
            <InterviewCard
              key={interview._id}
              t={t}
              variant={INTERVIEW_CARD_TYPE.ARCHIVED}
              interview={interview}
              onDuplicateInterviewClick={onDuplicateInterviewClick}
              onResultsClick={onResultsClick}
            />
          ))}
        </div>
      ) : (
        <div className={styles.infoMessage}>
          <div className={styles.infoMessageIconWrap}>
            <Icon name="info" className={styles.infoMessageIcon} />
          </div>
          <Typography
            variant="h3"
            component="h3"
            className={styles.infoMessageText}
          >
            {t('noInterviews')}
          </Typography>
        </div>
      )}
    </div>
  </div>
);

export default React.memo(ArchivePage);
