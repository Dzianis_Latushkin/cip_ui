import React from 'react';
import { useNavigate } from 'react-router-dom';
import { useTranslation } from 'react-i18next';

import { INTERVIEWS_PAGE_TYPE } from 'constants/common';

import * as api from 'api/requests/requests';

import { changeTitle } from 'helpers/changeTitleInterviewsCard';
import { divideInterviews } from 'helpers/divideInterviews';

import { useFetch } from 'hooks/useFetch';

import { MainLayout } from 'components/Layout';

import ArchivePage from './ArchivePage';

const ArchivePageContainer = () => {
  const { t } = useTranslation('archivePage');
  const navigate = useNavigate();

  const {
    data: { interviews },
    loading,
    setState: setInterviews,
  } = useFetch(
    {
      defaultData: [],
      fetcher: api.getManagersInterviews,
      immediate: true,
    },
    '?status=archived',
  );

  const myInterviews = interviews || [];

  const { planned, archived } = divideInterviews(
    myInterviews,
    INTERVIEWS_PAGE_TYPE.ARCHIVE,
  );

  const handleDuplicateInterviewClick = (id) => {
    const nextInterview = myInterviews.find(
      (interview) => interview._id === id,
    );
    const nextTitle = changeTitle(nextInterview.title, planned, nextInterview);

    api
      .createInterview({
        title: nextTitle,
        status: 'planned',
        employee: nextInterview.employee._id,
        questions: nextInterview.questions,
      })
      .then((res) => {
        navigate('/interviews');
      })
      .catch();
  };

  const handleResultslick = (id) => {
    navigate(`/feedback/${id}`);
  };

  return (
    <MainLayout loading={loading} pageTitle={t('pageTitle')}>
      <ArchivePage
        t={t}
        navigate={navigate}
        archivedInterviews={archived}
        setInterviews={setInterviews}
        onDuplicateInterviewClick={handleDuplicateInterviewClick}
        onResultsClick={handleResultslick}
      />
    </MainLayout>
  );
};

export default React.memo(ArchivePageContainer);
