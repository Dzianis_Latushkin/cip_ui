import * as Yup from 'yup';

export const getValidationSchema = (t) =>
  Yup.object().shape({
    generalComment: Yup.string().required(t('commentValidavion')),
    feedback: Yup.array().of(
      Yup.object().shape({
        sharedFeedback: Yup.string(),
        privateFeedback: Yup.string(),
        grade: Yup.string(),
      }),
    ),
  });
