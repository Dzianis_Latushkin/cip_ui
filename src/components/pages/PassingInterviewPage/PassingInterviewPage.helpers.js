import { isEqual } from 'lodash';

export const convertDataToApi = (values, interview, timeValue) => ({
  employee: interview.employee._id,
  interviewTitle: interview.title,
  generalComment: values.generalComment,
  feedback: values.feedback.map((question, index) => ({
    question: question.question,
    comment: question.comment,
    grade: question.grade,
    privateFeedback: question.privateFeedback,
    sharedFeedback: question.sharedFeedback,
  })),
  elapsedTime: timeValue,
  interviewId: interview._id,
});

export const convertInterviewDataToApi = (interview, values) => {
  return {
    title: interview.title,
    status: 'planned',
    employee: interview.employee.option,
    questions: values.feedback.map((question) => ({
      question: question.question,
      comment: question.comment,
      folderQuestionId: question.folderQuestionId,
    })),
  };
};

export const convertFeedbackDataToApi = (values, feedback) => ({
  employee: feedback.employee._id,
  interviewTitle: feedback.interview?.title,
  generalComment: values.generalComment,
  feedback: values.feedback,
  elapsedTime: feedback.elapsedTime,
});

export const convertDataToForm = (interview) => {
  return {
    generalComment: '',
    feedback: interview
      ? interview.questions?.map((question) => ({
          question: question.question,
          comment: question.comment,
          grade: '5',
          privateFeedback: '',
          sharedFeedback: '',
          id: question._id,
          folderQuestionId: question.folderQuestionId,
        }))
      : [],
  };
};

export const convertFeedbackDataToForm = (feedback) => ({
  generalComment: feedback ? feedback.generalComment : '',
  feedback: feedback
    ? feedback.feedback.map((item) => ({
        question: item.question,
        comment: item.comment,
        grade: item.grade,
        privateFeedback: item.privateFeedback,
        sharedFeedback: item.sharedFeedback,
      }))
    : [],
});

export const findFormItemByIndex = ({ index, arrayHelpers }) => {
  return arrayHelpers.form.values.feedback.find(
    (question, questionIndex) => questionIndex === index,
  );
};

export const setInitialQuestions = ({ interview, setQuestions }) => {
  setQuestions((prevState) => ({
    ...prevState,
    data: {
      folders: prevState.data.folders?.map((nextFolder) => {
        return {
          ...nextFolder,
          questions: nextFolder.questions?.map((nextQuestion) => {
            if (
              interview.questions.some(
                (question) => question.folderQuestionId === nextQuestion._id,
              )
            ) {
              return { ...nextQuestion, isSelected: true };
            }

            return nextQuestion;
          }),
        };
      }),
    },
  }));
};

export const markListQuestionAsSelected = ({ id, setQuestions }) => {
  setQuestions((prevState) => {
    return {
      ...prevState,
      data: {
        folders: prevState.data.folders.map((nextFolder) => {
          return {
            ...nextFolder,
            questions: nextFolder.questions.map((nextQuestion) => {
              if (nextQuestion._id === id) {
                return { ...nextQuestion, isSelected: true };
              }

              return nextQuestion;
            }),
          };
        }),
      },
    };
  });
};

export const markListQuestionAsUnselected = ({
  removeQuestion,
  setQuestions,
}) => {
  setQuestions((prevState) => ({
    ...prevState,
    data: {
      folders: prevState.data.folders.map((nextFolder) => ({
        ...nextFolder,
        questions: nextFolder.questions.map((nextQuestion) => {
          if (
            nextQuestion._id === removeQuestion.folderQuestionId ||
            nextQuestion._id === removeQuestion._id
          ) {
            return { ...nextQuestion, isSelected: false };
          }

          return nextQuestion;
        }),
      })),
    },
  }));
};

export const checkIsInterviewChanged = ({ values, interview }) => {
  const nextFeedbackQuestions = values.feedback.map((question) => ({
    comment: question.comment,
    question: question.question,
    _id: question.id,
    folderQuestionId: question.folderQuestionId,
  }));

  return !isEqual(interview?.questions, nextFeedbackQuestions);
};

export const getPassedTime = (secondsPassed) => {
  return secondsPassed > 3599
    ? new Date(secondsPassed * 1000).toISOString().substr(11, 8)
    : new Date(secondsPassed * 1000).toISOString().substr(14, 5);
};
