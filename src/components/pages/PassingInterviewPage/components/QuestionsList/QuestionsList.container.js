import React, { useState } from 'react';
import { flattenDeep } from 'lodash';
import { useFormikContext } from 'formik';
import _cloneDeep from 'lodash/cloneDeep';

import { normalizeGroupData } from 'helpers/normalizeGroupData';

import {
  findFormItemByIndex,
  markListQuestionAsSelected,
  markListQuestionAsUnselected,
} from '../../PassingInterviewPage.helpers';

import { useModal } from 'hooks/useModal';

import { ConfirmModal } from 'components/ConfirmModal';

import { AlertModal } from 'components/AlertModal';
import { FoldersModal } from '../FoldersModal';
import { CustomQuestionModal } from '../CustomQuestionModal';

import QuestionsList from './QuestionsList';

const QuestionsListContainer = ({ t, edit, folders, setQuestions }) => {
  const { values, setFieldValue } = useFormikContext();

  const [
    isCustomQuestionModalOpen,
    openCustomQuestionModal,
    closeCustomQuestionModal,
    customQuestionModalData,
  ] = useModal({});

  const [
    isFoldersModalOpen,
    openFoldersModal,
    closeFoldersModal,
    foldersModalData,
  ] = useModal({});

  const [isAlertModalOpen, openAlertModal, closeAlertModal, alertModalData] =
    useModal({});

  const [isConfirmModalOpen, openConfirmModal, closeConfirmModal, modalData] =
    useModal({});

  const [isInsertBeforeState, setIsInsertBeforeState] = useState(false);

  const handleAddOwnQuestionClick = ({
    index,
    arrayHelpers,
    isInsertBefore,
  }) => {
    setIsInsertBeforeState(isInsertBefore);
    openCustomQuestionModal({ index, arrayHelpers });
  };

  const handleAddQuestionFromFolderClick = ({ index, isInsertBefore }) => {
    setIsInsertBeforeState(isInsertBefore);
    openFoldersModal({
      index,
      values,
    });
  };

  const handleAddFolderQuestionClick = ({ id, index }) => {
    markListQuestionAsSelected({ id, setQuestions });

    folders.forEach((folder) =>
      folder.questions.find((question) => {
        if (question._id === id) {
          const nextFeedback = _cloneDeep(values.feedback);
          if (index === 0 && isInsertBeforeState === true) {
            nextFeedback.splice(index, 0, {
              comment: question.comment,
              folderQuestionId: question._id,
              grade: '5',
              id: question._id,
              privateFeedback: '',
              question: question.question,
              sharedFeedback: '',
            });
          } else {
            nextFeedback.splice(index + 1, 0, {
              comment: question.comment,
              folderQuestionId: question._id,
              grade: '5',
              id: question._id,
              privateFeedback: '',
              question: question.question,
              sharedFeedback: '',
            });
          }

          setFieldValue('feedback', nextFeedback);
        }
      }),
    );
  };

  const handleDeleteFolderQuestionClick = ({ id }) => {
    if (values.feedback.length <= 1) {
      openAlertModal({
        contentText: t('emptyErrorText'),
      });

      return;
    }
    const removeQuestion = flattenDeep(
      folders.map((folder) => folder.questions),
    ).find((question) => question._id === id);

    markListQuestionAsUnselected({ removeQuestion, setQuestions });

    const nextFeedback = values.feedback.filter(
      (item) => item.folderQuestionId !== id,
    );

    setFieldValue('feedback', nextFeedback);
  };

  const handleDeleteQuestion = ({ index, arrayHelpers }) => {
    const removeQuestion = findFormItemByIndex({ index, arrayHelpers });

    arrayHelpers.remove(index);

    markListQuestionAsUnselected({ removeQuestion, setQuestions });

    closeConfirmModal();
  };

  const handleDeleteQuestionClick = ({ index, arrayHelpers }) => {
    if (values.feedback.length <= 1) {
      openAlertModal({
        contentText: t('emptyErrorText'),
      });

      return;
    }

    openConfirmModal({
      titleText: t('confirmModalTitle'),
      contentText: t('confirmModalText'),
      onConfirmClick: () => {
        handleDeleteQuestion({ index, arrayHelpers });
      },
      onCancelClick: () => closeConfirmModal(),
    });
  };

  const handleEditQuestionClick = ({ index, arrayHelpers }) => {
    const question = values.feedback.find(
      (item, itemIndex) => itemIndex === index,
    );

    openCustomQuestionModal({ edit: true, question, index, arrayHelpers });
  };

  return (
    <>
      <QuestionsList
        t={t}
        edit={edit}
        values={values}
        onAddOwnQuestionClick={handleAddOwnQuestionClick}
        onAddQuestionFromFolderClick={handleAddQuestionFromFolderClick}
        onEditQuestionClick={handleEditQuestionClick}
        onDeleteQuestionClick={handleDeleteQuestionClick}
      />
      <FoldersModal
        {...foldersModalData}
        t={t}
        isFoldersModalOpen={isFoldersModalOpen}
        onClose={closeFoldersModal}
        onAddFolderQuestionClick={handleAddFolderQuestionClick}
        onDeleteFolderQuestionClick={handleDeleteFolderQuestionClick}
        groupData={normalizeGroupData(folders)}
      />
      <CustomQuestionModal
        {...customQuestionModalData}
        t={t}
        isInsertBefore={isInsertBeforeState}
        isCustomQuestionModalOpen={isCustomQuestionModalOpen}
        setQuestions={setQuestions}
        onClose={closeCustomQuestionModal}
      />
      <AlertModal
        {...alertModalData}
        t={t}
        isOpen={isAlertModalOpen}
        onClose={closeAlertModal}
      />
      <ConfirmModal
        {...modalData}
        t={t}
        isOpen={isConfirmModalOpen}
        onClose={closeConfirmModal}
      />
    </>
  );
};

export default React.memo(QuestionsListContainer);
