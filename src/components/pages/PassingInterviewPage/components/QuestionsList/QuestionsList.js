import React from 'react';
import { FieldArray } from 'formik';

import { QuestionItem } from '../QuestionItem';

import styles from './QuestionsList.styles.scss';

const QuestionsList = ({
  t,
  edit,
  values,
  onAddOwnQuestionClick,
  onAddQuestionFromFolderClick,
  onEditQuestionClick,
  onDeleteQuestionClick,
}) => (
  <FieldArray
    name="feedback"
    render={(arrayHelpers) => (
      <div className={styles.interviewContainer}>
        {values.feedback.map((item, index) => (
          <QuestionItem
            t={t}
            index={index}
            edit={edit}
            key={item.id}
            name={`${index}`}
            question={item.question}
            comment={item.comment}
            order={values.feedback.length}
            orderItem={index + 1}
            arrayHelpers={arrayHelpers}
            onAddOwnQuestionClick={onAddOwnQuestionClick}
            onAddQuestionFromFolderClick={onAddQuestionFromFolderClick}
            onDeleteQuestionClick={onDeleteQuestionClick}
            onEditQuestionClick={onEditQuestionClick}
          />
        ))}
      </div>
    )}
  />
);

export default QuestionsList;
