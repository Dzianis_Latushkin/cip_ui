import React from 'react';
import { Formik, Form, Field } from 'formik';

import { initialValues } from './CustomQuestionModal.formConfig';
import { getValidationSchema } from './CustomQuestionModal.formConfig';

import { convertDataToForm } from './CustomQuestionModal.helpers';

import { Modal } from 'components/shared/Modal';
import { Button } from 'components/shared/Button';
import { Typography } from 'components/shared/Typography';

import { InputField } from 'components/FormikFields';

import styles from './CustomQuestionModal.styles.scss';

const CustomQuestionModal = ({
  isCustomQuestionModalOpen,
  t,
  edit,
  question,
  onSubmit,
  onClose,
}) => (
  <Modal
    open={isCustomQuestionModalOpen}
    centered
    className={styles.modal}
    onClose={onClose}
  >
    <Typography variant="h1" className={styles.title}>
      {t('questionModal.addQuestionModalName')}
    </Typography>
    <Formik
      validationSchema={getValidationSchema(t)}
      validateOnMount
      initialValues={edit ? convertDataToForm(question) : initialValues}
      onSubmit={onSubmit}
    >
      {({ values, errors, touched }) => (
        <Form className={styles.form}>
          <Field
            label={t('questionModal.titleLabel')}
            name="title"
            placeholder={t('questionModal.titlePlaceholder')}
            component={InputField}
            multiline={true}
            inputClassName={styles.titleField}
          />
          <Field
            label={t('questionModal.commentLabel')}
            type="comment"
            name="comment"
            component={InputField}
            placeholder={t('questionModal.commentPlaceholder')}
            multiline={true}
            inputClassName={styles.commentField}
          />
          <div className={styles.buttonBar}>
            <Button type="submit" className={styles.button}>
              {t('questionModal.confirmButton')}
            </Button>
            <Button
              color="outlined"
              onClick={onClose}
              className={styles.button}
            >
              {t('questionModal.cancelButton')}
            </Button>
          </div>
        </Form>
      )}
    </Formik>
  </Modal>
);

export default CustomQuestionModal;
