import React from 'react';
import cn from 'classnames';

import { Group } from 'components/Group';
import { FolderItem } from '../FolderItem';

import styles from './FoldersModal.styles.scss';

export const groupRender = ({
  questionIndex,
  groupData,
  values,
  onAddFolderQuestionClick,
  onDeleteFolderQuestionClick,
}) =>
  groupData.map((group, index) => {
    if (group.folders) {
      return (
        <Group
          key={group._id}
          id={group._id}
          groupName={group.name}
          openedClassName={cn(styles.openedFirst, {
            [styles.opened]: group.parentId,
          })}
          closedClassName={cn(styles.closedFirst, {
            [styles.closed]: group.parentId,
          })}
          isInterviewModal
        >
          {groupRender({
            questionIndex: questionIndex,
            groupData: group.folders,
            onAddFolderQuestionClick: onAddFolderQuestionClick,
            onDeleteFolderQuestionClick: onDeleteFolderQuestionClick,
            values,
          })}
          {group.questions.map((item) => (
            <FolderItem
              key={item._id}
              id={item._id}
              questionIndex={questionIndex}
              isInterviewModal
              isSelected={item.isSelected}
              groupId={group._id}
              question={item.question}
              comment={item.comment}
              values={values}
              onAddFolderQuestionClick={onAddFolderQuestionClick}
              onDeleteFolderQuestionClick={onDeleteFolderQuestionClick}
            />
          ))}
        </Group>
      );
    } else {
      return (
        <Group
          key={group._id}
          isInterviewModal
          id={group._id}
          groupName={group.name}
          openedClassName={cn(styles.openedFirst, {
            [styles.opened]: group.parentId,
          })}
          closedClassName={cn(styles.closedFirst, {
            [styles.closed]: group.parentId,
          })}
        >
          {group.questions.map((item) => (
            <FolderItem
              key={item._id}
              isInterviewModal
              isSelected={item.isSelected}
              id={item._id}
              questionIndex={questionIndex}
              groupId={group._id}
              question={item.question}
              comment={item.comment}
              onAddFolderQuestionClick={onAddFolderQuestionClick}
              onDeleteFolderQuestionClick={onDeleteFolderQuestionClick}
              values={values}
            />
          ))}
        </Group>
      );
    }
  });
