import React from 'react';

import { groupRender } from './FoldersModal.helpers';

import { Modal } from 'components/shared/Modal';
import { Button } from 'components/shared/Button';
import { ScrollWrapper } from 'components/shared/ScrollWrapper';

import styles from './FoldersModal.styles.scss';

const FoldersModal = ({
  t,
  index,
  isFoldersModalOpen,
  groupData,
  values,
  onClose,
  onAddFolderQuestionClick,
  onDeleteFolderQuestionClick,
}) => {
  return (
    <Modal open={isFoldersModalOpen} className={styles.modal} onClose={onClose}>
      <div className={styles.modalTopContainer}>
        <Button onClick={onClose}>{t('interviewModal.finishButton')}</Button>
      </div>

      <div className={styles.questionsGroupContainer}>
        <ScrollWrapper
          translateContentSizeYToHolder
          trackClassName={styles.track}
          contentClassName={styles.content}
          className={styles.scroll}
        >
          {groupRender({
            questionIndex: index,
            groupData,
            values,
            onAddFolderQuestionClick,
            onDeleteFolderQuestionClick,
          })}
        </ScrollWrapper>
      </div>
    </Modal>
  );
};

export default FoldersModal;
