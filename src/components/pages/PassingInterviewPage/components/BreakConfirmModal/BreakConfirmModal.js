import React from 'react';
import { useTranslation } from 'react-i18next';

import { Modal } from 'components/shared/Modal';
import { Button } from 'components/shared/Button';
import { Typography } from 'components/shared/Typography';

import styles from './BreakConfirmModal.styles.scss';

const BreakConfirmModal = ({
  loading,
  isOpen,
  withCloseIcon,
  titleText,
  contentText,
  saveAndQuitButtonText,
  quitWithoutSavingButtonText,
  backButtonText,
  onSaveAndQuitClick,
  onQuitWithoutSavingButtonClick,
  onBackClick,
  onClose,
}) => {
  const { t } = useTranslation('confirmModal');

  return (
    <Modal
      open={isOpen}
      withCloseIcon={withCloseIcon}
      onClose={onClose}
      centered
      className={styles.modal}
    >
      <Typography variant="h1" className={styles.title}>
        {titleText || t('defaultTitle')}
      </Typography>
      {contentText && (
        <Typography variant="body" className={styles.subTitle}>
          {contentText}
        </Typography>
      )}
      <div className={styles.buttonBar}>
        <Button
          onClick={onSaveAndQuitClick}
          type="submit"
          variant="default"
          loading={loading}
          className={styles.button}
        >
          {saveAndQuitButtonText || t('saveAndQuitButton')}
        </Button>

        <Button
          variant="default"
          className={styles.button}
          onClick={onQuitWithoutSavingButtonClick}
        >
          {quitWithoutSavingButtonText || t('quitWithoutSaving')}
        </Button>

        <Button
          variant="outlined"
          className={styles.button}
          onClick={onBackClick}
        >
          {backButtonText || t('backButton')}
        </Button>
      </div>
    </Modal>
  );
};

BreakConfirmModal.defaultProps = {
  isOpen: false,
};

export default React.memo(BreakConfirmModal);
