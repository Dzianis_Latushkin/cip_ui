import React from 'react';
import { Field } from 'formik';
import Linkify from 'react-linkify';
import { Tooltip } from 'react-tippy';
import cn from 'classnames';

import { getRatingAnswers } from 'helpers/getRatingAnswers';

import { Icon } from 'components/shared/Icon';
import { Button } from 'components/shared/Button';
import { Tabs, Tab } from 'components/shared/Tabs';
import { Typography } from 'components/shared/Typography';

import { InputField } from 'components/FormikFields';
import { RatingRadioField } from 'components/FormikFields';

import styles from './QuestionItem.styles.scss';

const QuestionItem = ({
  t,
  index,
  edit,
  question,
  comment,
  order,
  orderItem,
  name,
  arrayHelpers,
  onAddOwnQuestionClick,
  onAddQuestionFromFolderClick,
  onDeleteQuestionClick,
  onEditQuestionClick,
}) => (
  <div className={styles.questionItem}>
    {!edit && index === 0 ? (
      <div className={cn(styles.addHotQuestion)}>
        <Tooltip
          html={
            <div className={styles.tooltip}>
              <Button
                size="small"
                variant="outlined"
                className={styles.tooltipButton}
                onClick={() => {
                  onAddQuestionFromFolderClick({
                    index,
                    arrayHelpers,
                    isInsertBefore: true,
                  });
                }}
              >
                {t('addExistedQuestionButton')}
              </Button>
              <Button
                size="small"
                variant="outlined"
                className={styles.tooltipButton}
                onClick={() => {
                  onAddOwnQuestionClick({
                    index,
                    arrayHelpers,
                    isInsertBefore: true,
                  });
                }}
              >
                {t('addNewQuestionButton')}
              </Button>
            </div>
          }
          position="top"
          trigger="click"
        >
          <Icon name="plusCircle" className={styles.addIcon} />
        </Tooltip>
      </div>
    ) : null}
    <div className={styles.question}>
      <div className={styles.questionContainer}>
        <div className={styles.questionWrapper}>
          <Icon name="question" className={styles.questionIcon} />
          <Typography variant="body" className={styles.questionContent}>
            {question}
          </Typography>
        </div>
        <div className={styles.titleInfo}>
          <Typography
            variant="body"
            className={styles.order}
          >{`${orderItem} / ${order}`}</Typography>
        </div>
      </div>

      <div className={styles.commentContainer}>
        <Linkify>
          <Typography variant="body" className={styles.commentContent}>
            {comment}
          </Typography>
        </Linkify>
      </div>

      <Tabs className={styles.tabs}>
        <Tab label={t('question.sharedTab')}>
          <Field
            name={`feedback[${name}].sharedFeedback`}
            placeholder={t('question.sharedAnswerFieldPlaceholder')}
            component={InputField}
            multiline={true}
            inputClassName={styles.answerField}
            formControlProps={{ className: styles.formControl }}
          />
        </Tab>

        <Tab label={t('question.privateTab')}>
          <Field
            name={`feedback[${name}].privateFeedback`}
            placeholder={t('question.privateAnswerFieldPlaceholder')}
            component={InputField}
            multiline={true}
            inputClassName={styles.answerField}
            formControlProps={{ className: styles.formControl }}
          />
        </Tab>
      </Tabs>
      <div className={styles.questionRowWrapper}>
        <Field
          name={`feedback[${name}].grade`}
          component={RatingRadioField}
          ratings={getRatingAnswers(t)}
          formControlClassName={styles.ratingRadioFieldControl}
        />
        {!edit && (
          <Tooltip
            html={
              <div className={styles.tooltip}>
                <div
                  className={styles.tooltipItem}
                  onClick={() => {
                    onEditQuestionClick({ index, arrayHelpers });
                  }}
                >
                  <Icon name="pencil" className={styles.tooltipItemIcon} />
                  <Typography variant="body" className={styles.tooltipItemText}>
                    {t('questionMenu.editQuestion')}
                  </Typography>
                </div>
                <div
                  className={styles.tooltipItem}
                  onClick={() => {
                    onDeleteQuestionClick({ index, arrayHelpers });
                  }}
                >
                  <Icon name="delete" className={styles.tooltipItemIcon} />
                  <Typography variant="body" className={styles.tooltipItemText}>
                    {t('questionMenu.deleteQuestion')}
                  </Typography>
                </div>
              </div>
            }
            position="bottom-end"
            trigger="click"
          >
            <Icon name="more" className={styles.questionMenu} />
          </Tooltip>
        )}
      </div>
    </div>
    {!edit && (
      <div className={styles.addHotQuestion}>
        <Tooltip
          html={
            <div className={styles.tooltip}>
              <Button
                size="small"
                variant="outlined"
                className={styles.tooltipButton}
                onClick={() => {
                  onAddQuestionFromFolderClick({
                    index,
                    arrayHelpers,
                    isInsertBefore: false,
                  });
                }}
              >
                {t('addExistedQuestionButton')}
              </Button>
              <Button
                size="small"
                variant="outlined"
                className={styles.tooltipButton}
                onClick={() => {
                  onAddOwnQuestionClick({
                    index,
                    arrayHelpers,
                    isInsertBefore: false,
                  });
                }}
              >
                {t('addNewQuestionButton')}
              </Button>
            </div>
          }
          position="top"
          trigger="click"
        >
          <Icon name="plusCircle" className={styles.addIcon} />
        </Tooltip>
      </div>
    )}
  </div>
);

export default React.memo(QuestionItem);
