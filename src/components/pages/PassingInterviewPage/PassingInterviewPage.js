import React from 'react';
import { Formik, Form, Field } from 'formik';

import { getValidationSchema } from './PassingInterviewPage.formConfig';

import {
  convertDataToForm,
  convertFeedbackDataToForm,
} from './PassingInterviewPage.helpers';

import { Icon } from 'components/shared/Icon';
import { Button } from 'components/shared/Button';
import { Typography } from 'components/shared/Typography';

import { InputField } from 'components/FormikFields';

import { QuestionsList } from './components/QuestionsList';

import styles from './PassingInterviewPage.styles.scss';

const PassingInterviewPage = ({
  t,
  edit,
  submitLoading,
  folders,
  timePassed,
  interview,
  feedback,
  onSubmit,
  setQuestions,
  onBreakInterview,
}) => (
  <div className={styles.page}>
    <Formik
      validationSchema={getValidationSchema(t)}
      validateOnMount
      onSubmit={onSubmit}
      initialValues={
        edit
          ? convertFeedbackDataToForm(feedback)
          : convertDataToForm(interview)
      }
    >
      {({ values }) => (
        <Form className={styles.form}>
          <div className={styles.commentsContainer}>
            <div className={styles.mainInfo}>
              <div className={styles.userInfo}>
                <Icon className={styles.userIcon} name="user" />
                <Typography className={styles.nameText}>
                  {(interview && interview.employee.name) ||
                    (feedback && feedback.employee.name)}
                </Typography>
              </div>
              <div className={styles.time}>
                <Icon className={styles.clockIcon} name="clock2" />
                <Typography>{timePassed}</Typography>
              </div>
            </div>
            <div className={styles.comments}>
              <Field
                label={t('generalCommentLabel')}
                name="generalComment"
                component={InputField}
                multiline={true}
                inputClassName={styles.commentsField}
                formControlProps={{ className: styles.formControl }}
              />
              <div className={styles.buttonBar}>
                <Button
                  className={styles.button}
                  type="submit"
                  loading={submitLoading}
                >
                  {edit ? t('updateButton') : t('completeButton')}
                </Button>
                <Button
                  className={styles.button}
                  variant="outlined"
                  onClick={() => {
                    onBreakInterview(values);
                  }}
                >
                  {edit ? t('cancelButton') : t('breakButton')}
                </Button>
              </div>
            </div>
          </div>
          <QuestionsList
            t={t}
            edit={edit}
            folders={folders}
            setQuestions={setQuestions}
          />
        </Form>
      )}
    </Formik>
  </div>
);

export default React.memo(PassingInterviewPage);
