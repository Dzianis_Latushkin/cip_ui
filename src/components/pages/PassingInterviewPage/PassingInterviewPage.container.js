import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useNavigate, useParams } from 'react-router-dom';

import * as api from 'api/requests/requests';

import {
  getPassedTime,
  convertDataToApi,
  setInitialQuestions,
  checkIsInterviewChanged,
  convertFeedbackDataToApi,
  convertInterviewDataToApi,
} from './PassingInterviewPage.helpers';
import { showToast } from 'helpers/showToast';

import { useFetch } from 'hooks/useFetch';
import { useTimer } from 'hooks/useTimer';
import { useModal } from 'hooks/useModal';

import { MainLayout } from 'components/Layout';

import { BreakConfirmModal } from './components/BreakConfirmModal';

import PassingInterviewPage from './PassingInterviewPage';

const PassingInterviewPageContainer = ({ edit }) => {
  const { t } = useTranslation('passingInterviewPage');
  const [tNotifications] = useTranslation('notifications');

  const { id: entityId } = useParams();
  const navigate = useNavigate();

  const [submitLoading, setSubmitLoading] = useState(false);

  const [secondsPassed, { stopTimer, setSeconds }] = useTimer({
    defaultSeconds: 0,
    defaultStarted: !edit,
  });

  const {
    data: { interview },
    loading: interviewLoading,
  } = useFetch(
    {
      defaultData: [],
      fetcher: api.getOneInterview,
      stopRequest: edit,
    },
    { id: entityId },
  );

  const {
    data: { folders },
    setState: setQuestions,
  } = useFetch(
    {
      defaultData: { folders: [] },
      fetcher: api.getAllFolders,
      stopRequest: !interview || interview.questions.length === 0,
      immediately: false,
      onSuccess: (res) => {
        setInitialQuestions({ interview, setQuestions });
      },
    },
    {},
    [interviewLoading],
  );

  const {
    data: { feedback },
    loading: feedbackLoading,
  } = useFetch(
    {
      defaultData: {},
      fetcher: api.getFeedback,
      stopRequest: !edit,
      onSuccess: (res) => {
        setSeconds(res.feedback.elapsedTime);
      },
    },
    entityId,
  );

  const [
    isBreakConfirmModalOpen,
    openBreakConfirmModal,
    closeBreakConfirmModal,
    breakModalData,
  ] = useModal({});

  useEffect(() => {
    const handleCloseWindow = (e) => {
      e.preventDefault();
      e.returnValue = '';
    };

    window.addEventListener('beforeunload', handleCloseWindow);

    return () => {
      window.removeEventListener('beforeunload', handleCloseWindow);
    };
  }, []);

  const handleSaveInterviewClick = async ({ values, withToast }) => {
    const data = convertInterviewDataToApi(interview, values);

    await api
      .editInterview(entityId, data)
      .then((res) => {
        withToast && showToast(tNotifications('saveInterviewNotification'));
      })
      .catch();
  };

  const handleSubmit = (values, actions) => {
    setSubmitLoading(true);

    if (!edit) {
      stopTimer();
    }

    const data = edit
      ? convertFeedbackDataToApi(values, feedback)
      : convertDataToApi(values, interview, secondsPassed);

    if (!edit && checkIsInterviewChanged({ values, interview })) {
      handleSaveInterviewClick({ values, withToast: false }).then(() => {
        api
          .createFeedback(data)
          .then((res) => {
            setSubmitLoading(false);
            showToast(tNotifications('createFeedbackNotification'));
            navigate(`/feedback/${res.data.feedback._id}`);
          })
          .catch();
      });
    }

    if (!edit && !checkIsInterviewChanged({ values, interview })) {
      api
        .createFeedback(data)
        .then((res) => {
          setSubmitLoading(false);
          showToast(tNotifications('createFeedbackNotification'));
          navigate(`/feedback/${res.data.feedback._id}`);
        })
        .catch();
    }

    if (edit) {
      api
        .updateFeedback(entityId, data)
        .then((res) => {
          setSubmitLoading(false);
          navigate(`/feedback/${entityId}`);
        })
        .catch();
    }
  };

  const handleBreakInterview = (values) => {
    if (edit) {
      navigate(`/feedback/${entityId}`);
    }

    if (!edit && checkIsInterviewChanged({ values, interview })) {
      openBreakConfirmModal({
        titleText: t('saveChangesModalTitle'),
        contentText: t('saveChangesModalText'),
        saveAndQuitButtonText: t('saveAndQuit'),
        quitWithoutSavingButtonText: t('quitWithoutSaving'),
        backButtonText: t('backToInterview'),
        withCloseIcon: true,
        onSaveAndQuitClick: () => {
          handleSaveInterviewClick({ values, withToast: true }).then(() => {
            navigate('/interviews');
          });
        },
        onQuitWithoutSavingButtonClick: () => {
          closeBreakConfirmModal();
          navigate('/interviews');
        },
        onBackClick: () => {
          closeBreakConfirmModal();
        },
      });
    }

    if (!edit && !checkIsInterviewChanged({ values, interview })) {
      navigate('/interviews');
    }
  };

  const timePassed = getPassedTime(secondsPassed);

  const loading = interviewLoading || feedbackLoading;

  return (
    <MainLayout
      loading={loading}
      pageTitle={
        (interview && interview?.title) ||
        (feedback && feedback.interview?.title)
      }
    >
      <PassingInterviewPage
        t={t}
        submitLoading={submitLoading}
        edit={edit}
        timePassed={timePassed}
        interview={interview}
        folders={folders}
        feedback={feedback}
        onSubmit={handleSubmit}
        setQuestions={setQuestions}
        onBreakInterview={handleBreakInterview}
      />
      <BreakConfirmModal
        {...breakModalData}
        t={t}
        isOpen={isBreakConfirmModalOpen}
        onClose={closeBreakConfirmModal}
      />
    </MainLayout>
  );
};

export default React.memo(PassingInterviewPageContainer);
