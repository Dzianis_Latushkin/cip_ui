import React from 'react';

import { Modal } from 'components/shared/Modal';
import { Button } from 'components/shared/Button';
import { Typography } from 'components/shared/Typography';
import { Checkbox } from 'components/shared/Checkbox';

import styles from './PrintOptionsModal.styles.scss';

const PrintOptionsModal = ({
  t,
  loading,
  isOpen,
  printOptions,
  onClose,
  onChange,
  onApply,
}) => (
  <Modal open={isOpen} centered className={styles.modal} onClose={onClose}>
    <Typography variant="h1" className={styles.title}>
      {t('PrintOptionsModal.title')}
    </Typography>
    <div className={styles.content}>
      <div className={styles.row}>
        <Checkbox
          id="markCheckboxId"
          checked={printOptions.isMarksPrinting}
          label={'Include marks'}
          onChange={() => onChange('isMarksPrinting')}
        />
      </div>
      <div className={styles.row}>
        <Checkbox
          id="durationCheckboxId"
          checked={printOptions.isDurationPrinting}
          label={'Include duration'}
          onChange={() => onChange('isDurationPrinting')}
        />
      </div>
      <div className={styles.row}>
        <Checkbox
          id="fiveStarAnswersCheckboxId"
          checked={printOptions.isFiveStarAnswersPrinting}
          label={'Include 5-star answers'}
          onChange={() => onChange('isFiveStarAnswersPrinting')}
        />
      </div>
      <div className={styles.row}>
        <Checkbox
          id="isEmptyFeedbackPrintingCheckboxId"
          checked={printOptions.isEmptyFeedbackPrinting}
          label={'Include empty feedback answer'}
          onChange={() => onChange('isEmptyFeedbackPrinting')}
        />
      </div>
    </div>

    <div className={styles.buttonBar}>
      <Button
        onClick={onApply}
        className={styles.button}
        variant="default"
        loading={loading}
      >
        {t('PrintOptionsModal.applyButton')}
      </Button>
      <Button variant="outlined" onClick={onClose} className={styles.button}>
        {t('PrintOptionsModal.cancelButton')}
      </Button>
    </div>
  </Modal>
);

export default PrintOptionsModal;
