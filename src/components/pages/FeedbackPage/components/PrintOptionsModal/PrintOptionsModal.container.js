import React from 'react';

import PrintOptionsModal from './PrintOptionsModal';

const PrintOptionsModalContainer = ({
  t,
  isOpen,
  userId,
  printOptions,
  onClose,
  onApply,
  handlePrintOptionsChange,
  ...restProps
}) => {
  return (
    <PrintOptionsModal
      t={t}
      isOpen={isOpen}
      printOptions={printOptions}
      onClose={onClose}
      onApply={onApply}
      onChange={handlePrintOptionsChange}
      {...restProps}
    />
  );
};

export default React.memo(PrintOptionsModalContainer);
