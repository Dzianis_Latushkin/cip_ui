import React from 'react';
import cn from 'classnames';
import Linkify from 'react-linkify';
import Collapsible from 'react-collapsible';

import {
  getResultRating,
  getResultTime,
  getResultDate,
} from './FeedbackPage.helpers';

import { Icon } from 'components/shared/Icon';
import { Button } from 'components/shared/Button';
import { Typography } from 'components/shared/Typography';

import styles from './FeedbackPage.styles.scss';

const FeedbackPage = ({
  t,
  isManager,
  isMarksPrinting,
  isDurationPrinting,
  isFiveStarAnswersPrinting,
  isEmptyFeedbackPrinting,
  openCollapsible,
  feedback,
  currentUserId,
  onEditFeedbackClick,
  onExportPdfClick,
  onCollapsibleTriggerClick,
}) => {
  const creatorId = feedback.creator?._id;

  return (
    <div className={styles.page}>
      <div className={styles.logoWrapper}>
        <Icon name="logoFullVersion" className={styles.logo} />
      </div>
      <div className={styles.header}>
        {!isManager ? (
          <Typography
            variant="h3"
            className={cn(styles.ratingTotal, styles.employeeFeedback)}
          >
            {getResultRating(feedback.feedback)}
          </Typography>
        ) : (
          ''
        )}
        <Typography variant="h1" className={styles.title}>
          {feedback.interview?.title}
        </Typography>
        <div className={styles.buttonsContainer}>
          {isManager && (
            <Button
              title={t('editFeedbackButtonTitle')}
              className={styles.button}
              onClick={onEditFeedbackClick}
              disabled={creatorId !== currentUserId}
            >
              {t('editFeedbackButton')}
            </Button>
          )}
          <Button className={styles.button} onClick={onExportPdfClick}>
            {t('exportPdfButton')}
          </Button>
        </div>
      </div>
      <div className={cn(styles.stats, styles.statsUserInfo)}>
        <Icon name="user" className={styles.userIcon} />

        {feedback.employee && (
          <Typography variant="h2" className={styles.userName}>
            {isManager ? feedback.employee.name : feedback.creator.name}
          </Typography>
        )}
        {isManager ? (
          <Typography
            variant="h3"
            className={cn(styles.ratingTotal, {
              [styles.noPrinting]: isMarksPrinting === false,
            })}
          >
            {getResultRating(feedback.feedback)}
          </Typography>
        ) : (
          ''
        )}
      </div>
      <div className={styles.stats}>
        <Icon name="calendar" className={styles.statsIconSmall} />
        <Typography variant="h4" className={styles.statsText}>
          {getResultDate(feedback.updatedAt)}
        </Typography>
        <Icon
          name="clock2"
          className={cn(styles.statsIconSmall, {
            [styles.noPrinting]: isDurationPrinting === false,
          })}
        />
        <Typography
          variant="h4"
          className={cn(styles.statsText, {
            [styles.noPrinting]: isDurationPrinting === false,
          })}
        >
          {getResultTime(feedback.elapsedTime)}
        </Typography>
      </div>
      <div className={styles.main}>
        <Typography variant="h3" className={styles.subtitle}>
          {t('summaryLabel')}
        </Typography>
        <Linkify
          componentDecorator={(decoratedHref, decoratedText, key) => (
            <Typography
              component="a"
              variant="link"
              target="blank"
              href={decoratedHref}
              key={key}
            >
              {' '}
              {decoratedText}{' '}
            </Typography>
          )}
        >
          <Typography className={styles.text}>
            {feedback.generalComment}
          </Typography>
        </Linkify>
        <div className={styles.items}>
          {feedback.feedback &&
            feedback.feedback.map((item) => {
              if (
                isEmptyFeedbackPrinting === false &&
                item.sharedFeedback === ''
              ) {
                return (
                  <Collapsible
                    key={item._id}
                    open={
                      !openCollapsible.find((question) => question === item._id)
                    }
                    onTriggerClosing={() => onCollapsibleTriggerClick(item._id)}
                    className={cn(styles.item, styles.noPrinting)}
                    openedClassName={cn(styles.openedItem, styles.noPrinting)}
                    trigger={
                      <div className={styles.trigger}>
                        <Icon name="question" className={styles.triggerIcon} />
                        <img
                          src="/assets/icons/question.svg"
                          alt="circle question icon"
                          height="24px"
                          width="24px"
                          className={styles.triggerIconForPrint}
                        />
                        <Typography className={styles.questionText}>
                          {item.question}
                        </Typography>
                        <Typography
                          variant="h4"
                          className={cn(styles.ratingItem, {
                            [styles.noPrinting]: isMarksPrinting === false,
                          })}
                        >
                          {item.grade}
                        </Typography>
                        <Icon
                          name="chevronDown"
                          className={styles.chevronIcon}
                        />
                      </div>
                    }
                    triggerWhenOpen={
                      <div className={styles.trigger}>
                        <Icon name="question" className={styles.triggerIcon} />
                        <img
                          src="/assets/icons/question.svg"
                          alt="circle question icon"
                          height="24px"
                          width="24px"
                          className={styles.triggerIconForPrint}
                        />
                        <Typography className={styles.questionText}>
                          {item.question}
                        </Typography>
                        <Typography
                          variant="h4"
                          className={cn(styles.ratingItem, {
                            [styles.noPrinting]: isMarksPrinting !== true,
                          })}
                        >
                          {item.grade}
                        </Typography>
                        <Icon name="chevronUp" className={styles.chevronIcon} />
                      </div>
                    }
                  >
                    <div className={styles.inner}>
                      <Linkify
                        componentDecorator={(
                          decoratedHref,
                          decoratedText,
                          key,
                        ) => (
                          <Typography
                            component="a"
                            variant="link"
                            target="blank"
                            href={decoratedHref}
                            key={key}
                          >
                            {' '}
                            {decoratedText}{' '}
                          </Typography>
                        )}
                      >
                        <Typography className={styles.commentText}>
                          {item.comment}
                        </Typography>
                      </Linkify>
                      <div className={styles.feedbacks}>
                        {item.sharedFeedback && (
                          <div className={styles.feedback}>
                            <div className={styles.feedbackHeader}>
                              <Typography variant="h4">
                                {t('sharedFeedbackLabel')}
                              </Typography>
                            </div>
                            <Linkify
                              componentDecorator={(
                                decoratedHref,
                                decoratedText,
                                key,
                              ) => (
                                <Typography
                                  component="a"
                                  variant="link"
                                  target="blank"
                                  href={decoratedHref}
                                  key={key}
                                >
                                  {' '}
                                  {decoratedText}{' '}
                                </Typography>
                              )}
                            >
                              <Typography className={styles.feedbackText}>
                                {item.sharedFeedback}
                              </Typography>
                            </Linkify>
                          </div>
                        )}
                        {isManager && item.privateFeedback && (
                          <div
                            className={cn(
                              styles.feedback,
                              styles.privateFeedback,
                            )}
                          >
                            <div className={styles.feedbackHeader}>
                              <Typography variant="h4">
                                {t('notesForManagerLabel')}
                              </Typography>
                            </div>
                            <Linkify
                              componentDecorator={(
                                decoratedHref,
                                decoratedText,
                                key,
                              ) => (
                                <Typography
                                  component="a"
                                  variant="link"
                                  target="blank"
                                  href={decoratedHref}
                                  key={key}
                                >
                                  {' '}
                                  {decoratedText}{' '}
                                </Typography>
                              )}
                            >
                              <Typography className={styles.feedbackText}>
                                {item.privateFeedback}
                              </Typography>
                            </Linkify>
                          </div>
                        )}
                        {!item.sharedFeedback && !item.privateFeedback && (
                          <div
                            className={cn(
                              styles.feedbackHeader,
                              styles.feedbackWaringMessage,
                            )}
                          >
                            <Icon
                              name="notAvailable"
                              className={styles.warningIcon}
                            />
                            <Typography variant="h3">
                              {t('noFeedbackText')}
                            </Typography>
                          </div>
                        )}
                      </div>
                    </div>
                  </Collapsible>
                );
              } else {
                return (
                  <Collapsible
                    key={item._id}
                    open={
                      !openCollapsible.find((question) => question === item._id)
                    }
                    onTriggerClosing={() => onCollapsibleTriggerClick(item._id)}
                    className={cn(styles.item, {
                      [styles.noPrinting]:
                        item.grade === '5' &&
                        isFiveStarAnswersPrinting === false,
                    })}
                    openedClassName={cn(styles.openedItem, {
                      [styles.noPrinting]:
                        item.grade === '5' &&
                        isFiveStarAnswersPrinting === false,
                    })}
                    trigger={
                      <div className={styles.trigger}>
                        <Icon name="question" className={styles.triggerIcon} />
                        <img
                          src="/assets/icons/question.svg"
                          alt="circle question icon"
                          height="24px"
                          width="24px"
                          className={styles.triggerIconForPrint}
                        />
                        <Typography className={styles.questionText}>
                          {item.question}
                        </Typography>
                        <Typography
                          variant="h4"
                          className={cn(styles.ratingItem, {
                            [styles.noPrinting]: isMarksPrinting === false,
                          })}
                        >
                          {item.grade}
                        </Typography>
                        <Icon
                          name="chevronDown"
                          className={styles.chevronIcon}
                        />
                      </div>
                    }
                    triggerWhenOpen={
                      <div className={styles.trigger}>
                        <Icon name="question" className={styles.triggerIcon} />
                        <img
                          src="/assets/icons/question.svg"
                          alt="circle question icon"
                          height="24px"
                          width="24px"
                          className={styles.triggerIconForPrint}
                        />
                        <Typography className={styles.questionText}>
                          {item.question}
                        </Typography>
                        <Typography
                          variant="h4"
                          className={cn(styles.ratingItem, {
                            [styles.noPrinting]: isMarksPrinting !== true,
                          })}
                        >
                          {item.grade}
                        </Typography>
                        <Icon name="chevronUp" className={styles.chevronIcon} />
                      </div>
                    }
                  >
                    <div className={styles.inner}>
                      <Linkify
                        componentDecorator={(
                          decoratedHref,
                          decoratedText,
                          key,
                        ) => (
                          <Typography
                            component="a"
                            variant="link"
                            target="blank"
                            href={decoratedHref}
                            key={key}
                          >
                            {' '}
                            {decoratedText}{' '}
                          </Typography>
                        )}
                      >
                        <Typography className={styles.commentText}>
                          {item.comment}
                        </Typography>
                      </Linkify>
                      <div className={styles.feedbacks}>
                        {item.sharedFeedback && (
                          <div className={styles.feedback}>
                            <div className={styles.feedbackHeader}>
                              <Typography variant="h4">
                                {t('sharedFeedbackLabel')}
                              </Typography>
                            </div>
                            <Linkify
                              componentDecorator={(
                                decoratedHref,
                                decoratedText,
                                key,
                              ) => (
                                <Typography
                                  component="a"
                                  variant="link"
                                  target="blank"
                                  href={decoratedHref}
                                  key={key}
                                >
                                  {' '}
                                  {decoratedText}{' '}
                                </Typography>
                              )}
                            >
                              <Typography className={styles.feedbackText}>
                                {item.sharedFeedback}
                              </Typography>
                            </Linkify>
                          </div>
                        )}
                        {isManager && item.privateFeedback && (
                          <div
                            className={cn(
                              styles.feedback,
                              styles.privateFeedback,
                            )}
                          >
                            <div className={styles.feedbackHeader}>
                              <Typography variant="h4">
                                {t('notesForManagerLabel')}
                              </Typography>
                            </div>
                            <Linkify
                              componentDecorator={(
                                decoratedHref,
                                decoratedText,
                                key,
                              ) => (
                                <Typography
                                  component="a"
                                  variant="link"
                                  target="blank"
                                  href={decoratedHref}
                                  key={key}
                                >
                                  {' '}
                                  {decoratedText}{' '}
                                </Typography>
                              )}
                            >
                              <Typography className={styles.feedbackText}>
                                {item.privateFeedback}
                              </Typography>
                            </Linkify>
                          </div>
                        )}
                        {!item.sharedFeedback && !item.privateFeedback && (
                          <div
                            className={cn(
                              styles.feedbackHeader,
                              styles.feedbackWaringMessage,
                            )}
                          >
                            <Icon
                              name="notAvailable"
                              className={styles.warningIcon}
                            />
                            <Typography variant="h3">
                              {t('noFeedbackText')}
                            </Typography>
                          </div>
                        )}
                      </div>
                    </div>
                  </Collapsible>
                );
              }
            })}
        </div>
      </div>
    </div>
  );
};
export default React.memo(FeedbackPage);
