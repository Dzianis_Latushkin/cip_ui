import React, { useState, useRef, useEffect } from 'react';
import { useSelector } from 'react-redux';
import { useNavigate, useParams } from 'react-router-dom';
import { useReactToPrint } from 'react-to-print';
import _isEmpty from 'lodash/isEmpty';
import { useTranslation } from 'react-i18next';

import { userSelector } from 'reducers/selectors';

import * as api from 'api/requests/requests';

import { getResultDate } from './FeedbackPage.helpers';

import { useFetch } from 'hooks/useFetch';
import { useModal } from 'hooks/useModal';

import { MainLayout } from 'components/Layout';
import { Icon } from 'components/shared/Icon';
import { Button } from 'components/shared/Button';

import FeedbackPage from './FeedbackPage';
import { PrintOptionsModal } from './components/PrintOptionsModal';

import styles from './FeedbackPage.styles.scss';

const FeedbackPageContainer = () => {
  const { t } = useTranslation('feedbackPage');

  const navigate = useNavigate();
  const { id: feedbackId, employeeFeedbackId } = useParams();

  const user = useSelector(userSelector);
  const currentUserId = user.currentUser._id;

  const pageRef = useRef();
  const [openCollapsible, setOpenCollapsible] = useState([]);
  const [printOptions, setPrintOptions] = useState({
    isMarksPrinting: true,
    isDurationPrinting: true,
    isFiveStarAnswersPrinting: true,
    isEmptyFeedbackPrinting: true,
  });

  const [
    isPrintOptionsModalOpen,
    openPrintOptionsModal,
    closePrintOptionsModal,
  ] = useModal({});

  const {
    data: { feedback: managerFeedback },
    loading: feedbackLoading,
  } = useFetch(
    {
      defaultData: { feedback: {} },
      fetcher: api.getFeedback,
      immediate: true,
      stopRequest: !feedbackId,
    },
    feedbackId,
  );

  const {
    data: { feedback: employeeFeedback },
    loading: employeeFeedbackLoading,
  } = useFetch(
    {
      defaultData: { feedback: {} },
      fetcher: api.getFeedback,
      immediate: true,
      stopRequest: !employeeFeedbackId,
    },
    employeeFeedbackId,
  );

  const handleEditFeedbackClick = () => {
    navigate(`/feedback/${feedbackId}/edit`);
  };

  const handleExportPdf = useReactToPrint({
    content: () => pageRef.current,
  });

  const handleExportPdfClick = () => {
    openPrintOptionsModal();
    setOpenCollapsible([]);
  };

  const handleApplyPrintOptions = () => {
    closePrintOptionsModal();
    handleExportPdf();
  };

  const loading = feedbackLoading || employeeFeedbackLoading;

  const feedback = _isEmpty(employeeFeedback)
    ? managerFeedback
    : employeeFeedback;
  const isManager = _isEmpty(employeeFeedback);

  useEffect(() => {
    if (feedback.interview?.title) {
      document.title = `${feedback.interview.title} - ${
        isManager ? feedback.employee.name : feedback.creator.name
      } - ${getResultDate(feedback.updatedAt)}`;
    }

    return () => {
      document.title = t('webPageTitle');
    };
  }, [feedback]);

  const handleReturnClick = (status) => {
    if (isManager) {
      if (status === 'completed') {
        navigate('/interviews');
      }
      if (status === 'archived') {
        navigate('/archive');
      }
    } else {
      navigate('/results');
    }
  };

  const handleCollapsibleTriggerClick = (itemId) => {
    setOpenCollapsible([...openCollapsible, itemId]);
  };

  const handlePrintOptionsChange = (option) => {
    setPrintOptions({ ...printOptions, [option]: !printOptions[option] });
  };

  return (
    <MainLayout
      loading={loading}
      pageRef={pageRef}
      pageTitle={
        <Button
          onClick={() => handleReturnClick(feedback.interview?.status)}
          startIcon={<Icon name="arrowLeftCircle" />}
          customIcon={styles.returnIcon}
          className={styles.returnButton}
        >
          {t('backButton')}
        </Button>
      }
    >
      <FeedbackPage
        t={t}
        setOpenCollapsible={setOpenCollapsible}
        isManager={isManager}
        isMarksPrinting={printOptions.isMarksPrinting}
        isDurationPrinting={printOptions.isDurationPrinting}
        isFiveStarAnswersPrinting={printOptions.isFiveStarAnswersPrinting}
        isEmptyFeedbackPrinting={printOptions.isEmptyFeedbackPrinting}
        openCollapsible={openCollapsible}
        feedback={feedback}
        currentUserId={currentUserId}
        onEditFeedbackClick={handleEditFeedbackClick}
        onExportPdfClick={handleExportPdfClick}
        onCollapsibleTriggerClick={handleCollapsibleTriggerClick}
      />
      <PrintOptionsModal
        t={t}
        isOpen={isPrintOptionsModalOpen}
        printOptions={printOptions}
        onClose={closePrintOptionsModal}
        onApply={handleApplyPrintOptions}
        handlePrintOptionsChange={handlePrintOptionsChange}
      />
    </MainLayout>
  );
};

export default React.memo(FeedbackPageContainer);
