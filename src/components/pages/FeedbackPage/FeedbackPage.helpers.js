export const getResultRating = (feedback) => {
  if (feedback) {
    const nextValue = feedback.reduce((acc, item) => acc + +item.grade, 0);

    return (nextValue / feedback.length).toFixed(1);
  }

  return null;
};

export const getResultDate = (time) => {
  if (time) {
    return new Date(Number(time)).toLocaleDateString();
  }

  return 0;
};

export const getResultTime = (time) => {
  if (time) {
    return Number(time) > 3599
      ? new Date(time * 1000).toISOString().substr(11, 8)
      : new Date(time * 1000).toISOString().substr(14, 5);
  }

  return 0;
};
