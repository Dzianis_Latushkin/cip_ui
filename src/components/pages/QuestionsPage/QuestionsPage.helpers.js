import React from 'react';
import cn from 'classnames';

import { Group } from 'components/Group';
import { Question } from 'components/Question';

import styles from './QuestionsPage.styles.scss';

export const groupRender = ({
  groupData,
  onAddQuestionClick,
  onEditQuestionClick,
  onDeleteQuestionClick,
  onAddFolderClick,
  onEditFolderClick,
  onDeleteFolderClick,
}) =>
  groupData.map((group, index) => {
    if (group.folders) {
      return (
        <Group
          key={group._id}
          id={group._id}
          groupName={group.name}
          onAddQuestionClick={onAddQuestionClick}
          onAddFolderClick={onAddFolderClick}
          onEditFolderClick={onEditFolderClick}
          onDeleteFolderClick={onDeleteFolderClick}
          openedClassName={cn(styles.openedFirst, {
            [styles.opened]: group.parentId,
          })}
          closedClassName={cn(styles.closedFirst, {
            [styles.closed]: group.parentId,
          })}
        >
          {groupRender({
            groupData: group.folders,
            onAddQuestionClick,
            onEditQuestionClick,
            onDeleteQuestionClick,
            onAddFolderClick,
            onEditFolderClick,
            onDeleteFolderClick,
          })}
          {group.questions.map((item) => (
            <Question
              key={item._id}
              id={item._id}
              groupId={group._id}
              question={item.question}
              comment={item.comment}
              onEditQuestionClick={onEditQuestionClick}
              onDeleteQuestionClick={onDeleteQuestionClick}
            />
          ))}
        </Group>
      );
    } else {
      return (
        <Group
          key={group._id}
          id={group._id}
          groupName={group.name}
          onAddQuestionClick={onAddQuestionClick}
          onAddFolderClick={onAddFolderClick}
          onEditFolderClick={onEditFolderClick}
          onDeleteFolderClick={onDeleteFolderClick}
          openedClassName={cn(styles.openedFirst, {
            [styles.opened]: group.parentId,
          })}
          closedClassName={cn(styles.closedFirst, {
            [styles.closed]: group.parentId,
          })}
        >
          {group.questions.map((item) => (
            <Question
              key={item._id}
              id={item._id}
              groupId={group._id}
              question={item.question}
              comment={item.comment}
              onAddQuestionClick={onAddQuestionClick}
              onEditQuestionClick={onEditQuestionClick}
              onDeleteQuestionClick={onDeleteQuestionClick}
            />
          ))}
        </Group>
      );
    }
  });
