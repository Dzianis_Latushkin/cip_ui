import React, { useState } from 'react';

import * as api from 'api/requests/requests';

import QuestionModal from './QuestionModal';

const QuestionModalContainer = ({
  t,
  edit,
  id,
  parentId,
  groupId,
  editData,
  setFolders,
  onClose,
  ...restProps
}) => {
  const [loading, setLoading] = useState(false);

  const handleSubmit = (values, actions) => {
    setLoading(true);

    const { title, comment } = values;

    if (edit) {
      api
        .editQuestion(id, { question: title, comment, folderId: groupId })
        .then((res) => {
          setFolders((prevState) => {
            return {
              ...prevState,
              data: {
                folders: prevState.data.folders.map((folder) => {
                  if (folder._id === res.data.question.folderId) {
                    return {
                      ...folder,
                      questions: folder.questions.map((quest) => {
                        if (quest._id === res.data.question._id) {
                          return res.data.question;
                        }

                        return quest;
                      }),
                    };
                  }

                  return folder;
                }),
              },
            };
          });
          setLoading(false);
          onClose();
        })
        .catch((err) => {
          setLoading(false);
          actions.setFieldError('title', err);
        });
    } else {
      api
        .addQuestion({ question: title, comment, folderId: id })
        .then((res) => {
          setFolders((prevState) => {
            return {
              ...prevState,
              data: {
                folders: prevState.data.folders.map((folder) => {
                  if (folder._id === res.data.question.folderId) {
                    return {
                      ...folder,
                      questions: [...folder.questions, res.data.question],
                    };
                  }

                  return folder;
                }),
              },
            };
          });
          setLoading(false);
          onClose();
        })
        .catch((err) => {
          setLoading(false);
          actions.setFieldError('title', err);
        });
    }
  };

  return (
    <QuestionModal
      t={t}
      loading={loading}
      edit={edit}
      editData={editData}
      onSubmit={handleSubmit}
      onClose={onClose}
      {...restProps}
    />
  );
};

export default React.memo(QuestionModalContainer);
