import * as Yup from 'yup';

export const getValidationSchema = (t) =>
  Yup.object().shape({
    title: Yup.string()
      .required(t('groupModal.titleValidationError'))
      .test('isValid', t('groupModal.titleSpaceError'), (value) => {
        if (value) {
          return value.replace(/\s/g, '').length;
        }

        return true;
      }),
  });

export const initialValues = {
  title: '',
};
