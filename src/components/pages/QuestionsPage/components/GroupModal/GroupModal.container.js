import React, { useState } from 'react';

import * as api from 'api/requests/requests';

import GroupModal from './GroupModal';

const GroupModalContainer = ({
  t,
  edit,
  id,
  editData,
  setFolders,
  onClose,
  ...restProps
}) => {
  const [loading, setLoading] = useState(false);

  const handleSubmit = (values, actions) => {
    setLoading(true);

    const { title } = values;
    if (edit) {
      api
        .editFolder(id, { name: title })
        .then((res) => {
          setLoading(false);
          setFolders((prevState) => {
            return {
              ...prevState,
              data: {
                ...prevState.data,
                folders: prevState.data.folders.map((folder) => {
                  if (folder._id === res.data.folder._id) {
                    return { ...folder, name: res.data.folder.name };
                  }

                  return folder;
                }),
              },
            };
          });
          onClose();
        })
        .catch((err) => {
          setLoading(false);
          actions.setFieldError('title', err);
        });
    } else {
      api
        .addFolder({ name: title, parentId: id })
        .then((res) => {
          setLoading(false);
          setFolders((prevState) => {
            return {
              ...prevState,
              data: {
                ...prevState.data,
                folders: [...prevState.data.folders, res.data.folder],
              },
            };
          });

          onClose();
        })
        .catch((err) => {
          setLoading(false);
          actions.setFieldError('title', err);
        });
    }
  };

  return (
    <GroupModal
      t={t}
      loading={loading}
      edit={edit}
      editData={editData}
      onSubmit={handleSubmit}
      onClose={onClose}
      {...restProps}
    />
  );
};

export default React.memo(GroupModalContainer);
