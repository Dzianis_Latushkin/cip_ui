import React from 'react';
import { Formik, Form, Field } from 'formik';

import { getValidationSchema } from './GroupModal.formConfig';
import { initialValues } from './GroupModal.formConfig';
import { convertDataToForm } from './GroupModal.helpers';

import { Modal } from 'components/shared/Modal';
import { Button } from 'components/shared/Button';
import { Typography } from 'components/shared/Typography';

import { InputField } from 'components/FormikFields';

import styles from './GroupModal.styles.scss';

const GroupModal = ({
  loading,
  edit,
  editData,
  t,
  onSubmit,
  isGroupModalOpen,
  onClose,
}) => {
  const modalTitle = edit
    ? `${t('groupModal.editModalTitle')}`
    : `${t('groupModal.addModalTitle')}`;

  return (
    <Modal
      open={isGroupModalOpen}
      centered
      className={styles.modal}
      onClose={onClose}
      isEdit={edit}
    >
      <Typography variant="h1" className={styles.title}>
        {modalTitle}
      </Typography>
      <Formik
        validationSchema={getValidationSchema(t)}
        validateOnMount
        initialValues={edit ? convertDataToForm(editData) : initialValues}
        onSubmit={onSubmit}
      >
        {({ values, errors, touched }) => (
          <Form className={styles.form}>
            <Field
              label={t('groupModal.titleLabel')}
              name="title"
              placeholder={t('groupModal.titlePlaceholder')}
              component={InputField}
              errorId={'title-error'}
            />
            <div className={styles.buttonBar}>
              <Button type="submit" loading={loading} className={styles.button}>
                {t('groupModal.confirmButton')}
              </Button>
              <Button
                variant="outlined"
                onClick={onClose}
                className={styles.button}
              >
                {t('groupModal.cancelButton')}
              </Button>
            </div>
          </Form>
        )}
      </Formik>
    </Modal>
  );
};

export default GroupModal;
