import React from 'react';

import { groupRender } from './QuestionsPage.helpers';

import { Icon } from 'components/shared/Icon';
import { Button } from 'components/shared/Button';
import { Typography } from 'components/shared/Typography';

import styles from './QuestionsPage.styles.scss';

const QuestionsPage = ({
  t,
  groupData,
  onAddQuestionClick,
  onEditQuestionClick,
  onDeleteQuestionClick,
  onAddFolderClick,
  onEditFolderClick,
  onDeleteFolderClick,
}) => (
  <div className={styles.page}>
    <div className={styles.titleContainer}>
      <Typography variant="h2" className={styles.title}>
        {t('groupsTitle')}
      </Typography>
      <Typography variant="h3">{t('groupsDescription')}</Typography>
    </div>
    <div className={styles.buttonContainer}>
      <Button
        variant="default"
        onClick={() => {
          onAddFolderClick(null);
        }}
        endIcon={<Icon name="plus" />}
        className={styles.button}
        data-testid="add-group"
      >
        {t('addGroupButton')}
      </Button>
    </div>
    <div className={styles.questionsGroupContainer}>
      {groupRender({
        groupData,
        onAddQuestionClick,
        onEditQuestionClick,
        onDeleteQuestionClick,
        onAddFolderClick,
        onEditFolderClick,
        onDeleteFolderClick,
      })}
    </div>
  </div>
);

export default React.memo(QuestionsPage);
