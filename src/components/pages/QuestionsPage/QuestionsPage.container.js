import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';

import * as api from 'api/requests/requests';

import { normalizeGroupData } from 'helpers/normalizeGroupData';

import { useModal } from 'hooks/useModal';
import { useFetch } from 'hooks/useFetch';

import { MainLayout } from 'components/Layout';

import { GroupModal } from './components/GroupModal';
import { ConfirmModal } from 'components/ConfirmModal';
import { QuestionModal } from './components/QuestionModal';

import QuestionsPage from './QuestionsPage';

const QuestionsPageContainer = () => {
  const { t } = useTranslation('questionsPage');

  const [confirmLoading, setConfrimLoading] = useState(false);

  const [
    isQuestionModalOpen,
    openQuestionModal,
    closeQuestionModal,
    questionModalData,
  ] = useModal({});

  const [isGroupModalOpen, openGroupModal, closeGroupModal, groupModalData] =
    useModal({});

  const [isConfirmModalOpen, openConfirmModal, closeConfirmModal, modalData] =
    useModal({});

  const {
    data: { folders },
    loading,
    setState: setFolders,
  } = useFetch(
    { defaultData: [], fetcher: api.getAllFolders, immediate: true },
    {},
  );

  const questions = folders || [];

  const handleAddQuestionClick = (id) => {
    openQuestionModal({ id });
  };

  const handleEditQuestionClick = (id, groupId) => {
    const editData = questions
      .find((question) => question._id === groupId)
      .questions.find((question) => question._id === id);

    openQuestionModal({
      edit: true,
      id,
      groupId,
      editData: editData,
    });
  };

  const handleRemoveQuestionClick = (id, groupId) => {
    openConfirmModal({
      titleText: t('confirmQuestionModalTitle'),
      contentText: t('confirmQuestionModalContent'),
      onConfirmClick: () => handleRemoveQuestion(id, groupId),
      onCancelClick: () => closeConfirmModal(),
    });
  };

  const handleRemoveQuestion = (id, groupId) => {
    setConfrimLoading(true);

    api
      .deleteQuestion(id)
      .then((res) => {
        setFolders((prevState) => {
          return {
            ...prevState,
            data: {
              ...prevState.data,
              folders: prevState.data.folders.map((folder) => {
                if (folder._id === groupId) {
                  return {
                    ...folder,
                    questions: folder.questions.filter(
                      (question) => question._id !== id,
                    ),
                  };
                }

                return folder;
              }),
            },
          };
        });

        setConfrimLoading(false);
        closeConfirmModal();
      })
      .catch(() => {
        setConfrimLoading(false);
      });
  };

  const handleAddFolderClick = (id) => {
    openGroupModal({ id });
  };

  const handleEditFolder = (id) => {
    const editData = questions.find((question) => question._id === id);

    openGroupModal({ edit: true, id, editData: editData });
  };

  const handleRemoveFolderClick = (id) => {
    openConfirmModal({
      titleText: t('confirmGroupModalTitle'),
      contentText: t('confirmGroupModalContent'),
      onConfirmClick: () => handleRemoveFolder(id),
      onCancelClick: () => closeConfirmModal(),
    });
  };

  const handleRemoveFolder = (id) => {
    setConfrimLoading(true);
    api
      .deleteFolder(id)
      .then((res) => {
        setFolders((prevState) => {
          return {
            ...prevState,
            data: {
              ...prevState.data,
              folders: prevState.data.folders.filter(
                (folder) => folder._id !== id,
              ),
            },
          };
        });

        setConfrimLoading(false);
        closeConfirmModal();
      })
      .catch(() => {
        setConfrimLoading(false);
      });
  };

  return (
    <MainLayout loading={loading} pageTitle={t('pageTitle')}>
      <QuestionsPage
        t={t}
        groupData={normalizeGroupData(questions)}
        onAddQuestionClick={handleAddQuestionClick}
        onEditQuestionClick={handleEditQuestionClick}
        onDeleteQuestionClick={handleRemoveQuestionClick}
        onAddFolderClick={handleAddFolderClick}
        onEditFolderClick={handleEditFolder}
        onDeleteFolderClick={handleRemoveFolderClick}
      />
      <GroupModal
        {...groupModalData}
        t={t}
        isGroupModalOpen={isGroupModalOpen}
        setFolders={setFolders}
        onClose={closeGroupModal}
      />
      <QuestionModal
        {...questionModalData}
        t={t}
        isQuestionModalOpen={isQuestionModalOpen}
        setFolders={setFolders}
        onClose={closeQuestionModal}
      />
      <ConfirmModal
        {...modalData}
        loading={confirmLoading}
        isOpen={isConfirmModalOpen}
        onClose={closeConfirmModal}
      />
    </MainLayout>
  );
};

export default React.memo(QuestionsPageContainer);
