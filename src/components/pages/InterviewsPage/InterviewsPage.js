import React from 'react';

import { INTERVIEW_CARD_TYPE } from 'constants/common';

import { Typography } from 'components/shared/Typography';

import { InterviewCard } from 'components/InterviewCard';
import { ActionsCard } from './components/ActionsCard';

import styles from './InterviewsPage.styles.scss';

const InterviewsPage = ({
  navigate,
  plannedInterviews,
  completedInterviews,
  t,
  onCreateFromTemplate,
  onDuplicateInterviewClick,
  onRemoveInterviewClick,
  onArchiveInterviewClick,
  onResultsClick,
  onStartInterviewClick,
}) => (
  <div className={styles.container}>
    <div className={styles.interviewsGroup}>
      {plannedInterviews.length > 0 ? (
        <Typography className={styles.title} variant="h2" component="h2">
          {t('plannedHeader')}
        </Typography>
      ) : (
        ''
      )}

      <div className={styles.interviews}>
        <ActionsCard
          navigate={navigate}
          t={t}
          onCreateFromTemplate={onCreateFromTemplate}
        />
        {plannedInterviews.reverse().map((interview) => (
          <InterviewCard
            key={interview._id}
            t={t}
            variant={INTERVIEW_CARD_TYPE.PLANNED}
            navigate={navigate}
            interview={interview}
            onDuplicateInterviewClick={onDuplicateInterviewClick}
            onRemoveInterviewClick={onRemoveInterviewClick}
            onStartInterviewClick={onStartInterviewClick}
          />
        ))}
      </div>
    </div>
    <div className={styles.interviewsGroup}>
      {completedInterviews.length > 0 ? (
        <Typography className={styles.title} variant="h2" component="h2">
          {t('completedHeader')}
        </Typography>
      ) : (
        ''
      )}

      <div className={styles.interviews}>
        {completedInterviews.reverse().map((interview) => (
          <InterviewCard
            key={interview._id}
            t={t}
            variant={INTERVIEW_CARD_TYPE.COMPLETED}
            navigate={navigate}
            interview={interview}
            onResultsClick={onResultsClick}
            onDuplicateInterviewClick={onDuplicateInterviewClick}
            onArchiveInterviewClick={onArchiveInterviewClick}
          />
        ))}
      </div>
    </div>
  </div>
);

export default React.memo(InterviewsPage);
