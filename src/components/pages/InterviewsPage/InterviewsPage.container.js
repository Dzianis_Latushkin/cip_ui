import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { useTranslation } from 'react-i18next';

import { INTERVIEWS_PAGE_TYPE } from 'constants/common';

import * as api from 'api/requests/requests';

import { showToast } from 'helpers/showToast';
import { changeTitle } from 'helpers/changeTitleInterviewsCard';
import { divideInterviews } from 'helpers/divideInterviews';

import { useModal } from 'hooks/useModal';
import { useFetch } from 'hooks/useFetch';

import { MainLayout } from 'components/Layout';

import { ConfirmModal } from 'components/ConfirmModal';
import { InterviewTemplateModal } from './components/InterviewTemplateModal';

import InterviewsPage from './InterviewsPage';

const InterviewsPageContainer = () => {
  const navigate = useNavigate();

  const [confirmLoading, setConfrimLoading] = useState(false);

  const [isConfirmModalOpen, openConfirmModal, closeConfirmModal, modalData] =
    useModal({});

  const { t } = useTranslation('interviewsPage');
  const [tNotifications] = useTranslation('notifications');

  const [
    isInterviewTemplateModalOpen,
    openInterviewTemplateModal,
    closeInterviewTemplateModal,
    interviewTemplateModalData,
  ] = useModal({});

  const {
    data: { interviews },
    loading,
    setState: setInterviews,
  } = useFetch(
    {
      defaultData: [],
      fetcher: api.getManagersInterviews,
      immediate: true,
    },
    '?status[0]=planned&status[1]=completed',
  );

  const myInterviews = interviews || [];

  const { planned, completed } = divideInterviews(
    myInterviews,
    INTERVIEWS_PAGE_TYPE.INTERVIEWS,
  );

  const handleDuplicateInterviewClick = (id) => {
    const nextInterview = myInterviews.find(
      (interview) => interview._id === id,
    );
    const nextTitle = changeTitle(nextInterview.title, planned, nextInterview);

    api
      .createInterview({
        title: nextTitle,
        status: 'planned',
        employee: nextInterview.employee._id,
        questions: nextInterview.questions,
      })
      .then((res) => {
        setInterviews((prevState) => {
          return {
            ...prevState,
            data: {
              ...prevState.data,
              interviews: [...prevState.data.interviews, res.data.interview],
            },
          };
        });
      })
      .catch();
  };

  const handleRemoveInterviewClick = (id) => {
    openConfirmModal({
      titleText: t('confirmModalTitle'),
      contentText: t('confirmModalContent'),
      onConfirmClick: () => handleRemoveInterview(id),
      onCancelClick: () => closeConfirmModal(),
    });
  };

  const handleArchiveInterviewClick = (id) => {
    const nextInterview = myInterviews.find(
      (interview) => interview._id === id,
    );

    api
      .editInterview(id, {
        title: nextInterview.title,
        status: 'archived',
        employee: nextInterview.employee._id,
        questions: nextInterview.questions,
        feedbackId: nextInterview.feedback._id,
      })
      .then((res) => {
        setInterviews((prevState) => {
          return {
            ...prevState,
            data: {
              ...prevState.data,
              interviews: prevState.data.interviews.filter(
                (item) => item._id !== id,
              ),
            },
          };
        });
        closeConfirmModal();

        showToast(tNotifications('archiveInterviewNotification'));
      })
      .catch(() => {
        setConfrimLoading(false);
      });
  };

  const handleRemoveInterview = (id) => {
    api
      .deleteInterview(id)
      .then((res) => {
        setInterviews((prevState) => {
          return {
            ...prevState,
            data: {
              ...prevState.data,
              interviews: prevState.data.interviews.filter(
                (item) => item._id !== id,
              ),
            },
          };
        });
        closeConfirmModal();
      })
      .catch(() => {
        setConfrimLoading(false);
      });
  };

  const handleResultsClick = (id) => {
    navigate(`/feedback/${id}`);
  };

  const handleCreateFromTemplate = () => {
    openInterviewTemplateModal();
  };

  const handleStartInterviewClick = (id) => {
    navigate(`/interview/passing/${id}`);
  };

  return (
    <MainLayout loading={loading} pageTitle={t('pageTitle')}>
      <InterviewsPage
        navigate={navigate}
        plannedInterviews={planned}
        completedInterviews={completed}
        t={t}
        setInterviews={setInterviews}
        onCreateFromTemplate={handleCreateFromTemplate}
        onDuplicateInterviewClick={handleDuplicateInterviewClick}
        onRemoveInterviewClick={handleRemoveInterviewClick}
        onArchiveInterviewClick={handleArchiveInterviewClick}
        onResultsClick={handleResultsClick}
        onStartInterviewClick={handleStartInterviewClick}
      />
      <ConfirmModal
        {...modalData}
        loading={confirmLoading}
        isOpen={isConfirmModalOpen}
        onClose={closeConfirmModal}
      />
      <InterviewTemplateModal
        {...interviewTemplateModalData}
        navigate={navigate}
        isOpen={isInterviewTemplateModalOpen}
        t={t}
        onClose={closeInterviewTemplateModal}
      />
    </MainLayout>
  );
};

export default React.memo(InterviewsPageContainer);
