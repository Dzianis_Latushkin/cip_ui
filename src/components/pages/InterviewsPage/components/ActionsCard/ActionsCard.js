import React from 'react';

import { Icon } from 'components/shared/Icon';
import { Typography } from 'components/shared/Typography';

import styles from './ActionsCard.styles.scss';

const ActionsCard = ({ t, navigate, onCreateFromTemplate }) => (
  <div className={styles.container}>
    <div
      onClick={() => {
        navigate('/interview/create');
      }}
      className={styles.option}
    >
      <Icon className={styles.icon} name="plusCircle" />
      <Typography variant="link" className={styles.title}>
        {t('actionsCard.planInterviewButton')}
      </Typography>
    </div>
    <div className={styles.option}>
      <Icon className={styles.icon} name="plusCircle" />
      <Typography
        onClick={onCreateFromTemplate}
        variant="link"
        className={styles.title}
      >
        {t('actionsCard.createFromTemplateButton')}
      </Typography>
    </div>
  </div>
);

export default React.memo(ActionsCard);
