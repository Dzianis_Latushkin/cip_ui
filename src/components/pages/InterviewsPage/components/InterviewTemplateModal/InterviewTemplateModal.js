import React from 'react';
import { Formik, Form, Field } from 'formik';
import cn from 'classnames';

import formConfig from './InterviewTemplateModal.formConfig';
import { convertDataToForm } from './InterviewTemplateModal.helpers';

import { RadioField } from 'components/FormikFields';

import { Icon } from 'components/shared/Icon';
import { Modal } from 'components/shared/Modal';
import { Button } from 'components/shared/Button';
import { Loading } from 'components/shared/Loading';
import { Typography } from 'components/shared/Typography';
import { ErrorMessage } from 'components/shared/ErrorMessage';
import { ScrollWrapper } from 'components/shared/ScrollWrapper';

import styles from './InterviewTemplateModal.styles.scss';

const templatesRender = (templatesList, templateGroupTitle, t) => (
  <div className={styles.templateGroup}>
    <Typography variant="h3" component="h3" className={styles.infoMessageText}>
      {t(templateGroupTitle)}
    </Typography>

    {templatesList.length ? (
      templatesList.map((item) => (
        <div className={styles.row}>
          <Field
            className={styles.radioField}
            key={item._id}
            component={RadioField}
            name="template"
            value={item._id}
            label={item.title}
          />
          <Icon
            name="template"
            className={styles.eyeIcon}
            onClick={() => window.open(`/template/${item._id}`, '_blank')}
          />
        </div>
      ))
    ) : (
      <div className={styles.infoMessage}>
        <div className={styles.infoMessageIconWrap}>
          <Icon name="info" className={styles.infoMessageIcon} />
        </div>
        <Typography
          variant="h3"
          component="h3"
          className={styles.infoMessageText}
        >
          {t('interviewTemplateModal.noTemplateText')}
        </Typography>
      </div>
    )}
  </div>
);

const InterviewTemplateModal = ({
  t,
  loading,
  templatesLoading,
  isOpen,
  isEmptyTeplates,
  templates,
  privateSharedTemplates,
  otherSharedTemplates,
  onSubmit,
  onClose,
  onCreateTemplate,
}) => (
  <Modal open={isOpen} centered className={styles.modal} onClose={onClose}>
    <Typography variant="h1" className={styles.modalTitle}>
      {t('interviewTemplateModal.title')}
    </Typography>

    <Formik
      {...formConfig}
      validateOnMount
      initialValues={templatesLoading && convertDataToForm(templates)}
      onSubmit={onSubmit}
    >
      {({ values, errors, touched }) => (
        <Form className={styles.form}>
          {templatesLoading ? (
            <Loading variant="primary" size={30} className={styles.loading} />
          ) : (
            <div className={styles.modalBody}>
              {!isEmptyTeplates ? (
                <ScrollWrapper
                  translateContentSizeYToHolder
                  className={styles.scrollWrapper}
                  thumbClassName={styles.thumb}
                >
                  {templates.length > 0 &&
                    templatesRender(
                      templates,
                      'interviewTemplateModal.myTemplates',
                      t,
                    )}

                  {privateSharedTemplates.length > 0 &&
                    templatesRender(
                      privateSharedTemplates,
                      'interviewTemplateModal.privateSharedTemplates',
                      t,
                    )}

                  {otherSharedTemplates.length > 0 &&
                    templatesRender(
                      otherSharedTemplates,
                      'interviewTemplateModal.otherSharedTemplates',
                      t,
                    )}

                  {errors.template && (
                    <ErrorMessage className={styles.errorMessage}>
                      {errors.template}
                    </ErrorMessage>
                  )}
                </ScrollWrapper>
              ) : (
                <div className={styles.emptyResult}>
                  <Icon className={styles.icon} name="notAvailable" />
                  <div>
                    <Typography variant="h4" className={styles.title}>
                      {t('interviewTemplateModal.noTemplatesMessage')}
                    </Typography>
                    <Typography
                      variant="link"
                      className={styles.title}
                      onClick={onCreateTemplate}
                    >
                      {t('interviewTemplateModal.createLink')}
                    </Typography>
                  </div>
                </div>
              )}
            </div>
          )}
          <div className={styles.buttonBar}>
            <Button
              disabled={!isEmptyTeplates ? false : true}
              className={
                !isEmptyTeplates
                  ? styles.button
                  : cn(styles.button, styles.disabled)
              }
              type="submit"
              variant="default"
              loading={loading}
            >
              {t('interviewTemplateModal.createButton')}
            </Button>
            <Button
              variant="outlined"
              className={styles.button}
              onClick={onClose}
            >
              {t('interviewTemplateModal.cancelButton')}
            </Button>
          </div>
        </Form>
      )}
    </Formik>
  </Modal>
);

export default InterviewTemplateModal;
