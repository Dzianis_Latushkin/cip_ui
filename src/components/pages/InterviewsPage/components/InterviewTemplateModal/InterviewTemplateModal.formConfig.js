import * as Yup from 'yup';

const validationSchema = Yup.object().shape({
  template: Yup.string(),
});

export const initialValues = {
  template: '',
};

export default {
  validationSchema,
};
