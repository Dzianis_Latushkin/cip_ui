import React, { useState } from 'react';

import * as api from 'api/requests/requests';

import { useFetch } from 'hooks/useFetch';

import InterviewTemplateModal from './InterviewTemplateModal';

const InterviewTemplateModalContainer = ({
  isOpen,
  navigate,
  openConfirm,
  onClose,
  ...restProps
}) => {
  const [loadingButton, setLoadingButton] = useState(false);

  const {
    data: {
      interviews: {
        templates = [],
        privateSharedTemplates = [],
        otherSharedTemplates = [],
      },
    },
    loading,
    setState: setTemplates,
  } = useFetch(
    {
      defaultData: {
        interviews: {
          templates: [],
          privateSharedTemplates: [],
          otherSharedTemplates: [],
        },
      },
      fetcher: api.getManagersInterviews,
      immediately: true,
      stopRequest: !isOpen,

      onSuccess: (response) => {
        setTemplates((prevState) => {
          const test = {
            ...prevState,
            data: {
              interviews: {
                templates: response.interviews
                  .filter((template) => !template.shared)
                  .sort(
                    ({ order: orderOne }, { order: orderTwo }) =>
                      orderOne - orderTwo,
                  ),
                privateSharedTemplates: response.interviews
                  .reverse()
                  .filter((template) => template.shared),
                otherSharedTemplates: response.sharedTemplates,
              },
            },
          };

          return test;
        });
      },
    },

    '?status=template',
  );

  const handleSubmit = (values, actions) => {
    setLoadingButton(true);
    navigate(`/interview/create?templateId=${values.template}`);
  };

  const handleCreateTemplate = () => {
    navigate('/template/createTemplate');
  };

  const isEmptyTeplates =
    templates.length === 0 &&
    privateSharedTemplates.length === 0 &&
    otherSharedTemplates.length === 0;

  return (
    <InterviewTemplateModal
      loading={loadingButton}
      templatesLoading={loading}
      isOpen={isOpen}
      isEmptyTeplates={isEmptyTeplates}
      templates={templates}
      privateSharedTemplates={privateSharedTemplates}
      otherSharedTemplates={otherSharedTemplates}
      onSubmit={handleSubmit}
      onClose={onClose}
      onCreateTemplate={handleCreateTemplate}
      {...restProps}
    />
  );
};

export default React.memo(InterviewTemplateModalContainer);
