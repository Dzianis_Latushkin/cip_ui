import React from 'react';
import { Formik, Form, Field } from 'formik';

import {
  getValidationSchema,
  initialValues,
} from './SecuritySettingsPage.formConfig';

import { Button } from 'components/shared/Button';
import { InputField } from 'components/FormikFields';

import styles from './SecuritySettingsPage.styles.scss';

const SecuritySettingsPage = ({ t, loading, onSubmit }) => (
  <Formik
    validationSchema={getValidationSchema(t)}
    validateOnMount
    initialValues={initialValues}
    onSubmit={onSubmit}
  >
    {({ dirty }) => (
      <Form>
        <Field
          label={t('securitySettings.currentPassword')}
          name="currentPassword"
          errorId={'current-password-error'}
          component={InputField}
          inputClassName={styles.inputField}
          type="password"
          autoComplete="off"
        />
        <Field
          label={t('securitySettings.newPassword')}
          name="newPassword"
          errorId={'new-password-error'}
          component={InputField}
          inputClassName={styles.inputField}
          type="password"
          autoComplete="off"
        />
        <Field
          label={t('securitySettings.repeatNewPassword')}
          name="repeatNewPassword"
          errorId={'repeat-password-error'}
          component={InputField}
          inputClassName={styles.inputField}
          type="password"
          autoComplete="off"
        />
        <Button
          type="submit"
          variant="default"
          loading={loading}
          disabled={!dirty}
        >
          {t('securitySettings.changePasswordButton')}
        </Button>
      </Form>
    )}
  </Formik>
);

export default SecuritySettingsPage;
