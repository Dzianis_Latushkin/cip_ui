import React, { useState } from 'react';

import { useSelector } from 'react-redux';

import * as api from 'api/requests/requests';

import { userSelector } from 'reducers/selectors';

import { showToast } from 'helpers/showToast';
import { convertDataToApi } from './SecuritySettingsPage.helpers';

import SecuritySettingsPage from './SecuritySettingsPage';

const SecuritySettingsPageContainer = ({ t, ...restProps }) => {
  const [loading, setLoading] = useState(false);
  const user = useSelector(userSelector);

  const handleSubmit = (values, actions) => {
    const userData = convertDataToApi(values);
    if (values.currentPassword === values.newPassword) {
      actions.setFieldError(
        'repeatNewPassword',
        t('securitySettings.sameChangedPasswordError'),
      );

      return;
    }
    setLoading(true);
    api
      .changeUserPassword(user.currentUser._id, userData)
      .then((res) => {
        setLoading(false);
        showToast(t('securitySettings.changePasswordNotification'));
      })
      .catch((err) => {
        console.log(err);
        setLoading(false);
        showToast(t('securitySettings.changePasswordErrorNotification'));
      });

    actions.resetForm({
      values: {
        currentPassword: '',
        newPassword: '',
        repeatNewPassword: '',
      },
    });
  };

  return (
    <SecuritySettingsPage t={t} loading={loading} onSubmit={handleSubmit} />
  );
};

export default React.memo(SecuritySettingsPageContainer);
