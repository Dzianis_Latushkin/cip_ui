import * as Yup from 'yup';

export const getValidationSchema = (t) =>
  Yup.object().shape({
    currentPassword: Yup.string()
      .transform((value, originalValue) =>
        /\s/.test(originalValue) ? NaN : value,
      )
      .typeError(t('securitySettings.spacePasswordError'))
      .required(t('securitySettings.requiredCurrentPasswordValidationError')),

    newPassword: Yup.string()
      .transform((value, originalValue) =>
        /\s/.test(originalValue) ? NaN : value,
      )
      .typeError(t('securitySettings.spacePasswordError'))
      .required(t('securitySettings.requiredNewPasswordValidationError'))
      .min(5, t('securitySettings.passwordMinLengthError')),

    repeatNewPassword: Yup.string()
      .transform((value, originalValue) =>
        /\s/.test(originalValue) ? NaN : value,
      )
      .typeError(t('securitySettings.spacePasswordError'))
      .required(t('securitySettings.requiredRepeatNewPasswordValidationError'))
      .min(5, t('securitySettings.passwordMinLengthError'))
      .oneOf(
        [Yup.ref('newPassword')],
        t('securitySettings.passwordMatchingError'),
      ),
  });

export const initialValues = {
  currentPassword: '',
  newPassword: '',
  repeatNewPassword: '',
};
