import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useDispatch, useSelector } from 'react-redux';

import * as api from 'api/requests/requests';

import { setUser } from 'reducers/userReducer';
import { userSelector } from 'reducers/selectors';

import { showToast } from 'helpers/showToast';
import { convertDataToApi } from './PersonalSettingsPage.helpers';

import PersonalSettingsPage from './PersonalSettingsPage';

const PersonalSettingsPageContainer = ({ t }) => {
  const [loading, setLoading] = useState(false);

  const user = useSelector(userSelector);
  const userName = user.currentUser.name;

  const [tNotifications] = useTranslation('notifications');
  const dispatch = useDispatch();

  const handleSubmit = (values, actions) => {
    setLoading(true);
    const userData = convertDataToApi(values);

    api
      .editEmployee(user.currentUser._id, userData)
      .then((res) => {
        setLoading(false);
        setUser(dispatch, res.data);
        showToast(tNotifications('changeNameNotification'));

        actions.resetForm({
          values: {
            name: userData.name,
          },
        });
      })
      .catch((err) => {
        setLoading(false);
        actions.setFieldError('name', err);
      });
  };

  const handleCancelClick = (setFieldValue) => {
    setFieldValue('name', userName);
  };

  return (
    <PersonalSettingsPage
      t={t}
      loading={loading}
      userName={userName}
      onSubmit={handleSubmit}
      onHandleCancelClick={handleCancelClick}
    />
  );
};

export default React.memo(PersonalSettingsPageContainer);
