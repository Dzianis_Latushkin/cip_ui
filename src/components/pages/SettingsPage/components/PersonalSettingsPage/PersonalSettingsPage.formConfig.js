import * as Yup from 'yup';

export const getValidationSchema = (t, userName) =>
  Yup.object().shape({
    name: Yup.string()
      .required(t('personalSettings.requiredNameValidationError'))
      .test('isValid', t('personalSettings.nameSpacesError'), (value) => {
        if (value) {
          return value.replace(/\s/g, '').length;
        }

        return true;
      }),
  });
