import React from 'react';
import { Formik, Form, Field } from 'formik';

import { convertDataToForm } from './PersonalSettingsPage.helpers';

import { getValidationSchema } from './PersonalSettingsPage.formConfig';

import { Button } from 'components/shared/Button';

import { InputField } from 'components/FormikFields';

import styles from './PersonalSettingsPage.styles.scss';

const PersonalSettingsPage = ({
  t,
  loading,
  userName,
  onSubmit,
  onHandleCancelClick,
}) => (
  <Formik
    validationSchema={getValidationSchema(t, userName)}
    validateOnMount
    initialValues={convertDataToForm(userName)}
    onSubmit={onSubmit}
  >
    {({ setFieldValue, dirty }) => (
      <Form>
        <Field
          label={t('personalSettings.name')}
          name="name"
          errorId={'name-error'}
          component={InputField}
          inputClassName={styles.inputField}
        />
        <div className={styles.ButtonBar}>
          <Button
            type="submit"
            variant="default"
            loading={loading}
            className={styles.button}
            disabled={!dirty}
          >
            {t('personalSettings.saveButton')}
          </Button>
          <Button
            variant="outlined"
            className={styles.button}
            onClick={() => onHandleCancelClick(setFieldValue)}
            disabled={!dirty}
          >
            {t('personalSettings.cancelButton')}
          </Button>
        </div>
      </Form>
    )}
  </Formik>
);

export default PersonalSettingsPage;
