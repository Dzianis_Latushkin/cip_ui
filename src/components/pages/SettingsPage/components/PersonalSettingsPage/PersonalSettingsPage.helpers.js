export const convertDataToApi = (values) => ({
  email: values.email,
  name: values.name,
  engLevel: values.englishLevel?.value,
  SMGlink: values.linkSMG || '',
  WODlink: values.linkWOD || '',
});

export const convertDataToForm = (name) => ({
  name,
});
