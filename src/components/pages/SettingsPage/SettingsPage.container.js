import React from 'react';
import { useTranslation } from 'react-i18next';

import { MainLayout } from 'components/Layout';

import SettingsPage from './SettingsPage';

const SettingsPageContainer = () => {
  const { t } = useTranslation('settingsPage');

  return (
    <MainLayout pageTitle="Settings">
      <SettingsPage t={t} />
    </MainLayout>
  );
};

export default SettingsPageContainer;
