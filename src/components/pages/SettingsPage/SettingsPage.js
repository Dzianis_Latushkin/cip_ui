import React from 'react';

import { Tabs, Tab } from 'components/shared/Tabs';

import { SecuritySettingsPage } from './components/SecuritySettinsPage';
import { PersonalSettingsPage } from './components/PersonalSettingsPage';

const SettingsPage = ({ t }) => (
  <Tabs size="large">
    <Tab label={t('personalLabel')}>
      <PersonalSettingsPage t={t} />
    </Tab>
    <Tab label={t('securityLabel')}>
      <SecuritySettingsPage t={t} />
    </Tab>
  </Tabs>
);

export default SettingsPage;
