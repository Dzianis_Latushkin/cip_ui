import React from 'react';
import { Navigate } from 'react-router-dom';

import { Loading } from 'components/shared/Loading';

import styles from './RedirectPage.styles.scss';

const RedirectPage = ({ userRoute }) =>
  userRoute ? (
    <Navigate to={{ pathname: userRoute }} />
  ) : (
    <Loading className={styles.loading} size={30} variant="primary" />
  );

export default React.memo(RedirectPage);
