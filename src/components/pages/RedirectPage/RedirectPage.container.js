import React, { useState, useEffect } from 'react';
import { useSelector } from 'react-redux';

import { ROLES } from 'constants/common';

import { userSelector } from 'reducers/selectors';

import RedirectPage from './RedirectPage';

const RedirectPageContainer = () => {
  const user = useSelector(userSelector);

  const [userRoute, setUserRoute] = useState(null);

  useEffect(() => {
    if (user.currentUser.role === ROLES.ADMIN) {
      setUserRoute('/managers');
    }

    if (user.currentUser.role === ROLES.MANAGER) {
      setUserRoute('/employees');
    }

    if (user.currentUser.role === ROLES.EMPLOYEE) {
      setUserRoute('/results');
    }
  }, []);

  return <RedirectPage userRoute={userRoute} />;
};

export default React.memo(RedirectPageContainer);
