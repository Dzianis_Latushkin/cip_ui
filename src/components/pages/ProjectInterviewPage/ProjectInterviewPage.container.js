import React, { useState, useRef } from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import { useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { useReactToPrint } from 'react-to-print';

import { PROJECT_INTERVIEW_PAGE_STATUS } from 'constants/common';

import * as api from 'api/requests/requests';

import { userSelector } from 'reducers/selectors';

import { showToast } from 'helpers/showToast';

import {
  convertDataToApi,
  setInitialQuestions,
  changeStatus,
} from './ProjectInterviewPage.helpers';

import { useFetch } from 'hooks/useFetch';
import { useModal } from 'hooks/useModal';

import { ConfirmModal } from 'components/ConfirmModal';
import ProjectInterviewPage from './ProjectInterviewPage';

const ProjectInterviewPageContainer = ({ status }) => {
  const { t } = useTranslation('projectInterviewPage');

  const [tNotifications] = useTranslation('notifications');

  const { id: entityId } = useParams();

  const navigate = useNavigate();
  const user = useSelector(userSelector);
  const role = user.currentUser.role;

  const pageRef = useRef();

  const [buttonsLoading, setButtonsLoading] = useState({
    saveAndQuit: false,
    saveAndContinue: false,
  });

  const [isConfirmModalOpen, openConfirmModal, closeConfirmModal, modalData] =
    useModal({});

  const {
    data: { projects },
  } = useFetch(
    {
      defaultData: { projects: [] },
      fetcher: api.getProjects,
      immediate: true,
    },
    {},
  );

  const formatProjects = (projectsData) => {
    return projectsData.map((project) => ({
      label: project.title,
      option: project._id,
    }));
  };

  const projectsToSelect = [
    { label: 'No project', option: null },
    ...formatProjects(projects),
  ];

  const {
    data: { projectInterview },
    loading: interviewLoading,
  } = useFetch(
    {
      defaultData: {
        projectInterview: {},
      },
      fetcher:
        role === 'employee'
          ? api.getProjectInterviewOne
          : api.getEmployeeProjectInterviewOne,
      immediately: true,
      stopRequest: status === 'CREATE_PROJECT_INTERVIEW',
    },
    { id: entityId },
  );

  const {
    data: { folders },
    setState: setQuestions,
  } = useFetch(
    {
      defaultData: { folders: [] },
      fetcher: api.getAllFolders,
      stopRequest: interviewLoading && status === 'EDIT_PROJECT_INTERVIEW',
      immediately: false,
      onSuccess: (res) => {
        if (status === 'EDIT_PROJECT_INTERVIEW') {
          setInitialQuestions({ projectInterview, setQuestions });
        }
      },
    },
    {},
    [interviewLoading],
  );

  const handleSaveAndQuitClick = (values, setFieldError) => {
    setButtonsLoading({
      saveAndQuit: true,
      saveAndContinue: false,
    });

    if (!values.employeeFeedback) {
      setButtonsLoading({
        saveAndQuit: false,
        saveAndContinue: false,
      });
      setFieldError('employeeFeedback', t('employeeFeedbackValidationError'));

      return;
    }

    if (!values.questions.length) {
      setButtonsLoading({
        saveAndQuit: false,
        saveAndContinue: false,
      });
      setFieldError('questions', t('questionsError'));

      return;
    }

    const data = convertDataToApi(values);

    status === PROJECT_INTERVIEW_PAGE_STATUS.CREATE_PROJECT_INTERVIEW &&
      api
        .postProjectInterviews(data)
        .then((res) => {
          api.postTrophyProjectInterviews(user.currentUser._id).then(() => {
            setButtonsLoading({
              saveAndQuit: false,
            });
            navigate('/project-interviews?trophyModal=true');
            showToast(tNotifications('saveInterviewNotification'));
          });
        })
        .catch((err) => {
          setButtonsLoading({
            saveAndQuit: false,
          });
          setFieldError('title', err);
        });

    (changeStatus(status, role) ===
      PROJECT_INTERVIEW_PAGE_STATUS.EDIT_PROJECT_INTERVIEW_MANAGER ||
      changeStatus(status, role) ===
        PROJECT_INTERVIEW_PAGE_STATUS.EDIT_PROJECT_INTERVIEW_EMPLOYEE) &&
      api
        .putEditProjectInterview(entityId, data)
        .then((res) => {
          navigate('/project-interviews');
          showToast(tNotifications('saveInterviewNotification'));
        })
        .catch((err) => {
          setButtonsLoading({
            saveAndQuit: false,
            saveAndContinue: false,
          });
          setFieldError('title', err);
        });
  };

  const handleCancelCreateInterviewClick = () => {
    openConfirmModal({
      titleText: t('confirmCancelCreationTitle'),
      contentText: t('confirmCancelCreationText'),
      onConfirmClick: () => {
        showToast(tNotifications('cancelNotification'));

        navigate('/project-interviews');
      },
      onCancelClick: () => closeConfirmModal(),
    });
  };

  const handleExportPdf = useReactToPrint({
    content: () => pageRef.current,
  });

  const handleEditInterviewClick = () => {
    navigate(`/project-interview/${entityId}/edit`);
  };

  return (
    <>
      <ProjectInterviewPage
        t={t}
        pageRef={pageRef}
        navigate={navigate}
        status={changeStatus(status, role)}
        loading={interviewLoading}
        interview={projectInterview}
        folders={folders}
        projects={projectsToSelect}
        setQuestions={setQuestions}
        buttonsLoading={buttonsLoading}
        onSaveAndQuitClick={handleSaveAndQuitClick}
        onCancelCreateInterviewClick={handleCancelCreateInterviewClick}
        onExportPdf={handleExportPdf}
        onEditInterviewClick={handleEditInterviewClick}
      />
      <ConfirmModal
        {...modalData}
        t={t}
        isOpen={isConfirmModalOpen}
        onClose={closeConfirmModal}
      />
    </>
  );
};

export default React.memo(ProjectInterviewPageContainer);
