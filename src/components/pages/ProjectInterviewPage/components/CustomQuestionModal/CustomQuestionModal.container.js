import React from 'react';
import { v4 as uuid } from 'uuid';

import { markListQuestionAsUnselected } from '../../ProjectInterviewPage.helpers';

import CustomQuestionModal from './CustomQuestionModal';

const CustomQuestionModalContainer = ({
  t,
  id,
  edit,
  index,
  question,
  onClose,
  arrayHelpers,
  setQuestions,
  isInsertBefore,
  ...restProps
}) => {
  const handleSubmit = (values) => {
    const { title } = values;
    if (!edit) {
      arrayHelpers.insert(index + (isInsertBefore ? 0 : 1), {
        grade: '5',
        privateFeedback: '',
        question: title,
        sharedFeedback: '',
        id: uuid(),
      });
    }

    if (edit) {
      arrayHelpers.replace(index, {
        grade: question.grade,
        question: title,
        privateFeedback: question.privateFeedback,
        sharedFeedback: question.sharedFeedback,
        id: question.id,
      });

      markListQuestionAsUnselected({ removeQuestion: question, setQuestions });
    }

    onClose();
  };

  return (
    <CustomQuestionModal
      t={t}
      edit={edit}
      question={question}
      onClose={onClose}
      onSubmit={handleSubmit}
      {...restProps}
    />
  );
};

export default React.memo(CustomQuestionModalContainer);
