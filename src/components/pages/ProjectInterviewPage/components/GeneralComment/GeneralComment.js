import React from 'react';
import { Field } from 'formik';
import cn from 'classnames';

import { PROJECT_INTERVIEW_PAGE_STATUS } from 'constants/common';

import { InputField } from 'components/FormikFields';

import { Icon } from 'components/shared/Icon';
import { Typography } from 'components/shared/Typography';
import { ErrorMessage } from 'components/shared/ErrorMessage';

import styles from './GeneralComment.styles.scss';

const noFeedbackAvailable = (t) => (
  <div className={cn(styles.feedbackHeader, styles.feedbackWaringMessage)}>
    <Icon name="notAvailable" className={styles.warningIcon} />
    <Typography variant="h3">{t('noFeedbackText')}</Typography>
  </div>
);

const GeneralComment = ({ t, status, errors, initialValues }) => {
  return (
    <>
      {status === PROJECT_INTERVIEW_PAGE_STATUS.VIEW_PROJECT_INTERVIEW && (
        <div className={styles.managerAndCustomerSummary}>
          <div className={styles.viewField}>
            <Typography variant="h4" className={styles.title}>
              {t('managerSummary')}
            </Typography>
            {initialValues.managerFeedback ? (
              <Typography variant="body" className={styles.text}>
                {initialValues.managerFeedback}
              </Typography>
            ) : (
              noFeedbackAvailable(t)
            )}
          </div>

          <div className={styles.viewField}>
            <Typography variant="h4" className={styles.title}>
              {t('customerSummary')}
            </Typography>
            {initialValues.customerFeedback ? (
              <Typography variant="body" className={styles.text}>
                {initialValues.customerFeedback}
              </Typography>
            ) : (
              noFeedbackAvailable(t)
            )}
          </div>
        </div>
      )}

      {status ===
        PROJECT_INTERVIEW_PAGE_STATUS.EDIT_PROJECT_INTERVIEW_MANAGER && (
        <>
          <div className={styles.managerAndCustomerSummary}>
            <Field
              label={t('managerSummary')}
              name="managerFeedback"
              component={InputField}
              multiline={true}
              readOnly={
                status === PROJECT_INTERVIEW_PAGE_STATUS.VIEW_PROJECT_INTERVIEW
              }
              inputClassName={styles.commentsField}
            />

            <Field
              label={t('customerSummary')}
              name="customerFeedback"
              component={InputField}
              multiline={true}
              readOnly={
                status === PROJECT_INTERVIEW_PAGE_STATUS.VIEW_PROJECT_INTERVIEW
              }
              inputClassName={styles.commentsField}
            />
          </div>
        </>
      )}

      {(status ===
        PROJECT_INTERVIEW_PAGE_STATUS.EDIT_PROJECT_INTERVIEW_MANAGER ||
        status === PROJECT_INTERVIEW_PAGE_STATUS.VIEW_PROJECT_INTERVIEW) && (
        <div className={styles.managerAndCustomerSummary}>
          <div className={styles.viewField}>
            <Typography variant="h4" className={styles.title}>
              {t('employeeSummary')}
            </Typography>
            <Typography variant="body" className={styles.text}>
              {initialValues.employeeFeedback}
            </Typography>
          </div>
        </div>
      )}

      {(status ===
        PROJECT_INTERVIEW_PAGE_STATUS.EDIT_PROJECT_INTERVIEW_EMPLOYEE ||
        status === PROJECT_INTERVIEW_PAGE_STATUS.CREATE_PROJECT_INTERVIEW) && (
        <div className={styles.commentFieldStyle}>
          <Field
            label={t('employeeSummary')}
            name="employeeFeedback"
            component={InputField}
            multiline={true}
            readOnly={
              status ===
                PROJECT_INTERVIEW_PAGE_STATUS.EDIT_PROJECT_INTERVIEW_MANAGER ||
              status === PROJECT_INTERVIEW_PAGE_STATUS.VIEW_PROJECT_INTERVIEW
            }
            inputClassName={styles.commentsField}
          />
          {errors.employeeFeedback && (
            <ErrorMessage className={styles.errorMessage}>
              {errors.employeeFeedback}
            </ErrorMessage>
          )}
        </div>
      )}
    </>
  );
};

export default React.memo(GeneralComment);
