import React from 'react';
import cn from 'classnames';
import { useSelector } from 'react-redux';

import { PROJECT_INTERVIEW_PAGE_STATUS } from 'constants/common';

import { Tag } from 'components/shared/Tag';
import { Icon } from 'components/shared/Icon';
import { Typography } from 'components/shared/Typography';
import { Button } from 'components/shared/Button';

import { userSelector } from 'reducers/selectors';

import styles from './InterviewInfo.styles.scss';

const renderStatusTag = (status, t) => {
  switch (status) {
    case 'pending':
      return <Tag variant="info">{t('pendingTag')}</Tag>;
    case 'failed':
      return <Tag variant="error">{t('failedTag')}</Tag>;
    case 'successful':
      return <Tag variant="success">{t('successfulTag')}</Tag>;
    case 'dismissed':
      return <Tag variant="default">{t('dismissedTag')}</Tag>;
    case true:
      return <Tag variant="info">{t('publishedTag')}</Tag>;
    case false:
      return <Tag variant="default">{t('unpublishedTag')}</Tag>;
  }
};

const InterviewInfo = ({
  t,
  status,
  interview,
  onExportPdf,
  isManagerEmployee,
  onEditInterviewClick,
}) => {
  const user = useSelector(userSelector);
  const role = user.currentUser.role;

  const passingDate = interview.passingDate
    ? new Date(Number(interview.passingDate)).toLocaleDateString()
    : interview.date;

  return (
    <div className={styles.statsContainer}>
      <div className={styles.titleWrapper}>
        <Typography variant="h1" className={styles.title}>
          {interview.title}
        </Typography>
        {status === PROJECT_INTERVIEW_PAGE_STATUS.VIEW_PROJECT_INTERVIEW && (
          <>
            {(!interview.published ||
              (role === 'manager' && isManagerEmployee)) && (
              <Button
                variant="outlined"
                className={styles.editButton}
                onClick={onEditInterviewClick}
              >
                {t('editButton')}
              </Button>
            )}
            <Button className={styles.exportPdfButton} onClick={onExportPdf}>
              {t('exportPdfButton')}
            </Button>
          </>
        )}
      </div>

      <div className={cn(styles.stats, styles.statsUserInfo)}>
        <div className={cn(styles.stats)}>
          <Icon name="user" className={styles.userIcon} />
          <Typography variant="h2" className={styles.userName}>
            {interview.employeeName}
          </Typography>
        </div>
        <div className={styles.stats}>
          <Icon name="suitcase" className={styles.userIcon} />
          <Typography variant="h2" className={styles.userName}>
            {interview.projectTitle}
          </Typography>
        </div>
      </div>
      <div className={styles.stats}>
        <Icon name="calendar" className={styles.statsIconSmall} />
        <Typography variant="h4" className={styles.statsText}>
          {interview.date} — Date of creation
        </Typography>
      </div>
      <div className={styles.passingDate}>
        <Icon name="calendar" className={styles.statsIconSmall} />
        <Typography variant="h4" className={styles.statsText}>
          {passingDate} — Date of passing
        </Typography>
      </div>

      {status === PROJECT_INTERVIEW_PAGE_STATUS.VIEW_PROJECT_INTERVIEW && (
        <div className={styles.interviewStatus}>
          <div className={styles.result}>
            <Typography variant="h4">{t('statusViewLabel')}</Typography>
            {renderStatusTag(interview.status.option, t)}
          </div>
          <div className={styles.result}>
            {renderStatusTag(interview.published, t)}
          </div>
        </div>
      )}
    </div>
  );
};

export default React.memo(InterviewInfo);
