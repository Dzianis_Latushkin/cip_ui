import React from 'react';
import cn from 'classnames';
import { Field } from 'formik';

import { PROJECT_INTERVIEW_PAGE_STATUS } from 'constants/common';

import { InputField } from 'components/FormikFields';

import { Icon } from 'components/shared/Icon';
import { ErrorMessage } from 'components/shared/ErrorMessage';

import styles from './FlexyInput.styles.scss';

const FlexyInput = ({ t, errors, status }) => {
  return (
    <div className={styles.titleContainer}>
      <div className={styles.title}>
        <span className={styles.hiddenValue}>{t('defaultTitle')}</span>
        <Field
          name="title"
          placeholder={t('defaultTitle')}
          component={InputField}
          formControlProps={{ className: styles.formControl }}
          readOnly={
            status ===
              PROJECT_INTERVIEW_PAGE_STATUS.EDIT_PROJECT_INTERVIEW_MANAGER ||
            status === PROJECT_INTERVIEW_PAGE_STATUS.VIEW_PROJECT_INTERVIEW
          }
          className={
            errors.title
              ? cn(styles.errorInput, styles.titleEditInput)
              : styles.titleEditInput
          }
          onClick={(event) => event.target.select()}
          endAdornment={<Icon name="pencil" className={styles.titleEditIcon} />}
          endAdornmentClassName={styles.endAdornment}
        />
      </div>
      {errors.title && (
        <ErrorMessage className={styles.errorMessage}>
          {errors.title}
        </ErrorMessage>
      )}
    </div>
  );
};

export default React.memo(FlexyInput);
