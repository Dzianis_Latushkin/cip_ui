import React, { useState } from 'react';
import { Field } from 'formik';

import {
  PROJECT_INTERVIEW_STATUS,
  PROJECT_INTERVIEW_PAGE_STATUS,
} from 'constants/common';

import { Typography } from 'components/shared/Typography';
import { SelectField, CheckboxField } from 'components/FormikFields';
import ReactDatePicker from 'react-datepicker';

import 'react-datepicker/dist/react-datepicker.css';
import styles from './Controls.styles.scss';

const projectField = (t, projects) => (
  <>
    <Typography variant="h4" className={styles.projectTitle}>
      {t('projectLabel')}
    </Typography>
    <Field
      name="project"
      placeholder={t('projectPlaceholder')}
      options={projects}
      component={SelectField}
      formClassName={styles.selectControl}
      inputClassName={styles.selectInput}
      className={styles.selectInput}
    />
  </>
);

const statusField = (t) => (
  <>
    <Typography variant="h4" className={styles.projectTitle}>
      {t('statusLabel')}
    </Typography>
    <Field
      name="status"
      placeholder={t('statusPlaceholder')}
      options={PROJECT_INTERVIEW_STATUS}
      component={SelectField}
      readOnly={status === PROJECT_INTERVIEW_PAGE_STATUS.VIEW_PROJECT_INTERVIEW}
      formClassName={styles.selectControl}
      inputClassName={styles.selectInput}
      className={styles.selectInput}
    />
  </>
);

const DatePicker = ({ field, form, passingDate }) => {
  const [startDate, setStartDate] = useState(
    passingDate ? new Date(+passingDate) : new Date(),
  );

  return (
    <ReactDatePicker
      wrapperClassName={styles.datePicker}
      selected={startDate}
      onChange={(date) => {
        setStartDate(date);
        form.setFieldValue(field.name, date.getTime().toString());
      }}
    />
  );
};

const dateField = (t, passingDate) => (
  <>
    <Typography variant="h4" className={styles.passingDateTitle}>
      {t('passingDateLabel')}
    </Typography>
    <Field
      name="passingDate"
      component={DatePicker}
      passingDate={passingDate}
    />
  </>
);

const Controls = ({ t, projects, status, values }) => {
  return (
    <div className={styles.constrols}>
      <div className={styles.projectSelectContainer}>
        {(status === PROJECT_INTERVIEW_PAGE_STATUS.CREATE_PROJECT_INTERVIEW ||
          status ===
            PROJECT_INTERVIEW_PAGE_STATUS.EDIT_PROJECT_INTERVIEW_EMPLOYEE) &&
          projectField(t, projects)}

        {(status === PROJECT_INTERVIEW_PAGE_STATUS.VIEW_PROJECT_INTERVIEW ||
          status ===
            PROJECT_INTERVIEW_PAGE_STATUS.EDIT_PROJECT_INTERVIEW_MANAGER) &&
          statusField(t)}

        {(status === PROJECT_INTERVIEW_PAGE_STATUS.CREATE_PROJECT_INTERVIEW ||
          status ===
            PROJECT_INTERVIEW_PAGE_STATUS.EDIT_PROJECT_INTERVIEW_EMPLOYEE) &&
          dateField(t, values.passingDate)}

        {status ===
          PROJECT_INTERVIEW_PAGE_STATUS.EDIT_PROJECT_INTERVIEW_MANAGER && (
          <div className={styles.checkbox}>
            <Field
              id="publishedInterviewCheckboxId"
              name="published"
              component={CheckboxField}
              label={t('publishedCheckboxLabel')}
              values={values}
            />
          </div>
        )}
      </div>
    </div>
  );
};

export default React.memo(Controls);
