import React from 'react';
import { Field } from 'formik';
import Linkify from 'react-linkify';
import cn from 'classnames';
import { Tooltip } from 'react-tippy';
import { useSelector } from 'react-redux';

import { Icon } from 'components/shared/Icon';
import { Typography } from 'components/shared/Typography';

import { InputField } from 'components/FormikFields';

import { userSelector } from 'reducers/selectors';

import { PROJECT_INTERVIEW_PAGE_STATUS } from 'constants/common';

import styles from './QuestionItem.styles.scss';

const noFeedbackAvailable = (t) => (
  <div className={cn(styles.feedbackHeader, styles.feedbackWaringMessage)}>
    <Icon name="notAvailable" className={styles.warningIcon} />
    <Typography variant="h3">{t('noFeedbackText')}</Typography>
  </div>
);
const conditionalRenderField = (status, answer, fieldRole) => {
  switch (status) {
    case PROJECT_INTERVIEW_PAGE_STATUS.CREATE_PROJECT_INTERVIEW:
      return true;

    case PROJECT_INTERVIEW_PAGE_STATUS.EDIT_PROJECT_INTERVIEW_EMPLOYEE:
      return true;

    case PROJECT_INTERVIEW_PAGE_STATUS.EDIT_PROJECT_INTERVIEW_MANAGER:
      if (fieldRole === 'employee') {
        return !!answer;
      } else {
        return true;
      }

    case PROJECT_INTERVIEW_PAGE_STATUS.VIEW_PROJECT_INTERVIEW:
      return !!answer;
  }
};

const QuestionItem = ({
  t,
  questionsCount,
  index,
  status,
  question,
  comment,
  order,
  orderItem,
  name,
  item,
  arrayHelpers,
  onAddOwnQuestionClick,
  onDeleteQuestionClick,
  onEditQuestionClick,
}) => {
  const user = useSelector(userSelector);
  const role = user.currentUser.role;

  return (
    <div
      className={cn(styles.questionItem, {
        [styles.editOrViewQuestionItem]:
          status ===
            PROJECT_INTERVIEW_PAGE_STATUS.EDIT_PROJECT_INTERVIEW_MANAGER ||
          status === PROJECT_INTERVIEW_PAGE_STATUS.VIEW_PROJECT_INTERVIEW,
      })}
    >
      {(status === PROJECT_INTERVIEW_PAGE_STATUS.CREATE_PROJECT_INTERVIEW ||
        status ===
          PROJECT_INTERVIEW_PAGE_STATUS.EDIT_PROJECT_INTERVIEW_EMPLOYEE) &&
      index === 0 ? (
        <div className={cn(styles.addHotQuestion)}>
          <Icon
            name="plusCircle"
            className={styles.addIcon}
            onClick={() => {
              onAddOwnQuestionClick({
                index,
                arrayHelpers,
                isInsertBefore: true,
              });
            }}
          />
        </div>
      ) : null}
      <div className={styles.question}>
        <div className={styles.questionContainer}>
          <div className={styles.questionWrapper}>
            <Icon name="question" className={styles.questionIcon} />
            <Typography variant="body" className={styles.questionContent}>
              {question}
            </Typography>
          </div>
          <div className={styles.titleInfo}>
            <Typography
              variant="body"
              className={styles.order}
            >{`${orderItem} / ${order}`}</Typography>
          </div>
        </div>

        <div className={styles.commentContainer}>
          <Linkify>
            <Typography variant="body" className={styles.commentContent}>
              {comment}
            </Typography>
          </Linkify>
        </div>
        <div className={styles.questionSummary}>
          <div className={styles.answerColumn}>
            {((status ===
              PROJECT_INTERVIEW_PAGE_STATUS.VIEW_PROJECT_INTERVIEW &&
              role === 'manager') ||
              status ===
                PROJECT_INTERVIEW_PAGE_STATUS.EDIT_PROJECT_INTERVIEW_MANAGER) && (
              <Typography variant="h3" className={styles.label}>
                {t('employeeFeedback')}
              </Typography>
            )}

            {conditionalRenderField(status, item.employeeAnswer, 'employee') ? (
              status === PROJECT_INTERVIEW_PAGE_STATUS.VIEW_PROJECT_INTERVIEW ||
              status ===
                PROJECT_INTERVIEW_PAGE_STATUS.EDIT_PROJECT_INTERVIEW_MANAGER ? (
                <Typography variant="body" className={styles.text}>
                  {item.employeeAnswer}
                </Typography>
              ) : (
                <Field
                  name={`questions[${name}].employeeAnswer`}
                  placeholder={t('question.employeeQuestionAnswer')}
                  component={InputField}
                  multiline={true}
                  readOnly={
                    status ===
                      PROJECT_INTERVIEW_PAGE_STATUS.VIEW_PROJECT_INTERVIEW ||
                    status ===
                      PROJECT_INTERVIEW_PAGE_STATUS.EDIT_PROJECT_INTERVIEW_MANAGER
                  }
                  inputClassName={styles.answerField}
                  formControlProps={{ className: styles.formControl }}
                />
              )
            ) : (
              noFeedbackAvailable(t)
            )}
          </div>

          {(status ===
            PROJECT_INTERVIEW_PAGE_STATUS.EDIT_PROJECT_INTERVIEW_MANAGER ||
            (status === PROJECT_INTERVIEW_PAGE_STATUS.VIEW_PROJECT_INTERVIEW &&
              role === 'manager')) && (
            <div className={styles.answerColumn}>
              <Typography variant="h3" className={styles.label}>
                {t('managerNotes')}
              </Typography>
              {conditionalRenderField(
                status,
                item.managerComment,
                'manager',
              ) ? (
                status ===
                PROJECT_INTERVIEW_PAGE_STATUS.VIEW_PROJECT_INTERVIEW ? (
                  <Typography variant="body" className={styles.text}>
                    {item.managerComment}
                  </Typography>
                ) : (
                  <Field
                    name={`questions[${name}].managerComment`}
                    placeholder={t('question.managerQuestionComment')}
                    component={InputField}
                    multiline={true}
                    readOnly={
                      status ===
                      PROJECT_INTERVIEW_PAGE_STATUS.VIEW_PROJECT_INTERVIEW
                    }
                    inputClassName={styles.answerField}
                    formControlProps={{ className: styles.formControl }}
                  />
                )
              ) : (
                noFeedbackAvailable(t)
              )}
            </div>
          )}
        </div>

        <div className={styles.questionRowWrapper}>
          {(status === PROJECT_INTERVIEW_PAGE_STATUS.CREATE_PROJECT_INTERVIEW ||
            status ===
              PROJECT_INTERVIEW_PAGE_STATUS.EDIT_PROJECT_INTERVIEW_EMPLOYEE) && (
            <Tooltip
              html={
                <div className={styles.tooltip}>
                  <div
                    className={styles.tooltipItem}
                    onClick={() => {
                      onEditQuestionClick({ index, arrayHelpers });
                    }}
                  >
                    <Icon name="pencil" className={styles.tooltipItemIcon} />
                    <Typography
                      variant="body"
                      className={styles.tooltipItemText}
                    >
                      {t('questionMenu.editQuestion')}
                    </Typography>
                  </div>
                  <div
                    className={styles.tooltipItem}
                    onClick={() => {
                      onDeleteQuestionClick({ index, arrayHelpers });
                    }}
                  >
                    <Icon name="delete" className={styles.tooltipItemIcon} />
                    <Typography
                      variant="body"
                      className={styles.tooltipItemText}
                    >
                      {t('questionMenu.deleteQuestion')}
                    </Typography>
                  </div>
                </div>
              }
              position="bottom-end"
              trigger="click"
            >
              <Icon name="more" className={styles.questionMenu} />
            </Tooltip>
          )}
        </div>
      </div>

      {(status === PROJECT_INTERVIEW_PAGE_STATUS.CREATE_PROJECT_INTERVIEW ||
        status ===
          PROJECT_INTERVIEW_PAGE_STATUS.EDIT_PROJECT_INTERVIEW_EMPLOYEE) &&
        (index !== questionsCount - 1 ? (
          <div className={styles.addHotQuestion}>
            <Icon
              name="plusCircle"
              className={styles.addIcon}
              onClick={() =>
                onAddOwnQuestionClick({
                  index,
                  arrayHelpers,
                  isInsertBefore: false,
                })
              }
            />
          </div>
        ) : (
          <div className={styles.addHotQuestionEmpty} />
        ))}
    </div>
  );
};

export default React.memo(QuestionItem);
