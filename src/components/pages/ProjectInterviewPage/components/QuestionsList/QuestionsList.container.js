import React, { useState } from 'react';
import { useFormikContext } from 'formik';

import { useModal } from 'hooks/useModal';

import { ConfirmModal } from 'components/ConfirmModal';

import { AlertModal } from 'components/AlertModal';
import { CustomQuestionModal } from '../CustomQuestionModal';

import QuestionsList from './QuestionsList';

const QuestionsListContainer = ({ t, edit, setQuestions, errors, status }) => {
  const { values } = useFormikContext();

  const [
    isCustomQuestionModalOpen,
    openCustomQuestionModal,
    closeCustomQuestionModal,
    customQuestionModalData,
  ] = useModal({});

  const [isAlertModalOpen, openAlertModal, closeAlertModal, alertModalData] =
    useModal({});

  const [isConfirmModalOpen, openConfirmModal, closeConfirmModal, modalData] =
    useModal({});

  const [isInsertBeforeState, setIsInsertBeforeState] = useState(false);

  const handleAddOwnQuestionClick = ({
    index,
    arrayHelpers,
    isInsertBefore,
  }) => {
    setIsInsertBeforeState(isInsertBefore);
    openCustomQuestionModal({ index, arrayHelpers });
  };

  const handleDeleteQuestion = ({ index, arrayHelpers }) => {
    arrayHelpers.remove(index);
    closeConfirmModal();
  };

  const handleDeleteQuestionClick = ({ index, arrayHelpers }) => {
    if (values.questions.length <= 1) {
      openAlertModal({
        contentText: t('alertErrorText'),
      });

      return;
    }

    openConfirmModal({
      titleText: t('confirmModalTitle'),
      contentText: t('confirmModalText'),
      onConfirmClick: () => {
        handleDeleteQuestion({ index, arrayHelpers });
      },
      onCancelClick: () => closeConfirmModal(),
    });
  };

  const handleEditQuestionClick = ({ index, arrayHelpers }) => {
    const question = values.questions.find(
      (item, itemIndex) => itemIndex === index,
    );

    openCustomQuestionModal({ edit: true, question, index, arrayHelpers });
  };

  return (
    <>
      <QuestionsList
        t={t}
        status={status}
        edit={edit}
        values={values}
        errors={errors}
        onAddOwnQuestionClick={handleAddOwnQuestionClick}
        onEditQuestionClick={handleEditQuestionClick}
        onDeleteQuestionClick={handleDeleteQuestionClick}
      />
      <CustomQuestionModal
        {...customQuestionModalData}
        t={t}
        isInsertBefore={isInsertBeforeState}
        isCustomQuestionModalOpen={isCustomQuestionModalOpen}
        setQuestions={setQuestions}
        onClose={closeCustomQuestionModal}
      />
      <AlertModal
        {...alertModalData}
        t={t}
        isOpen={isAlertModalOpen}
        onClose={closeAlertModal}
      />
      <ConfirmModal
        {...modalData}
        t={t}
        isOpen={isConfirmModalOpen}
        onClose={closeConfirmModal}
      />
    </>
  );
};

export default React.memo(QuestionsListContainer);
