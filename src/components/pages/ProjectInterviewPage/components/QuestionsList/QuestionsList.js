import React from 'react';
import { FieldArray } from 'formik';

import { PROJECT_INTERVIEW_PAGE_STATUS } from 'constants/common';

import { Button } from 'components/shared/Button';

import { QuestionItem } from '../QuestionItem';

import { ErrorMessage } from 'components/shared/ErrorMessage';

import styles from './QuestionsList.styles.scss';

const QuestionsList = ({
  t,
  values,
  errors,
  status,
  interview,
  onAddOwnQuestionClick,
  onEditQuestionClick,
  onDeleteQuestionClick,
}) => (
  <FieldArray
    name="questions"
    render={(arrayHelpers) => (
      <>
        <div className={styles.interviewContainer}>
          {values.questions &&
            values.questions.map((item, index) => {
              return (
                <QuestionItem
                  t={t}
                  questionsCount={values.questions.length}
                  index={index}
                  key={item.id || item._id}
                  name={`${index}`}
                  question={item.question}
                  comment={item.comment}
                  order={values.questions.length}
                  orderItem={index + 1}
                  arrayHelpers={arrayHelpers}
                  status={status}
                  interview={interview}
                  item={item}
                  onAddOwnQuestionClick={onAddOwnQuestionClick}
                  onDeleteQuestionClick={onDeleteQuestionClick}
                  onEditQuestionClick={onEditQuestionClick}
                />
              );
            })}
        </div>

        {(status === PROJECT_INTERVIEW_PAGE_STATUS.CREATE_PROJECT_INTERVIEW ||
          status ===
            PROJECT_INTERVIEW_PAGE_STATUS.EDIT_PROJECT_INTERVIEW_EMPLOYEE) && (
          <div className={styles.controls}>
            <Button
              className={styles.button}
              onClick={() =>
                onAddOwnQuestionClick({
                  index: values.questions.length,
                  arrayHelpers,
                })
              }
            >
              {t('addOwnQuestionButton')}
            </Button>
          </div>
        )}
        {errors?.questions && <ErrorMessage>{errors.questions}</ErrorMessage>}
      </>
    )}
  />
);

export default QuestionsList;
