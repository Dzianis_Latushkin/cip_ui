import { isEqual } from 'lodash';
import _upperFirst from 'lodash/upperFirst';

import { ROLES, PROJECT_INTERVIEW_PAGE_STATUS } from 'constants/common';

import { initialValues } from './ProjectInterviewPage.formConfig';

export const getResultDate = (time) => {
  if (time) {
    return new Date(Number(time)).toLocaleDateString();
  }

  return 0;
};

export const convertDataToApi = (values) => ({
  project: values.project.option,
  title: values.title,
  employeeFeedback: values.employeeFeedback,
  managerFeedback: values.managerFeedback,
  customerFeedback: values.customerFeedback,
  passingDate: values.passingDate || new Date().getTime().toString(),
  status: values.status ? values.status.option : 'pending',
  published: values.published ? values.published : false,
  questions: values.questions.map((question) => ({
    question: question.question,
    comment: question.comment,
    employeeAnswer: question.employeeAnswer,
    managerComment: question.managerComment,
    description: 'should was',
  })),
});

export const convertDataToForm = ({ status, interview, t }) => {
  const values = {
    ...initialValues(t),
    title: interview.title || '',
    projectTitle: interview.project?.title || t('noProjectTitle'),
    employeeName: interview.creator?.name || '',
    date: getResultDate(interview.createdAt) || '',
    passingDate: interview.passingDate || '',
    employeeFeedback: interview.employeeFeedback || '',
    managerFeedback: interview.managerFeedback || '',
    customerFeedback: interview.customerFeedback || '',
    project: interview.project
      ? { option: interview.project._id, label: interview.project.title }
      : { option: null, label: 'Select project' },
    status: { option: interview.status, label: _upperFirst(interview.status) },
    published: interview.published,
    questions: interview
      ? interview.questions?.map((question) => ({
          question: question.question,
          comment: question.comment,
          employeeAnswer: question.employeeAnswer,
          managerComment: question.managerComment,
          id: question._id,
        }))
      : [],
  };

  return status !== PROJECT_INTERVIEW_PAGE_STATUS.CREATE_PROJECT_INTERVIEW
    ? values
    : initialValues(t);
};

export const changeStatus = (status, role) => {
  if (status === 'CREATE_PROJECT_INTERVIEW') {
    return PROJECT_INTERVIEW_PAGE_STATUS.CREATE_PROJECT_INTERVIEW;
  }
  if (status === 'EDIT_PROJECT_INTERVIEW' && role === ROLES.EMPLOYEE) {
    return PROJECT_INTERVIEW_PAGE_STATUS.EDIT_PROJECT_INTERVIEW_EMPLOYEE;
  }
  if (status === 'EDIT_PROJECT_INTERVIEW' && role === ROLES.MANAGER) {
    return PROJECT_INTERVIEW_PAGE_STATUS.EDIT_PROJECT_INTERVIEW_MANAGER;
  }
  if (status === 'VIEW_PROJECT_INTERVIEW') {
    return PROJECT_INTERVIEW_PAGE_STATUS.VIEW_PROJECT_INTERVIEW;
  }
};

export const findFormItemByIndex = ({ index, arrayHelpers }) => {
  return arrayHelpers.form.values.questions.find(
    (question, questionIndex) => questionIndex === index,
  );
};

export function toInterviewDate(interviewDate) {
  const date = new Date(+interviewDate);
  let day = date.getDate();
  let month = date.getMonth() + 1;
  let year = date.getFullYear();

  if (day < 10) {
    day = `0${day}`;
  }

  if (month < 10) {
    month = `0${month}`;
  }

  return `${day}.${month}.${year}`;
}

export const setInitialQuestions = ({ projectInterview, setQuestions }) => {
  setQuestions((prevState) => ({
    ...prevState,
    data: {
      folders: prevState.data.folders?.map((nextFolder) => {
        return {
          ...nextFolder,
          questions: nextFolder.questions?.map((nextQuestion) => {
            if (
              projectInterview.questions.some(
                (question) => question.question === nextQuestion.question,
              )
            ) {
              return { ...nextQuestion, isSelected: true };
            }

            return nextQuestion;
          }),
        };
      }),
    },
  }));
};

export const markListQuestionAsSelected = ({ question, setQuestions }) => {
  setQuestions((prevState) => {
    return {
      ...prevState,
      data: {
        folders: prevState.data.folders.map((nextFolder) => {
          return {
            ...nextFolder,
            questions: nextFolder.questions.map((nextQuestion) => {
              if (nextQuestion.question === question) {
                return { ...nextQuestion, isSelected: true };
              }

              return nextQuestion;
            }),
          };
        }),
      },
    };
  });
};

export const markListQuestionAsUnselected = ({
  removeQuestion,
  setQuestions,
}) => {
  setQuestions((prevState) => ({
    ...prevState,
    data: {
      folders: prevState.data.folders.map((nextFolder) => ({
        ...nextFolder,
        questions: nextFolder.questions.map((nextQuestion) => {
          if (
            nextQuestion.question === removeQuestion.question ||
            nextQuestion._id === removeQuestion._id
          ) {
            return { ...nextQuestion, isSelected: false };
          }

          return nextQuestion;
        }),
      })),
    },
  }));
};

export const checkIsInterviewChanged = ({ values, interview }) => {
  const nextFeedbackQuestions = values.feedback.map((question) => ({
    comment: question.comment,
    question: question.question,
    _id: question.id,
  }));

  return !isEqual(interview?.questions, nextFeedbackQuestions);
};

export const getPassedTime = (secondsPassed) => {
  return secondsPassed > 3599
    ? new Date(secondsPassed * 1000).toISOString().substr(11, 8)
    : new Date(secondsPassed * 1000).toISOString().substr(14, 5);
};
