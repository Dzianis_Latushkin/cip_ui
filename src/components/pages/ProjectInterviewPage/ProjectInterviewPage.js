import React from 'react';
import { Formik, Form } from 'formik';
import { useSelector } from 'react-redux';
import { useQuery } from 'hooks/useQuery';

import { PROJECT_INTERVIEW_PAGE_STATUS } from 'constants/common';

import { userSelector } from 'reducers/selectors';

import { getValidationSchema } from './ProjectInterviewPage.formConfig';

import { convertDataToForm } from './ProjectInterviewPage.helpers';

import { Icon } from 'components/shared/Icon';
import { Button } from 'components/shared/Button';
import { MainLayout } from 'components/Layout';

import { Controls } from './components/Controls';
import { FlexyInput } from './components/FlexyInput';
import { QuestionsList } from './components/QuestionsList';
import { GeneralComment } from './components/GeneralComment';
import { InterviewInfo } from './components/InterviewInfo';

import styles from './ProjectInterviewPage.styles.scss';

const ProjectInterviewPage = ({
  t,
  navigate,
  buttonsLoading,
  loading,
  interview,
  status,
  projects,
  pageRef,
  setQuestions,
  onSaveAndQuitClick,
  onCancelCreateInterviewClick,
  onEditInterviewClick,
  onExportPdf,
}) => {
  const query = useQuery();
  const isFromProjectsHistory = query.get('fromPH');

  const user = useSelector(userSelector);

  const isManagerEmployee = user.currentUser.employees.includes(
    interview.creator?._id,
  );

  return (
    <MainLayout
      loading={
        status !== PROJECT_INTERVIEW_PAGE_STATUS.CREATE_PROJECT_INTERVIEW &&
        loading
      }
      pageTitle={
        status === PROJECT_INTERVIEW_PAGE_STATUS.VIEW_PROJECT_INTERVIEW && (
          <Button
            onClick={() => {
              isFromProjectsHistory
                ? navigate('/projects-history')
                : navigate(-1);
            }}
            startIcon={<Icon name="arrowLeftCircle" />}
            customIcon={styles.returnIcon}
            className={styles.returnButton}
          >
            {t('backButton')}
          </Button>
        )
      }
      pageRef={pageRef}
    >
      <div className={styles.logoWrapper}>
        <Icon name="logoFullVersion" className={styles.logo} />
      </div>
      <div className={styles.page}>
        <Formik
          validateOnMount
          enableReinitialize={true}
          initialValues={convertDataToForm({ status, interview, t })}
          validationSchema={getValidationSchema(t)}
        >
          {({ values, errors, setFieldError }) => (
            <Form>
              {(status ===
                PROJECT_INTERVIEW_PAGE_STATUS.EDIT_PROJECT_INTERVIEW_EMPLOYEE ||
                status ===
                  PROJECT_INTERVIEW_PAGE_STATUS.CREATE_PROJECT_INTERVIEW) && (
                <div className={styles.topContainer}>
                  <FlexyInput t={t} errors={errors} status={status} />
                  <div className={styles.buttonTopContainer}>
                    <Button
                      className={styles.button}
                      variant="outlined"
                      onClick={() => {
                        onCancelCreateInterviewClick(values);
                      }}
                    >
                      {t('controls.cancelButton')}
                    </Button>

                    <Button
                      disabled={buttonsLoading.saveAndContinue}
                      onClick={() => onSaveAndQuitClick(values, setFieldError)}
                      loading={buttonsLoading.saveAndQuit}
                      className={styles.button}
                    >
                      {t('controls.saveAndQuitButton')}
                    </Button>
                  </div>
                </div>
              )}

              {status !==
                PROJECT_INTERVIEW_PAGE_STATUS.EDIT_PROJECT_INTERVIEW_EMPLOYEE &&
                status !==
                  PROJECT_INTERVIEW_PAGE_STATUS.CREATE_PROJECT_INTERVIEW && (
                  <div className={styles.topContainerManager}>
                    <InterviewInfo
                      t={t}
                      status={status}
                      interview={convertDataToForm({ status, interview, t })}
                      onExportPdf={onExportPdf}
                      onEditInterviewClick={onEditInterviewClick}
                      isManagerEmployee={isManagerEmployee}
                    />
                    {status ===
                      PROJECT_INTERVIEW_PAGE_STATUS.EDIT_PROJECT_INTERVIEW_MANAGER && (
                      <div className={styles.buttonTopContainer}>
                        <Button
                          className={styles.button}
                          variant="outlined"
                          onClick={() => {
                            onCancelCreateInterviewClick(values);
                          }}
                        >
                          {t('controls.cancelButton')}
                        </Button>

                        <Button
                          disabled={buttonsLoading.saveAndContinue}
                          onClick={() =>
                            onSaveAndQuitClick(values, setFieldError)
                          }
                          loading={buttonsLoading.saveAndQuit}
                          className={styles.button}
                        >
                          {t('controls.saveAndQuitButton')}
                        </Button>
                      </div>
                    )}
                  </div>
                )}

              {status !==
                PROJECT_INTERVIEW_PAGE_STATUS.VIEW_PROJECT_INTERVIEW && (
                <Controls
                  t={t}
                  buttonsLoading={buttonsLoading}
                  projects={projects}
                  status={status}
                  values={values}
                  setFieldError={setFieldError}
                  onSaveAndQuitClick={onSaveAndQuitClick}
                  onCancelCreateInterviewClick={onCancelCreateInterviewClick}
                />
              )}
              <GeneralComment
                t={t}
                status={status}
                errors={errors}
                initialValues={convertDataToForm({ status, interview, t })}
              />
              <QuestionsList
                t={t}
                values={values}
                errors={errors}
                status={status}
                interview={interview}
                setQuestions={setQuestions}
              />
            </Form>
          )}
        </Formik>
      </div>
    </MainLayout>
  );
};

export default React.memo(ProjectInterviewPage);
