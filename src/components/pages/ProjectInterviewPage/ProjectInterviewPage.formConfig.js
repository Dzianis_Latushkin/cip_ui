import * as Yup from 'yup';

export const getValidationSchema = (t) =>
  Yup.object().shape({
    title: Yup.string()
      .required(t('titleValidationError'))
      .test('isValid', t('titleSpacesError'), (value) => {
        if (value) {
          return value.replace(/\s/g, '').length;
        }

        return true;
      }),
  });

export const initialValues = (t) => {
  return {
    title: t('defaultTitle'),
    questions: [],
    project: { option: null, label: 'Select project' },
  };
};
