export const getTableColumns = (t) => [
  {
    key: 'title',
    minWidth: '230px',
    maxWidth: '230px',
    sortable: false,
  },
  {
    key: 'description',
    minWidth: '300px',
    maxWidth: '100%',
    sortable: false,
  },
  {
    key: 'link',
    minWidth: '50px',
    maxWidth: '50px',
    sortable: false,
  },
  {
    key: 'controls',
    minWidth: '50px',
    maxWidth: '50px',
    sortable: false,
  },
];
