export const convertDataToForm = (editData) => ({
  title: editData.title,
  description: editData.description,
  link: editData.link || '',
});
