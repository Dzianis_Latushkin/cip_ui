import React, { useState } from 'react';

import * as api from 'api/requests/requests';

import { showToast } from 'helpers/showToast';

import ProjectModal from './ProjectModal';
import { useTranslation } from 'react-i18next';

const ProjectModalContainer = ({
  t,
  setProjects,
  id,
  edit,
  editData,
  onClose,
  ...restProps
}) => {
  const [loading, setLoading] = useState(false);
  const [tNotifications] = useTranslation('notifications');

  const handleSubmit = (values, actions) => {
    setLoading(true);

    const { title, description, link } = values;

    if (edit) {
      api
        .editProject(id, {
          title: title,
          description: description,
          link: link,
        })
        .then((res) => {
          setProjects((prevState) => {
            return {
              ...prevState,
              data: {
                ...prevState.data,
                projects: prevState.data.projects.map((project) => {
                  if (project._id === res.data.project._id) {
                    return res.data.project;
                  }

                  return project;
                }),
              },
            };
          });
          setLoading(false);

          onClose();
          showToast(tNotifications('editProjectNotification'));
        })
        .catch((err) => {
          setLoading(false);
          actions.setFieldError('title', err);
        });
    } else {
      api
        .addProject({
          title: title.trim(),
          description: description.trim(),
          link: link.trim(),
        })
        .then((res) => {
          setLoading(false);

          setProjects((prevState) => {
            return {
              ...prevState,
              data: {
                ...prevState.data,
                projects: [res.data.project, ...prevState.data.projects],
              },
            };
          });

          onClose();
          showToast(tNotifications('addProjectNotification'));
        })
        .catch((err) => {
          setLoading(false);
          actions.setFieldError('title', err);
        });
    }
  };

  return (
    <ProjectModal
      t={t}
      loading={loading}
      edit={edit}
      editData={editData}
      onSubmit={handleSubmit}
      onClose={onClose}
      {...restProps}
    />
  );
};

export default React.memo(ProjectModalContainer);
