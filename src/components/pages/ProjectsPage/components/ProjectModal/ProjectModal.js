import React from 'react';
import { Formik, Form, Field } from 'formik';

import { convertDataToForm } from './ProjectModal.helpers';
import { getValidationSchema, initialValues } from './ProjectModal.formConfig';

import { Modal } from 'components/shared/Modal';
import { Button } from 'components/shared/Button';
import { Typography } from 'components/shared/Typography';

import { InputField } from 'components/FormikFields';

import styles from './ProjectModal.styles.scss';

const ProjectModal = ({
  t,
  loading,
  isOpen,
  edit,
  editData,
  onSubmit,
  onClose,
}) => (
  <Modal open={isOpen} centered className={styles.modal} onClose={onClose}>
    <Typography variant="h1" className={styles.title}>
      {t('projectModal.title')}
    </Typography>
    <Formik
      validateOnMount
      onSubmit={onSubmit}
      validationSchema={getValidationSchema(t)}
      initialValues={edit ? convertDataToForm(editData) : initialValues}
    >
      {({ values, errors }) => (
        <Form className={styles.form}>
          <Field
            label={t('projectModal.titleLabel')}
            name="title"
            errorId={'title-error'}
            placeholder={t('projectModal.titlePlaceholder')}
            component={InputField}
            inputClassName={styles.inputField}
          />
          <Field
            label={t('projectModal.descriptionLabel')}
            name="description"
            errorId={'description-error'}
            placeholder={t('projectModal.descriptionPlaceholder')}
            component={InputField}
            inputClassName={styles.descriptionField}
            multiline={true}
          />
          <Field
            label={t('projectModal.linkLabel')}
            name="link"
            errorId={'link-error'}
            placeholder={t('projectModal.linkPlaceholder')}
            component={InputField}
            inputClassName={styles.inputField}
          />
          <div className={styles.buttonBar}>
            <Button
              type="submit"
              variant="default"
              loading={loading}
              className={styles.button}
            >
              {t('projectModal.confirmButton')}
            </Button>
            <Button
              variant="outlined"
              className={styles.button}
              onClick={onClose}
            >
              {t('projectModal.cancelButton')}
            </Button>
          </div>
        </Form>
      )}
    </Formik>
  </Modal>
);

export default ProjectModal;
