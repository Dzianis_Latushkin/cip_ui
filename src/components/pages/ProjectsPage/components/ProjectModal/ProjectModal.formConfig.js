import * as Yup from 'yup';

export const getValidationSchema = (t) =>
  Yup.object().shape({
    title: Yup.string()
      .required(t('projectModal.requiredTitleValidationError'))
      .test('isValid', t('projectModal.titleSpacesError'), (value) => {
        if (value) {
          return value.replace(/\s/g, '').length;
        }

        return true;
      }),
    description: Yup.string(),
    link: Yup.string().url(t('projectModal.linkValidationError')),
  });

export const initialValues = {
  title: '',
  description: '',
  link: '',
};
