export const normalizeData = (projects) => {
  return projects.map((project) => ({
    id: project._id,
    title: project.title,
    link: project.link,
    description: project.description,
  }));
};
