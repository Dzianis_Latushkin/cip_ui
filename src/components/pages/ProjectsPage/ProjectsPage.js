import React from 'react';
import cn from 'classnames';

import { normalizeData } from './ProjectsPage.helpers';

import { getTableColumns } from './ProjectsPage.config';

import { Icon } from 'components/shared/Icon';
import { Table } from 'components/shared/Table';
import { Button } from 'components/shared/Button';
import { MainLayout } from 'components/Layout';
import { Typography } from 'components/shared/Typography';

import { Tooltip } from 'react-tippy';

import styles from './ProjectsPage.styles.scss';

const cellRenderer = (
  { item, key, minWidth, maxWidth, onSortClick },
  DefaultCell,
  { onLinkClick, onEditProjectClick, onRemoveProjectClick },
  t,
) => {
  switch (key) {
    case 'title': {
      return (
        <div className={styles.item} key={key} style={{ minWidth, maxWidth }}>
          <Typography title={item[key]} className={styles.itemText}>
            {item[key]}
          </Typography>
        </div>
      );
    }
    case 'description': {
      return (
        <div className={styles.item} key={key} style={{ minWidth, maxWidth }}>
          <Typography title={item[key]} className={styles.itemText}>
            {item[key]}
          </Typography>
        </div>
      );
    }
    case 'link': {
      return (
        <div className={styles.item} key={key} style={{ minWidth, maxWidth }}>
          <Tooltip
            html={
              <div className={styles.tooltip}>
                <Typography className={styles.tooltipText}>
                  {t('linkTooltip')}
                </Typography>
              </div>
            }
          >
            <Icon
              name="wod"
              className={cn(styles.icon, {
                [styles.disabledIcon]: !item[key],
              })}
              onClick={(e) => {
                e.stopPropagation();
                onLinkClick(item[key]);
              }}
            />
          </Tooltip>
        </div>
      );
    }
    case 'controls': {
      return (
        <div
          className={cn(styles.item, styles.iconItem)}
          key={key}
          style={{ minWidth, maxWidth }}
        >
          <Tooltip
            html={
              <div className={styles.tooltip}>
                <div
                  className={styles.tooltipItem}
                  onClick={() => onEditProjectClick(item.id)}
                >
                  <Icon name="pencil" className={styles.tooltipItemIcon} />
                  <Typography variant="body" className={styles.tooltipItemText}>
                    {t('editProjectTooltip')}
                  </Typography>
                </div>
                <div
                  className={styles.tooltipItem}
                  onClick={() => onRemoveProjectClick(item.id)}
                >
                  <Icon name="delete" className={styles.tooltipItemIcon} />
                  <Typography variant="body" className={styles.tooltipItemText}>
                    {t('removeProjectTooltip')}
                  </Typography>
                </div>
              </div>
            }
            position="bottom-end"
            trigger="click"
          >
            <Icon data-testid="moreIcon" name="more" />
          </Tooltip>
        </div>
      );
    }
    default: {
      return (
        <DefaultCell key={key} minWidth={minWidth} maxWidth={maxWidth}>
          {item[key] ? item[key] : '-'}
        </DefaultCell>
      );
    }
  }
};

const ProjectsPage = ({
  t,
  loading,
  projects,
  onLinkClick,
  onAddProjectClick,
  onEditProjectClick,
  onRemoveProjectClick,
}) => (
  <MainLayout loading={loading} pageTitle={t('title')}>
    <div className={styles.page}>
      <Button
        onClick={onAddProjectClick}
        className={styles.button}
        endIcon={<Icon name="plus" />}
      >
        {t('addButton')}
      </Button>
      <Table
        className={styles.table}
        columns={getTableColumns(t)}
        data={normalizeData(projects)}
        cellRenderer={(props, DefaultCell) =>
          cellRenderer(
            props,
            DefaultCell,
            { onLinkClick, onEditProjectClick, onRemoveProjectClick },
            t,
          )
        }
      />
    </div>
  </MainLayout>
);

export default React.memo(ProjectsPage);
