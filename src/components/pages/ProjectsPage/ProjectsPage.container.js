import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';

import * as api from 'api/requests/requests';

import { showToast } from 'helpers/showToast';

import { useModal } from 'hooks/useModal';
import { useFetch } from 'hooks/useFetch';

import { ProjectModal } from './components/ProjectModal';
import { ConfirmModal } from 'components/ConfirmModal';

import ProjectsPage from './ProjectsPage';

const ProjectsPageContainer = () => {
  const { t } = useTranslation('projectsPage');
  const [tNotifications] = useTranslation('notifications');

  const [confirmLoading, setConfrimLoading] = useState(false);

  const [
    isProjectModalOpen,
    openProjectModal,
    closeProjectModal,
    projectModalData,
  ] = useModal({});
  const [isConfirmModalOpen, openConfirmModal, closeConfirmModal, modalData] =
    useModal({});

  const {
    data: { projects },
    loading,
    setState: setProjects,
  } = useFetch(
    { defaultData: [], fetcher: api.getProjects, immediate: true },
    {},
  );

  const projectsArray = projects || [];

  const handleAddProjectClick = () => {
    openProjectModal();
  };

  const handleRemoveProjectClick = (id) => {
    openConfirmModal({
      titleText: t('confirmModalTitle'),
      contentText: t('confirmModalContent'),
      onConfirmClick: () => handleRemoveProject(id),
      onCancelClick: () => closeConfirmModal(),
    });
  };

  const handleRemoveProject = (id) => {
    setConfrimLoading(true);

    api
      .deleteProject(id)
      .then((res) => {
        setProjects((prevState) => {
          return {
            ...prevState,
            data: {
              ...prevState.data,
              projects: prevState.data.projects.filter(
                (project) => project._id !== id,
              ),
            },
          };
        });
        setConfrimLoading(false);
        closeConfirmModal();

        showToast(tNotifications('deleteProjectNotification'));
      })
      .catch(() => {
        setConfrimLoading(false);
      });
  };

  const handleEditProjectClick = (id) => {
    const editData = projects.find((project) => project._id === id);

    openProjectModal({
      edit: true,
      id,
      editData: editData,
    });
  };

  const handleLinkClick = (link) => {
    if (link) {
      window.open(link, '_blank');
    }
  };

  return (
    <>
      <ProjectsPage
        t={t}
        loading={loading}
        projects={projectsArray}
        onLinkClick={handleLinkClick}
        onAddProjectClick={handleAddProjectClick}
        onEditProjectClick={handleEditProjectClick}
        onRemoveProjectClick={handleRemoveProjectClick}
      />
      <ConfirmModal
        {...modalData}
        loading={confirmLoading}
        isOpen={isConfirmModalOpen}
        onClose={closeConfirmModal}
      />
      <ProjectModal
        {...projectModalData}
        t={t}
        isOpen={isProjectModalOpen}
        setProjects={setProjects}
        onClose={closeProjectModal}
      />
    </>
  );
};

export default React.memo(ProjectsPageContainer);
