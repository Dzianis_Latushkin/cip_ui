import React from 'react';
import { useTranslation } from 'react-i18next';

import { Typography } from 'components/shared/Typography';

import { MainLayout } from 'components/Layout';

import styles from './NotFoundPage.styles.scss';

const NotFoundPage = () => {
  const { t } = useTranslation('managersPage');

  return (
    <MainLayout>
      <div className={styles.page}>
        <div className={styles.pageErrorView}>404</div>
        <Typography variant="h1" component="h1" className={styles.title}>
          {t('title')}
        </Typography>
        <Typography variant="h3" component="h3" className={styles.title}>
          {t('description')}
        </Typography>
      </div>
    </MainLayout>
  );
};

export default React.memo(NotFoundPage);
