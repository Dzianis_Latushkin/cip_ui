import React from 'react';
import Linkify from 'react-linkify';

import { getResultRating } from '../FeedbackPage/FeedbackPage.helpers';

import { Icon } from 'components/shared/Icon';
import { Button } from 'components/shared/Button';
import { Typography } from 'components/shared/Typography';

import styles from './EmployeeInterviewsPage.styles.scss';

const EmployeeInterviewsPage = ({
  t,
  loading,
  feedbacks,
  onDetailedFeedbackClick,
}) => {
  if (feedbacks.length > 0) {
    return (
      <div className={styles.inner}>
        <div className={styles.interviewsContainer}>
          {feedbacks.map((feedback) => (
            <div key={feedback._id} className={styles.interviewCard}>
              <div className={styles.interviewCardHeader}>
                <div className={styles.interviewCardHeaderContent}>
                  <Typography
                    variant="h2"
                    className={styles.interviewTitle}
                    component="h2"
                  >
                    {feedback.interview?.title}
                  </Typography>
                  <div className={styles.interviewDate}>
                    <Icon
                      name="calendar"
                      className={styles.interviewIconDate}
                    />
                    <Typography>
                      {new Date(
                        Number(feedback.updatedAt),
                      ).toLocaleDateString()}
                    </Typography>
                  </div>
                  <Typography className={styles.feedbackByCreator}>
                    Created by {feedback.creator.name}
                  </Typography>
                </div>

                <div className={styles.interviewCardRate}>
                  <Typography variant="h3" className={styles.ratingTotal}>
                    {getResultRating(feedback.feedback)}
                  </Typography>
                </div>
              </div>
              <div className={styles.interviewCardComment}>
                <Linkify
                  componentDecorator={(decoratedHref, decoratedText, key) => (
                    <Typography
                      component="a"
                      variant="link"
                      target="blank"
                      href={decoratedHref}
                      key={key}
                    >
                      {' '}
                      {decoratedText}{' '}
                    </Typography>
                  )}
                >
                  <Typography variant="h3" className={styles.subtitle}>
                    {t('summaryLabel')}
                  </Typography>
                  <Typography className={styles.interviewComment}>
                    {feedback.generalComment}
                  </Typography>
                </Linkify>
              </div>
              <div className={styles.interviewCardFooter}>
                <Button onClick={() => onDetailedFeedbackClick(feedback._id)}>
                  {t('feedbackButton')}
                </Button>
              </div>
            </div>
          ))}
        </div>
      </div>
    );
  }

  return (
    <div className={styles.inner}>
      <div className={styles.noInterviews}>
        <Icon name="history" className={styles.noInterviewsIcon} />
        <Typography variant="h3" component="h3">
          {t('noInterviewText')}
        </Typography>
      </div>
    </div>
  );
};

export default React.memo(EmployeeInterviewsPage);
