import React from 'react';
import { useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { useNavigate } from 'react-router-dom';

import * as api from 'api/requests/requests';

import { userSelector } from 'reducers/selectors';

import { useFetch } from 'hooks/useFetch';

import { MainLayout } from 'components/Layout';

import EmployeeInterviewsPage from './EmployeeInterviewsPage';

const EmployeeInterviewsPageContainer = () => {
  const { t } = useTranslation('employeeInterviewsPage');
  const navigate = useNavigate();
  const user = useSelector(userSelector);

  const userId = user.currentUser._id;

  const {
    data: { feedbacks },
    loading,
  } = useFetch(
    {
      defaultData: { feedbacks: [] },
      fetcher: api.getFeedbackList,
      immediate: true,
      stopRequest: !userId,
    },
    userId,
  );

  const handleDetailedFeedbackClick = (feedbackId) => {
    navigate(`/result/${feedbackId}`);
  };

  return (
    <MainLayout loading={loading} pageTitle={t('pageTitle')}>
      <EmployeeInterviewsPage
        t={t}
        feedbacks={feedbacks}
        onDetailedFeedbackClick={handleDetailedFeedbackClick}
      />
    </MainLayout>
  );
};

export default React.memo(EmployeeInterviewsPageContainer);
