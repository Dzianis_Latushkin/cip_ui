import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useNavigate } from 'react-router-dom';

import * as api from 'api/requests/requests';

import { showToast } from 'helpers/showToast';

import { useModal } from 'hooks/useModal';
import { useFetch } from 'hooks/useFetch';

import { MainLayout } from 'components/Layout';
import { ConfirmModal } from 'components/ConfirmModal';
import { EmployeesModal } from 'components/EmployeesModal';
import { EmployeesCompareModal } from 'components/EmployeesCompareModal';

import { InterviewModal } from './components/InterviewModal';

import EmployeesPage from './EmployeesPage';

const EmployeesPageContainer = () => {
  const navigate = useNavigate();
  const { t } = useTranslation('employeesPage');
  const [tNotifications] = useTranslation('notifications');

  const [confirmLoading, setConfrimLoading] = useState(false);

  const [
    isEmployeesModalOpen,
    openEmployeesModal,
    closeEmployeesModal,
    employeeModalData,
  ] = useModal({});

  const [
    isEmployeesCompareModalOpen,
    openEmployeesCompareModal,
    closeEmployeesCompareModal,
    employeesCompareModalData,
  ] = useModal({});

  const [
    isInterviewModalOpen,
    openInterviewModal,
    closeInterviewModal,
    interviewModalData,
  ] = useModal({});

  const [isConfirmModalOpen, openConfirmModal, closeConfirmModal, modalData] =
    useModal({});

  const [searchQuery, setSearchQuery] = useState('');

  const {
    data: { users },
    loading,
    setState: setEmployees,
  } = useFetch(
    { defaultData: { users: [] }, fetcher: api.getUsers, immediate: true },
    {},
  );

  const handleAddEmployeeClick = () => {
    openEmployeesModal({ users });
  };

  const handleEditEmployeeClick = (id) => {
    const editData = users.find((user) => user._id === id);

    openEmployeesModal({
      edit: true,
      id,
      editData: editData,
    });
  };

  const handleRemoveEmployeeClick = (id) => {
    openConfirmModal({
      titleText: t('confirmModalTitle'),
      contentText: t('confirmModalContent'),
      onConfirmClick: () => handleRemoveEmployee(id),
      onCancelClick: () => closeConfirmModal(),
    });
  };

  const handleRemoveEmployee = (id) => {
    setConfrimLoading(true);

    api
      .removeEmployee(id)
      .then((res) => {
        setEmployees((prevState) => {
          return {
            ...prevState,
            data: {
              ...prevState.data,
              users: prevState.data.users.filter((user) => user._id !== id),
            },
          };
        });
        setConfrimLoading(false);
        closeConfirmModal();
        showToast(tNotifications('deleteEmployeeNotification'));
      })
      .catch(() => {
        setConfrimLoading(false);
      });
  };

  const handleInterviewHistoryClick = (userId) => {
    openInterviewModal({ userId });
  };

  const handleSearchChange = (e) => {
    setSearchQuery(e.target.value);
  };

  const filterUsers = users.filter(
    (user) =>
      `${user.name} ${user.surname}`
        .toLowerCase()
        .includes(searchQuery.toLowerCase()) ||
      user.email?.toLowerCase().includes(searchQuery.toLowerCase()),
  );

  const handleLinkClick = (link) => {
    if (link) {
      window.open(link, '_blank');
    }
  };

  return (
    <MainLayout loading={loading}>
      <EmployeesPage
        t={t}
        filterUsers={filterUsers}
        navigate={navigate}
        onLinkClick={handleLinkClick}
        onAddEmployeeClick={handleAddEmployeeClick}
        onEditEmploeeClick={handleEditEmployeeClick}
        onInterviewHistoryClick={handleInterviewHistoryClick}
        handleSearchChange={handleSearchChange}
        onRemoveEmployeeClick={handleRemoveEmployeeClick}
      />
      <EmployeesModal
        {...employeeModalData}
        isOpen={isEmployeesModalOpen}
        t={t}
        setEmployees={setEmployees}
        onClose={closeEmployeesModal}
        openEmployeesCompareModal={openEmployeesCompareModal}
      />
      <EmployeesCompareModal
        {...employeesCompareModalData}
        isOpen={isEmployeesCompareModalOpen}
        setEmployees={setEmployees}
        onClose={closeEmployeesCompareModal}
      />
      <InterviewModal
        {...interviewModalData}
        isOpen={isInterviewModalOpen}
        t={t}
        onClose={closeInterviewModal}
      />
      <ConfirmModal
        {...modalData}
        loading={confirmLoading}
        isOpen={isConfirmModalOpen}
        onClose={closeConfirmModal}
      />
    </MainLayout>
  );
};

export default React.memo(EmployeesPageContainer);
