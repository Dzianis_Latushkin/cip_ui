import React from 'react';
import cn from 'classnames';

import { Icon } from 'components/shared/Icon';
import { Typography } from 'components/shared/Typography';

import { Tooltip } from 'react-tippy';

import styles from './EmployeeBar.styles.scss';

const EmployeeBar = ({
  t,
  navigate,
  user,
  onShowHistoryClick,
  onEditEmploeeClick,
  onRemoveEmployeeClick,
  onLinkClick,
}) => {
  return (
    <div
      className={styles.container}
      onClick={onShowHistoryClick}
      data-testid="employeeBar"
    >
      <Typography className={styles.fullName} data-testid="employeeName">
        {user.name} {user.surname}
      </Typography>

      <Typography className={styles.englishLevel}>
        {user.engLevel || 'Unknown'}
      </Typography>

      <div className={styles.controls}>
        <Tooltip
          html={
            <div className={styles.tooltip}>
              <Typography className={styles.tooltipText}>
                {t('employeeBar.addInterviewTooltip')}
              </Typography>
            </div>
          }
          position="top-end"
          trigger="mouseenter"
          delay={[1000, 1]}
        >
          <Icon
            name="newInterview"
            className={styles.icon}
            onClick={(e) => {
              e.stopPropagation();
              navigate(`/interview/create?userId=${user._id}`);
            }}
          />
        </Tooltip>
        <Tooltip
          html={
            <div className={styles.tooltip}>
              <Typography className={styles.tooltipText}>
                {t('employeeBar.smgTooltip')}
              </Typography>
            </div>
          }
          position="top-end"
          trigger="mouseenter"
          delay={[1000, 1]}
        >
          <Icon
            name="smg"
            className={cn(styles.smgIcon, {
              [styles.disabledIcon]: !user.SMGlink,
            })}
            onClick={(e) => {
              e.stopPropagation();
              onLinkClick(user.SMGlink);
            }}
          />
        </Tooltip>

        <Tooltip
          html={
            <div className={styles.tooltip}>
              <Typography className={styles.tooltipText}>
                {t('employeeBar.wodTooltip')}
              </Typography>
            </div>
          }
          position="top-end"
          trigger="mouseenter"
          delay={[1000, 1]}
        >
          <Icon
            name="wod"
            className={cn(styles.icon, {
              [styles.disabledIcon]: !user.WODlink,
            })}
            onClick={(e) => {
              e.stopPropagation();
              onLinkClick(user.WODlink);
            }}
          />
        </Tooltip>

        <Tooltip
          html={
            <div className={styles.tooltip}>
              <Typography className={styles.tooltipText}>
                {t('employeeBar.editTooltip')}
              </Typography>
            </div>
          }
          position="top-end"
          trigger="mouseenter"
          delay={[1000, 1]}
        >
          <Icon
            name="pencil"
            className={styles.icon}
            onClick={(e) => {
              e.stopPropagation();
              onEditEmploeeClick(user._id);
            }}
          />
        </Tooltip>

        <Tooltip
          html={
            <div className={styles.tooltip}>
              <Typography className={styles.tooltipText}>
                {t('employeeBar.deleteTooltip')}
              </Typography>
            </div>
          }
          position="top-end"
          trigger="mouseenter"
          delay={[1000, 1]}
        >
          <Icon
            name="delete"
            className={cn(styles.icon, styles.removeIcon)}
            onClick={(e) => {
              e.stopPropagation();
              onRemoveEmployeeClick(user._id);
            }}
            data-testid="deleteEmployee"
          />
        </Tooltip>
      </div>
    </div>
  );
};

export default EmployeeBar;
