import React from 'react';
import { useNavigate } from 'react-router-dom';

import * as api from 'api/requests/requests';

import { useFetch } from 'hooks/useFetch';

import InterviewModal from './InterviewModal';

const InterviewModalContainer = ({ t, userId, onClose, ...restProps }) => {
  const navigate = useNavigate();

  const {
    data: { feedbacks },
    loading,
  } = useFetch(
    {
      defaultData: [],
      fetcher: api.getFeedbackList,
      immediate: true,
      stopRequest: !userId,
    },
    userId,
  );

  const handleDetailedFeedbackClick = (feedbackId) => {
    navigate(`/feedback/${feedbackId}`);
  };

  return (
    <InterviewModal
      t={t}
      loading={loading}
      feedbacks={feedbacks}
      onClose={onClose}
      onDetailedFeedbackClick={handleDetailedFeedbackClick}
      {...restProps}
    />
  );
};

export default React.memo(InterviewModalContainer);
