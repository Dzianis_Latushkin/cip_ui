import React from 'react';
import Linkify from 'react-linkify';

import { getResultRating } from './InterviewModal.helpers';

import { Icon } from 'components/shared/Icon';
import { Modal } from 'components/shared/Modal';
import { Button } from 'components/shared/Button';
import { Loading } from 'components/shared/Loading';
import { Typography } from 'components/shared/Typography';
import { ScrollWrapper } from 'components/shared/ScrollWrapper';

import styles from './InterviewModal.styles.scss';

const InterviewModal = ({
  t,
  loading,
  feedbacks,
  onDetailedFeedbackClick,
  isOpen,
  onClose,
}) => (
  <Modal open={isOpen} centered className={styles.modal} onClose={onClose}>
    <Typography variant="h1" className={styles.title}>
      {t('interviewModal.interviewHistory')}
    </Typography>
    {loading ? (
      <Loading variant="primary" size={30} className={styles.loading} />
    ) : (
      <ScrollWrapper
        translateContentSizeYToHolder
        thumbClassName={styles.thumb}
        className={styles.ScrollWrapper}
      >
        <div className={styles.inner}>
          {feedbacks && feedbacks.length > 0 ? (
            feedbacks.map((feedback) => (
              <div key={feedback._id} className={styles.feedback}>
                <div className={styles.feedbackTime}>
                  <Icon name="calendar" />
                  <Typography className={styles.date}>
                    {new Date(Number(feedback.updatedAt)).toLocaleDateString()}
                  </Typography>
                </div>
                <div className={styles.feedbackContent}>
                  <div className={styles.feedbackHeader}>
                    <Typography variant="h3" className={styles.feedbackTitle}>
                      {feedback.interview?.title}
                    </Typography>
                    <Typography variant="h3" className={styles.ratingTotal}>
                      {getResultRating(feedback.feedback)}
                    </Typography>
                  </div>
                  <Linkify
                    componentDecorator={(decoratedHref, decoratedText, key) => (
                      <Typography
                        component="a"
                        variant="link"
                        target="blank"
                        href={decoratedHref}
                        key={key}
                      >
                        {' '}
                        {decoratedText}{' '}
                      </Typography>
                    )}
                  >
                    <Typography className={styles.feedbackComment}>
                      {feedback.generalComment}
                    </Typography>
                  </Linkify>

                  <div className={styles.feedbackFooter}>
                    <Typography
                      variant="link"
                      component="a"
                      className={styles.feedbackLink}
                      onClick={() => onDetailedFeedbackClick(feedback._id)}
                    >
                      {t('interviewModal.seeFeedbackButton')}
                    </Typography>
                  </div>
                </div>
              </div>
            ))
          ) : (
            <div className={styles.noFeedback}>
              <Icon name="history" className={styles.noFeedbackIcon} />
              <Typography variant="h3" component="h3">
                {t('interviewModal.emptyHistoryText')}
              </Typography>
            </div>
          )}
        </div>
      </ScrollWrapper>
    )}
    <div className={styles.buttonBar}>
      <Button variant="default" onClick={onClose} className={styles.button}>
        {t('interviewModal.closeButton')}
      </Button>
    </div>
  </Modal>
);

export default InterviewModal;
