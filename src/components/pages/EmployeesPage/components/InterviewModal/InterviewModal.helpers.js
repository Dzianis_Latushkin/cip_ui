export const getResultRating = (feedback) => {
  if (feedback) {
    const nextValue = feedback.reduce((acc, item) => acc + +item.grade, 0);

    return (nextValue / feedback.length).toFixed(1);
  }

  return null;
};
