import React from 'react';

import { Icon } from 'components/shared/Icon';
import { Input } from 'components/shared/Input';
import { Button } from 'components/shared/Button';
import { Typography } from 'components/shared/Typography';

import { EmployeeBar } from './components/EmployeeBar';

import styles from './EmployeesPage.styles.scss';

const EmployeesPage = ({
  t,
  filterUsers,
  navigate,
  onLinkClick,
  onAddEmployeeClick,
  onEditEmploeeClick,
  onInterviewHistoryClick,
  handleSearchChange,
  onRemoveEmployeeClick,
}) => (
  <div className={styles.page}>
    <div className={styles.headersPart}>
      <Typography className={styles.title} variant="h1" component="h1">
        {t('title')}
      </Typography>
      <div className={styles.buttonsBar}>
        <Button
          onClick={() => onAddEmployeeClick()}
          className={styles.button}
          endIcon={<Icon name="plus" />}
          data-testid="addEmployeeButton"
        >
          {t('addButton')}
        </Button>
        <Button className={styles.button} disabled>
          {t('wodSync')}
        </Button>
      </div>

      <Input
        onChange={handleSearchChange}
        className={styles.searchInput}
        placeholder={t('searchInputPlaceholder')}
        endAdornment={
          <Icon name="magnifier" className={styles.searchInputIcon} />
        }
      />
    </div>
    <div className={styles.employeesContainer}>
      {filterUsers.length ? (
        filterUsers.map((user) => {
          return (
            <EmployeeBar
              key={user._id}
              t={t}
              user={user}
              navigate={navigate}
              onShowHistoryClick={() => onInterviewHistoryClick(user._id)}
              onEditEmploeeClick={onEditEmploeeClick}
              onRemoveEmployeeClick={onRemoveEmployeeClick}
              onLinkClick={onLinkClick}
            />
          );
        })
      ) : (
        <div className={styles.infoMessage}>
          <div className={styles.infoMessageIconWrap}>
            <Icon name="info" className={styles.infoMessageIcon} />
          </div>
          <Typography
            variant="h3"
            component="h3"
            className={styles.infoMessageText}
          >
            {t('emptySearchResult')}
          </Typography>
        </div>
      )}
    </div>
  </div>
);

export default React.memo(EmployeesPage);
