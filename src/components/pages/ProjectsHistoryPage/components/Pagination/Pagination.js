import React from 'react';
import cn from 'classnames';
import ReactPaginate from 'react-paginate';

import { Icon } from 'components/shared/Icon';

import styles from './Pagination.styles.scss';

const Pagination = ({
  totalItems = 1,
  itemsPerPage,
  handlePageClick,
  forcePage,
}) => {
  const pageCount = Math.ceil(totalItems / itemsPerPage);

  return pageCount > 1 ? (
    <>
      <ReactPaginate
        previousLabel={
          <Icon
            name="chevronUp"
            className={cn(styles.chevronIcon, styles.chevronIconPrevious)}
          />
        }
        nextLabel={
          <Icon
            name="chevronUp"
            className={cn(styles.chevronIcon, styles.chevronIconNext)}
          />
        }
        breakLabel="..."
        onPageChange={handlePageClick}
        pageCount={pageCount}
        forcePage={forcePage}
        renderOnZeroPageCount={null}
        containerClassName={cn(styles.paginationContainer)}
        pageClassName={cn(styles.page)}
        activeClassName={cn(styles.activePage)}
        pageLinkClassName={cn(styles.link)}
        breakClassName={cn(styles.break)}
        breakLinkClassName={cn(styles.link)}
        previousClassName={cn(styles.page)}
        previousLinkClassName={cn(styles.link)}
        nextClassName={cn(styles.page)}
        nextLinkClassName={cn(styles.link)}
        disabledClassName={cn(styles.disabled)}
      />
    </>
  ) : null;
};

export default React.memo(Pagination);
