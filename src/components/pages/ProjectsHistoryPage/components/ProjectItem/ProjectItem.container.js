import React, { useEffect, useState } from 'react';

import * as api from 'api/requests/requests';

import ProjectItem from './ProjectItem';

const ProjectItemContainer = ({ t, project, navigate }) => {
  const [projectsInterviews, setProjectsInterviews] = useState([]);
  const [totalInterviewsCount, setTotalInterviewsCount] = useState([]);
  const [currentPage, setCurrentPage] = useState(1);

  const [interviewsStatuses, setInterviewsStatuses] = useState({
    isPending: true,
    isFailed: true,
    isSuccessful: true,
    isDismissed: true,
  });

  //TODO Make refactor using useFetch

  const defaultStatuses =
    '&status=pending&status=failed&status=successful&status=dismissed';

  const handleShowProjectsInterviews = () => {
    setInterviewsStatuses({
      isPending: true,
      isFailed: true,
      isSuccessful: true,
      isDismissed: true,
    });
    api
      .getAllProjectInterviews(project._id, currentPage, defaultStatuses)
      .then((res) => {
        setProjectsInterviews(res.data.projectInterviews);
        setTotalInterviewsCount(res.data.totalInterviewCount);
        setCurrentPage(1);
      })
      .catch(() => {});
  };

  const handlePageClick = (event) => {
    setCurrentPage(event.selected + 1);
    let statuses = '';

    if (
      !interviewsStatuses.isPending &&
      !interviewsStatuses.isFailed &&
      !interviewsStatuses.isSuccessful &&
      !interviewsStatuses.isDismissed
    ) {
      setProjectsInterviews([]);
      setTotalInterviewsCount(0);

      return;
    } else {
      statuses = `${interviewsStatuses.isPending ? '&status=pending' : ''}${
        interviewsStatuses.isFailed ? '&status=failed' : ''
      }${interviewsStatuses.isSuccessful ? '&status=successful' : ''}${
        interviewsStatuses.isDismissed ? '&status=dismissed' : ''
      }`;
    }
    api
      .getAllProjectInterviews(project._id, event.selected + 1, statuses)
      .then((res) => {
        setProjectsInterviews(res.data.projectInterviews);
        setTotalInterviewsCount(res.data.totalInterviewCount);
      })
      .catch(() => {});
  };

  useEffect(() => {
    setCurrentPage(1);
    let st = '';

    if (
      !interviewsStatuses.isPending &&
      !interviewsStatuses.isFailed &&
      !interviewsStatuses.isSuccessful &&
      !interviewsStatuses.isDismissed
    ) {
      setProjectsInterviews([]);
      setTotalInterviewsCount(0);

      return;
    } else {
      st = `${interviewsStatuses.isPending ? '&status=pending' : ''}${
        interviewsStatuses.isFailed ? '&status=failed' : ''
      }${interviewsStatuses.isSuccessful ? '&status=successful' : ''}${
        interviewsStatuses.isDismissed ? '&status=dismissed' : ''
      }`;
    }

    api
      .getAllProjectInterviews(project._id, 1, st)
      .then((res) => {
        setProjectsInterviews(res.data.projectInterviews);
        setTotalInterviewsCount(res.data.totalInterviewCount);
      })
      .catch(() => {});
  }, [interviewsStatuses]);

  const handleFilterOptionsChange = (status) => {
    setInterviewsStatuses({
      ...interviewsStatuses,
      [status]: !interviewsStatuses[status],
    });
  };

  return (
    <ProjectItem
      t={t}
      project={project}
      navigate={navigate}
      projectsInterviews={projectsInterviews}
      totalInterviewsCount={totalInterviewsCount}
      onChange={handleFilterOptionsChange}
      handleShowProjectsInterviews={handleShowProjectsInterviews}
      forcePage={currentPage - 1}
      handlePageClick={handlePageClick}
      interviewsStatuses={interviewsStatuses}
    />
  );
};

export default React.memo(ProjectItemContainer);
