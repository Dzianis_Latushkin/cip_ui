import React from 'react';
import Collapsible from 'react-collapsible';
import cn from 'classnames';

import { Icon } from 'components/shared/Icon';
import { Typography } from 'components/shared/Typography';
import { Checkbox } from 'components/shared/Checkbox';
import { Pagination } from '../Pagination';
import { ProjectInterviewsItem } from '../ProjectInterviewsItem';

import styles from './ProjectItem.styles.scss';

const ProjectItem = ({
  t,
  project,
  navigate,
  projectsInterviews,
  handleShowProjectsInterviews,
  forcePage,
  handlePageClick,
  onChange,
  interviewsStatuses,
  totalInterviewsCount,
}) => {
  return (
    <Collapsible
      className={cn(styles.projectListItem)}
      openedClassName={cn(styles.openedProjectListItem)}
      trigger={
        <div className={styles.trigger}>
          <div className={styles.projectNameContainer}>
            <Icon name={'suitcase'} className={styles.projectIcon} />
            <Typography variant="subtitle" className={styles.projectName}>
              {project.title}
            </Typography>
          </div>
          <Icon name="chevronDown" className={styles.chevronIcon} />
        </div>
      }
      triggerWhenOpen={
        <div className={styles.trigger}>
          <div className={styles.projectNameContainer}>
            <Icon name={'suitcase'} className={styles.projectIcon} />
            <Typography variant="subtitle" className={styles.projectName}>
              {project.title}
            </Typography>
          </div>
          <Icon name="chevronUp" className={styles.chevronIcon} />
        </div>
      }
      onTriggerOpening={handleShowProjectsInterviews}
    >
      <div className={styles.projectInterviewsContainer}>
        <div className={styles.filtersContainer}>
          <fieldset>
            <legend>Interview status</legend>
            <Checkbox
              id={project._id + 'isPending'}
              classNameCheckboxField={styles.filterCheckbox}
              checked={interviewsStatuses.isPending}
              label={'Pending'}
              onChange={() => onChange('isPending')}
            />
            <Checkbox
              id={project._id + 'isFailed'}
              classNameCheckboxField={styles.filterCheckbox}
              checked={interviewsStatuses.isFailed}
              label={'Failed'}
              onChange={() => onChange('isFailed')}
            />
            <Checkbox
              id={project._id + 'isSuccessful'}
              classNameCheckboxField={styles.filterCheckbox}
              checked={interviewsStatuses.isSuccessful}
              label={'Successful'}
              onChange={() => onChange('isSuccessful')}
            />
            <Checkbox
              id={project._id + 'isDismissed'}
              classNameCheckboxField={styles.filterCheckbox}
              checked={interviewsStatuses.isDismissed}
              label={'Dismissed'}
              onChange={() => onChange('isDismissed')}
            />
          </fieldset>
        </div>

        {projectsInterviews && projectsInterviews.length > 0 ? (
          projectsInterviews.map((interview) => (
            <ProjectInterviewsItem
              interview={interview}
              key={interview._id}
              navigate={navigate}
            />
          ))
        ) : (
          <Typography>{t('noProjectInterviews')}</Typography>
        )}
        <Pagination
          totalItems={totalInterviewsCount}
          handlePageClick={handlePageClick}
          itemsPerPage={20}
          forcePage={forcePage}
        />
      </div>
    </Collapsible>
  );
};

export default React.memo(ProjectItem);
