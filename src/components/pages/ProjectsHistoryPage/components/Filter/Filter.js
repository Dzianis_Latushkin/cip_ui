import React, { useCallback } from 'react';

import { Icon } from 'components/shared/Icon';
import { Input } from 'components/shared/Input';

import styles from './Filter.styles.scss';

const Filter = ({ handleFilter, t }) => {
  const onChange = useCallback(
    (e) => {
      handleFilter(e.target.value);
    },
    [handleFilter],
  );

  return (
    <div className={styles.filterContainer}>
      <Input
        onChange={onChange}
        className={styles.searchInput}
        placeholder={t('searchInputPlaceholder')}
        endAdornment={
          <Icon name="magnifier" className={styles.searchInputIcon} />
        }
      />
    </div>
  );
};

export default React.memo(Filter);
