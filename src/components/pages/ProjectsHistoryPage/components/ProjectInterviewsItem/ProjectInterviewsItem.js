import React from 'react';
import cn from 'classnames';

import { Tag } from 'components/shared/Tag';
import { Icon } from 'components/shared/Icon';
import { Typography } from 'components/shared/Typography';

import styles from './ProjectInterviewsItem.styles.scss';
import { Link } from 'react-router-dom';

const renderStatusTag = (status) => {
  switch (status) {
    case 'pending':
      return <Tag variant="info">Pending</Tag>;
    case 'failed':
      return <Tag variant="error">Failed</Tag>;
    case 'successful':
      return <Tag variant="success">Successful</Tag>;
    case 'dismissed':
      return <Tag variant="default">Dismissed</Tag>;
  }
};

const ProjectInterviewsItem = ({ interview }) => (
  <Link
    className={cn(styles.projectInterview)}
    to={`/project-interview/${interview._id}/view?fromPH=true`}
    target="_blank"
  >
    <div className={styles.projectInterviewContainer}>
      <div className={styles.projectInterviewTitle}>
        <Icon name="template" className={styles.projectInterviewIcon} />
        <Typography variant="body" className={styles.projectInterviewContent}>
          {interview.title}
        </Typography>
      </div>
      <div className={styles.projectInterviewInformation}>
        <div className={styles.projectInterviewCreator}>
          <Icon
            name="user"
            className={styles.projectInterviewInfoIcon}
            title="Created by "
          />
          <Typography
            variant="body"
            className={styles.projectInterviewInfoContent}
          >
            {interview.creator.name}
          </Typography>
        </div>

        <div className={styles.projectInterviewDate}>
          <Icon name="calendar" className={styles.projectInterviewInfoIcon} />
          <Typography
            variant="body"
            className={styles.projectInterviewInfoContent}
          >
            {new Date(Number(interview.createdAt)).toLocaleDateString()}
          </Typography>
        </div>

        <div className={styles.projectInterviewStatus}>
          {renderStatusTag(interview.status)}
        </div>
      </div>
    </div>
  </Link>
);

export default React.memo(ProjectInterviewsItem);
