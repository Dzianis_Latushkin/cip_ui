import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';

import * as api from 'api/requests/requests';

import { useFetch } from 'hooks/useFetch';

import { MainLayout } from 'components/Layout';

import HistoryPage from './ProjectsHistoryPage';

const ProjectsHistoryPageContainer = () => {
  const { t } = useTranslation('projectsHistoryPage');

  const [filterValue, setFilterValue] = useState('');

  const {
    data: { projects, totalProjectsCount },
    loadingProj,
    setState: setProjects,
  } = useFetch(
    { defaultData: [], fetcher: api.getProjectsHistory, immediate: true },
    { page: 1, searchValue: filterValue },
    [filterValue],
  );

  const handlePageClick = (event) => {
    api.getProjectsHistory(event.selected + 1).then((res) => {
      setProjects({
        data: {
          projects: res.data.projects,
          totalProjectsCount: res.data.totalProjectsCount,
        },
      });
    });
  };

  return (
    <MainLayout loading={loadingProj} pageTitle={t('pageTitle')}>
      <HistoryPage
        t={t}
        projects={projects}
        handlePageClick={handlePageClick}
        totalProjectsCount={totalProjectsCount}
        handleFilter={setFilterValue}
      />
    </MainLayout>
  );
};

export default React.memo(ProjectsHistoryPageContainer);
