import React from 'react';

import { Pagination } from './components/Pagination';
import { ProjectItem } from './components/ProjectItem';
import { Filter } from './components/Filter';

import styles from './ProjectsHistoryPage.styles.scss';
import { useNavigate } from 'react-router-dom';

const ProjectsHistoryPage = ({
  t,
  projects,
  handlePageClick,
  totalProjectsCount,
  handleFilter,
}) => {
  const navigate = useNavigate();

  return (
    <div className={styles.page}>
      <Filter handleFilter={handleFilter} t={t} />
      {projects?.length > 0 && (
        <>
          <div className={styles.projectsContainer}>
            {projects.map((prj) => {
              return (
                <ProjectItem
                  t={t}
                  project={prj}
                  key={prj._id}
                  navigate={navigate}
                />
              );
            })}
          </div>
          <Pagination
            totalItems={totalProjectsCount}
            handlePageClick={handlePageClick}
            itemsPerPage={100}
          />
        </>
      )}
    </div>
  );
};

export default React.memo(ProjectsHistoryPage);
