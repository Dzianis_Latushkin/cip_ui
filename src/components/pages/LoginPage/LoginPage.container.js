import React, { useState } from 'react';
import { useDispatch } from 'react-redux';

import * as api from 'api/requests/requests';

import { setUser } from 'reducers/userReducer';

import LoginPage from './LoginPage';

const LoginPageContainer = () => {
  const dispatch = useDispatch();

  const [loading, setLoading] = useState(false);
  const [showPassword, setShowPassword] = useState(false);

  const handleSubmit = (values, actions) => {
    setLoading(true);

    api
      .loginUser({ password: values.password, email: values.login })
      .then((res) => {
        setLoading(false);
        setUser(dispatch, res.data);
      })
      .catch((error) => {
        setLoading(false);
        actions.setFieldError('form', error);
      });
  };

  const handleEyeClick = () => {
    setShowPassword(!showPassword);
  };

  return (
    <LoginPage
      loading={loading}
      showPassword={showPassword}
      onSubmit={handleSubmit}
      onEyeClick={handleEyeClick}
    />
  );
};

export default React.memo(LoginPageContainer);
