import * as Yup from 'yup';

export const getValidationSchema = (t) => {
  return Yup.object().shape({
    login: Yup.string().required(t('loginValidationError')),
    password: Yup.string().required(t('passwordValidationError')),
  });
};

export const initialValues = {
  login: '',
  password: '',
};
