import React from 'react';
import { Formik, Form, Field } from 'formik';
import { useTranslation } from 'react-i18next';

import { getValidationSchema, initialValues } from './LoginPage.formConfig';

import { Icon } from 'components/shared/Icon';
import { Button } from 'components/shared/Button';
import { ErrorMessage } from 'components/shared/ErrorMessage';
import { Typography } from 'components/shared/Typography';

import { InputField } from 'components/FormikFields';

import styles from './LoginPage.styles.scss';

const LoginPage = ({ loading, showPassword, onSubmit, onEyeClick }) => {
  const { t } = useTranslation('loginPage');

  return (
    <div className={styles.page}>
      <div className={styles.inner}>
        <div className={styles.preview}>
          <Icon name="logo" className={styles.logo} />
          <Typography variant="subtitle" className={styles.logoText}>
            {t('description')}
          </Typography>
          <Typography className={styles.logoTitle}>{t('titleTop')}</Typography>
          <Typography className={styles.logoTitle}>
            {t('titleMiddle')}
          </Typography>
          <Typography className={styles.logoTitle}>
            {t('titleBottom')}
          </Typography>
        </div>
        <Formik
          validationSchema={getValidationSchema(t)}
          validateOnMount
          initialValues={initialValues}
          onSubmit={onSubmit}
        >
          {({ values, errors }) => (
            <Form className={styles.form}>
              <Typography variant="h2" className={styles.title}>
                {t('loginFormTitle')}
              </Typography>
              <Field
                label={t('loginLabel')}
                name="login"
                placeholder={t('loginPlaceholder')}
                component={InputField}
              />
              <Field
                label={t('passwordLabel')}
                type={showPassword ? 'text' : 'password'}
                name="password"
                placeholder={t('passwordPlaceholder')}
                autoComplete="username"
                component={InputField}
                endAdornment={
                  <Icon
                    name={showPassword ? 'showEye' : 'hideEye'}
                    className={styles.eyeIcon}
                    onClick={onEyeClick}
                  />
                }
              />
              <div className={styles.errorContainer}>
                {errors.form && (
                  <ErrorMessage
                    className={styles.errorMessage}
                    errorId="loginError"
                  >
                    {errors.form}
                  </ErrorMessage>
                )}
              </div>
              <Button
                type="submit"
                variant="alert"
                loading={loading}
                className={styles.button}
              >
                {t('loginButton')}
              </Button>
            </Form>
          )}
        </Formik>
      </div>
      <div className={styles.footer}>
        <Typography variant="subtitle" className={styles.text}>
          {t('copyright')}
        </Typography>
      </div>
    </div>
  );
};

export default React.memo(LoginPage);
