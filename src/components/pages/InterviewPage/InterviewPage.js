import React from 'react';
import cn from 'classnames';
import { Formik, Form, Field } from 'formik';

import { convertDataToForm, onPreventKeyDown } from './InterviewPage.helpers';
import { getValidationSchema } from './InterviewPage.formConfig';

import { timeEstimation } from 'helpers/timeEstimation';

import { Icon } from 'components/shared/Icon';
import { Button } from 'components/shared/Button';
import { Question } from 'components/Question';
import { SortableList } from 'components/SortableList';
import { Typography } from 'components/shared/Typography';
import { ErrorMessage } from 'components/shared/ErrorMessage';

import { InputField, SelectField } from 'components/FormikFields';

import styles from './InterviewPage.styles.scss';

const InterviewPage = ({
  t,
  buttonsLoading,
  status,
  userId,
  interviewToEdit,
  template,
  templates,
  myUsers,
  selectedQuestions,
  setQuestions,
  onSortEnd,
  onSaveAsTemplateClick,
  onAddQuestionFromListClick,
  onAddOwnQuestionClick,
  onAddEmployeeClick,
  onCancelCreateIterviewClick,
  onSaveAndContinueClick,
  onSaveAndQuitClick,
  onEditQuestionClick,
  onDeleteQuestionClick,
}) => (
  <div className={styles.page}>
    <Formik
      validationSchema={getValidationSchema(t)}
      validateOnMount
      initialValues={convertDataToForm({
        t,
        status,
        userId,
        interviewToEdit,
        template,
        myUsers,
      })}
    >
      {({
        values,
        errors,
        setFieldError,
        submitForm,
        validateForm,
        setSubmitting,
      }) => (
        <Form>
          <div className={styles.titleContainer}>
            <div className={styles.title}>
              <span className={styles.hiddenValue}>
                {values.title ? values.title : t('defaultTitle')}
              </span>
              <Field
                name="title"
                placeholder={t('defaultTitle')}
                component={InputField}
                formControlProps={{ className: styles.formControl }}
                className={
                  errors.title
                    ? cn(styles.errorInput, styles.titleEditInput)
                    : styles.titleEditInput
                }
                onClick={(event) => event.target.select()}
                onKeyDown={onPreventKeyDown}
                endAdornment={
                  <Icon name="pencil" className={styles.titleEditIcon} />
                }
                endAdornmentClassName={styles.endAdornment}
              />
            </div>
            {errors.title && (
              <ErrorMessage className={styles.errorMessage}>
                {errors.title}
              </ErrorMessage>
            )}
          </div>
          <div className={styles.assigneeTopButtonsContainer}>
            {myUsers.length > 0 ? (
              <div className={styles.assigneeContainer}>
                <Typography variant="h4" className={styles.assigneeTitle}>
                  {t('assigneeLabel')}
                </Typography>
                <Field
                  name="employee"
                  placeholder={t('assigneePlaceholder')}
                  options={myUsers}
                  component={SelectField}
                  formClassName={styles.selectControl}
                  inputClassName={styles.selectInput}
                  className={styles.selectInput}
                />
              </div>
            ) : (
              <div className={styles.assigneeButtonContainer}>
                <Button
                  onClick={onAddEmployeeClick}
                  className={styles.button}
                  endIcon={<Icon name="plus" />}
                >
                  {t('addEmployeeButton')}
                </Button>
                {errors.employee && (
                  <ErrorMessage className={styles.errorMessage}>
                    {errors.employee}
                  </ErrorMessage>
                )}
              </div>
            )}

            <div className={styles.buttonTopContainer}>
              <Button
                className={styles.button}
                variant="outlined"
                onClick={() => {
                  onCancelCreateIterviewClick(values);
                }}
              >
                {t('cancelButton')}
              </Button>

              <>
                <Button
                  disabled={
                    buttonsLoading.saveAndQuit || buttonsLoading.saveAndContinue
                  }
                  onClick={() => {
                    onSaveAsTemplateClick(setFieldError);
                  }}
                  className={styles.button}
                >
                  {t('saveAsTemplateButton')}
                </Button>
                <Button
                  disabled={buttonsLoading.saveAndContinue}
                  onClick={() => onSaveAndQuitClick(values, setFieldError)}
                  loading={buttonsLoading.saveAndQuit}
                  className={styles.button}
                >
                  {t('saveAndQuitButton')}
                </Button>
                <Button
                  disabled={buttonsLoading.saveAndQuit}
                  onClick={() => onSaveAndContinueClick(values, setFieldError)}
                  loading={buttonsLoading.saveAndContinue}
                  className={styles.button}
                >
                  {t('saveAndContinueButton')}
                </Button>
              </>
            </div>
          </div>

          <div className={styles.buttonBottomContainer}>
            <Button
              onClick={() => {
                onAddQuestionFromListClick(setFieldError);
              }}
              className={styles.button}
              variant="outlined"
            >
              {t('addExistedQuestionButton')}
            </Button>
            <Button
              className={styles.button}
              onClick={onAddOwnQuestionClick}
              variant="outlined"
            >
              {t('addNewQuestionButton')}
            </Button>
          </div>

          {errors.questionsError && (
            <ErrorMessage>{errors.questionsError}</ErrorMessage>
          )}
          {selectedQuestions.length > 0 && (
            <div className={styles.questionContainer}>
              <div className={styles.timeBar}>
                <Typography className={styles.timeBarText}>
                  {'There are '}
                  {selectedQuestions.length}
                  {' question(s). It takes about '}
                  {timeEstimation(selectedQuestions)}
                </Typography>
              </div>
              <SortableList
                useDragHandle
                items={selectedQuestions}
                onSortEnd={onSortEnd}
                onEditQuestionClick={onEditQuestionClick}
                onDeleteQuestionClick={onDeleteQuestionClick}
                renderItem={({ item, ...restProps }) => {
                  return (
                    <Question
                      key={item._id}
                      id={item._id}
                      groupId={item.groupId}
                      question={item.question}
                      comment={item.comment}
                      {...restProps}
                      className={styles.interviewQuestion}
                    />
                  );
                }}
              />
            </div>
          )}
        </Form>
      )}
    </Formik>
  </div>
);

export default React.memo(InterviewPage);
