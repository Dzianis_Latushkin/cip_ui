import React from 'react';

import { groupRender } from './InterviewModal.helpers';

import { Modal } from 'components/shared/Modal';
import { Button } from 'components/shared/Button';
import { ScrollWrapper } from 'components/shared/ScrollWrapper';

import styles from './InterviewModal.styles.scss';

const InterviewModal = ({
  t,
  isInterviewModalOpen,
  groupData,
  setFieldError,
  onClose,
  onAddQuestionClick,
  onDeleteQuestionClick,
}) => {
  return (
    <Modal
      open={isInterviewModalOpen}
      className={styles.modal}
      onClose={onClose}
    >
      <div className={styles.modalTopContainer}>
        <Button onClick={onClose}>{t('interviewModal.finishButton')}</Button>
      </div>

      <div className={styles.questionsGroupContainer}>
        <ScrollWrapper
          translateContentSizeYToHolder
          trackClassName={styles.track}
          contentClassName={styles.content}
          className={styles.scroll}
        >
          {groupRender({
            groupData,
            setFieldError,
            onAddQuestionClick,
            onDeleteQuestionClick,
          })}
        </ScrollWrapper>
      </div>
    </Modal>
  );
};

export default InterviewModal;
