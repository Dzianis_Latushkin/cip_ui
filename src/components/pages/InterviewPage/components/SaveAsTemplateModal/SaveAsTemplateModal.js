import React from 'react';
import { Formik, Form, Field } from 'formik';

import {
  initialValues,
  getValidationSchema,
} from './SaveAsTemplateModal.formConfig';

import { Modal } from 'components/shared/Modal';
import { Button } from 'components/shared/Button';
import { Typography } from 'components/shared/Typography';

import { InputField } from 'components/FormikFields';

import styles from './SaveAsTemplateModal.styles.scss';

const InterviewTemplateModal = ({
  t,
  loading,
  isOpen,
  templates,
  selectedQuestions,
  onClose,
  saveTemplate,
}) => (
  <Modal open={isOpen} centered className={styles.modal} onClose={onClose}>
    <Typography variant="h1" className={styles.title}>
      {t('saveAsTemplateModal.title')}
    </Typography>
    <Formik
      validationSchema={getValidationSchema(t, templates)}
      validateOnMount
      initialValues={initialValues}
      onSubmit={(values, actions) => {
        saveTemplate(values.title, selectedQuestions);
      }}
    >
      {({ values, errors }) => (
        <Form className={styles.form}>
          <div className={styles.modalBody}>
            <Field
              label={t('saveAsTemplateModal.title')}
              name="title"
              placeholder={t('saveAsTemplateModal.titlePlaceholder')}
              component={InputField}
            />
          </div>
          <div className={styles.buttonBar}>
            <Button
              type="submit"
              variant="default"
              loading={loading}
              className={styles.button}
            >
              {t('saveAsTemplateModal.saveButton')}
            </Button>
            <Button
              variant="outlined"
              className={styles.button}
              onClick={onClose}
            >
              {t('saveAsTemplateModal.cancelButton')}
            </Button>
          </div>
        </Form>
      )}
    </Formik>
  </Modal>
);

export default InterviewTemplateModal;
