import React, { useState } from 'react';
import { toast } from 'react-toastify';

import * as api from 'api/requests/requests';

import { convertDataToApi } from './SaveAsTemplateModal.helpers';

import SaveAsTemplateModal from './SaveAsTemplateModal';

import { useTranslation } from 'react-i18next';

const SaveAsTemplateModalContainer = ({
  selectedQuestions,
  t,
  onClose,
  ...restProps
}) => {
  const [loading, setLoading] = useState(false);
  const [tNotifications] = useTranslation('notifications');

  const handleSaveAsTemplate = (title, questions) => {
    setLoading(true);

    const data = convertDataToApi(title, questions);
    api
      .createInterview(data)
      .then((res) => {
        setLoading(false);
        onClose();
      })
      .catch(() => {
        setLoading(false);
      });

    toast.info(tNotifications('saveTemplateNotification'), {
      position: toast.POSITION.BOTTOM_RIGHT,
      autoClose: 3000,
    });
  };

  return (
    <SaveAsTemplateModal
      t={t}
      loading={loading}
      selectedQuestions={selectedQuestions}
      onClose={onClose}
      saveTemplate={handleSaveAsTemplate}
      {...restProps}
    />
  );
};

export default React.memo(SaveAsTemplateModalContainer);
