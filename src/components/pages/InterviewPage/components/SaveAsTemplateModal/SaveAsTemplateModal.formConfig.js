import * as Yup from 'yup';

export const getValidationSchema = (t, templates) =>
  Yup.object().shape({
    title: Yup.string()
      .required(t('saveAsTemplateModal.nameRequireError'))
      .test(
        'isValid',
        t('saveAsTemplateModal.nameValidationError'),
        (value) => {
          if (value) {
            return value.replace(/\s/g, '').length;
          }

          return true;
        },
      )
      .test('isValid', t('templateTitleError'), (value) => {
        if (
          value &&
          templates.some(
            (item) => item.title.toLowerCase() === value.toLowerCase(),
          )
        ) {
          return false;
        }

        return true;
      }),
  });

export const initialValues = {
  title: '',
};
