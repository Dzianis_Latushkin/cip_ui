export const convertDataToApi = (title, selectedQuestions) => {
  return {
    title: title,
    status: 'template',
    employee: null,
    questions: selectedQuestions,
  };
};
