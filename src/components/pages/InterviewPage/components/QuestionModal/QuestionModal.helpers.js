export const convertDataToForm = (editQuestionData) => ({
  title: editQuestionData.question,
  comment: editQuestionData.comment,
});
