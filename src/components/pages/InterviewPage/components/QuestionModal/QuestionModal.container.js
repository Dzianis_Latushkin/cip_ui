import React from 'react';
import { v4 as uuid } from 'uuid';

import QuestionModal from './QuestionModal';

const QuestionModalContainer = ({
  t,
  isEditQuestion,
  questionId,
  editQuestionData,
  onClose,
  setSelectedQuestions,
  setQuestions,
  ...restProps
}) => {
  const handleSubmit = (values) => {
    const { title, comment } = values;

    if (questionId) {
      setSelectedQuestions((prevState) => {
        const nextQuestions = prevState.map((question) => {
          if (question._id === questionId) {
            const nextTitle = question.question;
            const nextComment = question.comment;

            if (nextTitle === title && nextComment === comment) {
              return question;
            } else {
              setQuestions((prevQuestionsState) => {
                return {
                  ...prevQuestionsState,
                  data: {
                    folders: prevQuestionsState.data.folders.map(
                      (nextFolder) => {
                        return {
                          ...nextFolder,
                          questions: nextFolder.questions.map(
                            (nextQuestion) => {
                              if (nextQuestion._id === questionId) {
                                return { ...nextQuestion, isSelected: false };
                              }

                              return nextQuestion;
                            },
                          ),
                        };
                      },
                    ),
                  },
                };
              });

              return { comment, question: title, _id: uuid() };
            }
          }

          return question;
        });

        return nextQuestions;
      });
    } else {
      setSelectedQuestions((prevState) => {
        return [...prevState, { comment, question: title, _id: uuid() }];
      });
    }

    onClose();
  };

  return (
    <QuestionModal
      t={t}
      isEditQuestion={isEditQuestion}
      editQuestionData={editQuestionData}
      onClose={onClose}
      onSubmit={handleSubmit}
      {...restProps}
    />
  );
};

export default React.memo(QuestionModalContainer);
