import { INTERVIEW_PAGE_STATUS } from 'constants/common';

import { initialValues } from './InterviewPage.formConfig';

export const convertDataToForm = ({
  t,
  status,
  userId,
  interviewToEdit,
  template,
  myUsers,
}) => {
  switch (status) {
    case INTERVIEW_PAGE_STATUS.CREATE_INTERVIEW: {
      const interviewUser = myUsers.find((user) => user.option === userId);

      if (template) {
        return {
          ...initialValues(t),
          title: template.title,
          employee: interviewUser || {
            option: null,
            label: t('assigneePlaceholder'),
          },
        };
      } else {
        return {
          ...initialValues(t),
          employee: interviewUser || {
            option: null,
            label: t('assigneePlaceholder'),
          },
        };
      }
    }
    case INTERVIEW_PAGE_STATUS.EDIT_INTERVIEW: {
      const interviewUser = myUsers.find(
        (user) => user.option === interviewToEdit.employee._id,
      );

      return {
        title: interviewToEdit ? interviewToEdit.title : '',
        employee: interviewUser || {},
      };
    }
    case INTERVIEW_PAGE_STATUS.VIEW_INTERVIEW: {
      //TODO Waiting for next epic
      break;
    }
    default: {
      return;
    }
  }
};

export const convertDataToApi = (values, selectedQuestions) => {
  return {
    title: values.title,
    status: 'planned',
    employee: values.employee.option,
    questions: selectedQuestions,
  };
};

export const convertDataToNewTemplateApi = (value, selectedQuestions) => {
  return {
    title: value.title,
    status: 'template',
    employee: null,
    questions: selectedQuestions,
    shared: value.shared,
  };
};

export const onPreventKeyDown = (keyEvent) => {
  if ((keyEvent.charCode || keyEvent.keyCode) === 13) {
    keyEvent.preventDefault();
    keyEvent.target.blur();
  }
};
