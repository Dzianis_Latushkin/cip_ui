import React, { useState, useEffect, useRef } from 'react';
import { useTranslation } from 'react-i18next';
import { useNavigate, useParams } from 'react-router-dom';

import * as api from 'api/requests/requests';

import { INTERVIEW_PAGE_STATUS } from 'constants/common';

import { convertDataToApi } from './InterviewPage.helpers';
import { showToast } from 'helpers/showToast';
import { normalizeGroupData } from 'helpers/normalizeGroupData';
import { moveStorableItem } from 'helpers/moveStorableItem';

import { useModal } from 'hooks/useModal';
import { useFetch } from 'hooks/useFetch';
import { useQuery } from 'hooks/useQuery';

import { MainLayout } from 'components/Layout';
import { ConfirmModal } from 'components/ConfirmModal';
import { EmployeesModal } from 'components/EmployeesModal';
import { EmployeesCompareModal } from 'components/EmployeesCompareModal';

import { QuestionModal } from './components/QuestionModal';
import { InterviewModal } from './components/InterviewModal';
import { SaveAsTemplateModal } from './components/SaveAsTemplateModal';

import InterviewPage from './InterviewPage';

const InterviewPageContainer = ({ status }) => {
  const selectRef = useRef();

  const { t } = useTranslation('interviewPage');
  const [tNotifications] = useTranslation('notifications');

  const navigate = useNavigate();

  const { id: interviewId } = useParams();

  const query = useQuery();
  const userId = query.get('userId');
  const templateId = query.get('templateId');

  const [createdInterviewId, setCreatedInterviewId] = useState('');

  const [isCreation, setIsCreation] = useState(true);

  const [selectedQuestions, setSelectedQuestions] = useState([]);
  const [buttonsLoading, setButtonsLoading] = useState({
    saveAndQuit: false,
    saveAndContinue: false,
  });

  const [lastSavedValues, setLastSavedValues] = useState({
    title: `${templateId ? t('defaultTemplateTitle') : t('defaultTitle')}`,
    employee: { option: null },
    questionsCount: 0,
  });

  const [
    isInterviewModalOpen,
    openInterviewModal,
    closeInterviewModal,
    interviewModalData,
  ] = useModal({});

  const [
    isEmployeesModalOpen,
    openEmployeesModal,
    closeEmployeesModal,
    employeeModalData,
  ] = useModal({});

  const [
    isQuestionModalOpen,
    openQuestionModal,
    closeQuestionModal,
    questionModalData,
  ] = useModal({});

  const [
    isSaveAsTemplateModalOpen,
    openSaveAsTemplateModal,
    closeSaveAsTemplateModal,
  ] = useModal({});

  const [
    isEmployeesCompareModalOpen,
    openEmployeesCompareModal,
    closeEmployeesCompareModal,
    employeesCompareModalData,
  ] = useModal({});

  const [isConfirmModalOpen, openConfirmModal, closeConfirmModal, modalData] =
    useModal({});

  const {
    data: { folders },
    loading: foldersLoading,
    setState: setQuestions,
  } = useFetch({
    defaultData: { folders: [] },
    fetcher: api.getAllFolders,
  });

  const {
    data: { interview: interviewToEdit },
    loading: interviewLoading,
  } = useFetch(
    {
      defaultData: {},
      fetcher: api.getOneInterview,
      stopRequest: status === INTERVIEW_PAGE_STATUS.CREATE_INTERVIEW,
    },
    { id: interviewId },
  );

  const {
    data: { interview: template },
    loading: templateLoading,
  } = useFetch(
    {
      defaultData: [],
      fetcher: api.getOneInterview,
      stopRequest: !templateId,
    },
    { id: templateId, status: '?status=template' },
  );

  const {
    data: { users },
    loading: employeesLoading,
    setState: setEmployees,
  } = useFetch({
    defaultData: { users: [] },
    fetcher: api.getUsers,
    onSuccess: ({ users: nextUsers }) => {
      setEmployees((prevState) => {
        return {
          ...prevState,
          data: {
            ...prevState.data,
            users: nextUsers.map((user) => ({
              label: user.name,
              option: user._id,
            })),
          },
        };
      });
    },
  });

  const {
    data: { interviews: templates },
    loading: templatesLoading,
  } = useFetch(
    {
      defaultData: { interviews: [] },
      fetcher: api.getManagersInterviews,
    },
    '?status=template',
  );

  useEffect(() => {
    if (
      interviewToEdit &&
      folders &&
      status === INTERVIEW_PAGE_STATUS.EDIT_INTERVIEW
    ) {
      setSelectedQuestions(interviewToEdit.questions);

      setLastSavedValues({
        title: interviewToEdit.title,
        employee: { option: interviewToEdit.employee._id },
        questionsCount: interviewToEdit.questions.length,
      });

      setQuestions((prevState) => {
        return {
          ...prevState,
          data: {
            folders: prevState.data.folders.map((nextFolder) => {
              return {
                ...nextFolder,
                questions: nextFolder.questions.map((nextQuestion) => {
                  if (
                    interviewToEdit.questions.some(
                      (question) =>
                        question.folderQuestionId === nextQuestion._id,
                    )
                  ) {
                    return { ...nextQuestion, isSelected: true };
                  }

                  return nextQuestion;
                }),
              };
            }),
          },
        };
      });
    }

    if (
      template &&
      folders &&
      status === INTERVIEW_PAGE_STATUS.CREATE_INTERVIEW
    ) {
      setSelectedQuestions(template.questions);

      setLastSavedValues({
        title: template.title,
        questionsCount: template.questions.length,
      });

      setQuestions((prevState) => {
        return {
          ...prevState,
          data: {
            folders: prevState.data.folders.map((nextFolder) => {
              return {
                ...nextFolder,
                questions: nextFolder.questions.map((nextQuestion) => {
                  if (
                    template.questions.some(
                      (question) =>
                        question.folderQuestionId === nextQuestion._id,
                    )
                  ) {
                    return { ...nextQuestion, isSelected: true };
                  }

                  return nextQuestion;
                }),
              };
            }),
          },
        };
      });
    }
  }, [interviewLoading, foldersLoading, templateLoading]);

  const handleAddOwnQuestionClick = () => {
    openQuestionModal();
  };

  const handleAddQuestionFromListClick = (setFieldError) => {
    openInterviewModal({ setFieldError });
  };

  const handleAddQuestionClick = (id, setFieldError) => {
    folders.forEach((folder) =>
      folder.questions.find((question) => {
        if (question._id === id) {
          setSelectedQuestions([
            ...selectedQuestions,
            { ...question, folderQuestionId: question._id },
          ]);
        }
      }),
    );

    setQuestions((prevState) => {
      return {
        ...prevState,
        data: {
          folders: prevState.data.folders.map((nextFolder) => {
            return {
              ...nextFolder,
              questions: nextFolder.questions.map((nextQuestion) => {
                if (nextQuestion._id === id) {
                  return { ...nextQuestion, isSelected: true };
                }

                return nextQuestion;
              }),
            };
          }),
        },
      };
    });

    setFieldError('questionsError', '');
  };

  const handleEditQuestionClick = (id) => {
    const editQuestionData = selectedQuestions.find(
      (question) => question._id === id,
    );

    openQuestionModal({
      isEditQuestion: true,
      questionId: id,
      editQuestionData,
    });
  };

  const handleDeleteQuestion = (id) => {
    const nextQuestions = selectedQuestions
      .filter((question) => question.folderQuestionId !== id)
      .filter((question) => question._id !== id);

    setSelectedQuestions(nextQuestions);

    setQuestions((prevState) => ({
      ...prevState,
      data: {
        folders: prevState.data.folders.map((nextFolder) => ({
          ...nextFolder,
          questions: nextFolder.questions.map((nextQuestion) => {
            if (nextQuestion._id === id) {
              return { ...nextQuestion, isSelected: false };
            }

            return nextQuestion;
          }),
        })),
      },
    }));

    closeConfirmModal();
  };

  const handleDeleteQuestionClick = (id, confirm) => {
    if (confirm) {
      openConfirmModal({
        titleText: t('confirmModalTitle'),
        contentText: t('confirmModalText'),
        onConfirmClick: () => handleDeleteQuestion(id),
        onCancelClick: () => closeConfirmModal(),
      });
    } else {
      handleDeleteQuestion(id);
    }
  };

  const handleSaveAndQuitClick = (values, setFieldError) => {
    setButtonsLoading({
      saveAndQuit: true,
      saveAndContinue: false,
    });

    if (!values.employee.option) {
      setButtonsLoading({
        saveAndQuit: false,
        saveAndContinue: false,
      });

      setFieldError('employee', t('employeeSelectionError'));

      return;
    }

    if (!selectedQuestions.length) {
      setButtonsLoading({
        saveAndQuit: false,
        saveAndContinue: false,
      });
      setFieldError('questionsError', t('questionsError'));

      return;
    }

    const data = convertDataToApi(values, selectedQuestions);

    if (isCreation && status === INTERVIEW_PAGE_STATUS.CREATE_INTERVIEW) {
      api
        .createInterview(data)
        .then((res) => {
          setButtonsLoading({
            saveAndQuit: false,
            saveAndContinue: false,
          });

          navigate('/interviews');
        })
        .catch(() => {
          setButtonsLoading({
            saveAndQuit: false,
            saveAndContinue: false,
          });
        });
    } else if (!isCreation || status === INTERVIEW_PAGE_STATUS.EDIT_INTERVIEW) {
      api
        .editInterview(createdInterviewId || interviewId, data)
        .then((res) => {
          setButtonsLoading({
            saveAndQuit: false,
            saveAndContinue: false,
          });

          navigate('/interviews');
        })
        .catch(() => {
          setButtonsLoading({
            saveAndQuit: false,
            saveAndContinue: false,
          });
        });
    }
    showToast(tNotifications('saveInterviewNotification'));

    setLastSavedValues({ ...values, questionsCount: selectedQuestions.length });
  };

  const handleSaveAndContinueClick = (values, setFieldError) => {
    setButtonsLoading({
      saveAndQuit: false,
      saveAndContinue: true,
    });

    if (!values.employee.option) {
      setButtonsLoading({
        saveAndQuit: false,
        saveAndContinue: false,
      });
      setFieldError('employee', t('employeeSelectionError'));

      return;
    }

    if (!selectedQuestions.length) {
      setButtonsLoading({
        saveAndQuit: false,
        saveAndContinue: false,
      });
      setFieldError('questionsError', t('questionsError'));

      return;
    }

    const data = convertDataToApi(values, selectedQuestions);

    if (isCreation && status === INTERVIEW_PAGE_STATUS.CREATE_INTERVIEW) {
      api
        .createInterview(data)
        .then((res) => {
          setCreatedInterviewId(res.data.interview._id);
          setIsCreation(false);
          setButtonsLoading({
            saveAndQuit: false,
            saveAndContinue: false,
          });
        })
        .catch(() => {
          setButtonsLoading({
            saveAndQuit: false,
            saveAndContinue: false,
          });
        });
    } else if (!isCreation || status === INTERVIEW_PAGE_STATUS.EDIT_INTERVIEW) {
      api
        .editInterview(createdInterviewId || interviewId, data)
        .then((res) => {
          setButtonsLoading({
            saveAndQuit: false,
            saveAndContinue: false,
          });
        })
        .catch(() => {
          setButtonsLoading({
            saveAndQuit: false,
            saveAndContinue: false,
          });
        });
    }

    showToast(tNotifications('saveInterviewNotification'));

    setLastSavedValues({ ...values, questionsCount: selectedQuestions.length });
  };

  const loading =
    foldersLoading ||
    employeesLoading ||
    interviewLoading ||
    templateLoading ||
    templatesLoading;

  const handleAddEmployeeClick = () => {
    if (selectRef.current) {
      selectRef.current.blur();
    }

    openEmployeesModal({ users: [] });
  };

  const handleSaveAsTemplateClick = (setFieldError) => {
    if (!selectedQuestions.length) {
      setFieldError('questionsError', t('templateQuestionsError'));

      return;
    }

    openSaveAsTemplateModal();
  };

  const handleCancelCreateInterviewClick = (values) => {
    if (
      selectedQuestions.length !== lastSavedValues.questionsCount ||
      values.title !== lastSavedValues.title ||
      values.employee?.option !== lastSavedValues.employee?.option
    ) {
      openConfirmModal({
        titleText: t('confirmCancelCreationTitle'),
        contentText: t('confirmCancelCreationText'),
        onConfirmClick: () => {
          showToast(tNotifications('cancelNotification'));

          navigate('/interviews');
        },
        onCancelClick: () => closeConfirmModal(),
      });
    } else {
      navigate('/interviews');
    }
  };

  const onSortEnd = ({ oldIndex, newIndex }) => {
    setSelectedQuestions((prevState) =>
      moveStorableItem(prevState, oldIndex, newIndex),
    );
  };

  return (
    <MainLayout loading={loading}>
      <InterviewPage
        t={t}
        buttonsLoading={buttonsLoading}
        status={status}
        userId={userId}
        interviewToEdit={interviewToEdit}
        template={template}
        templates={templates}
        myUsers={users}
        selectedQuestions={selectedQuestions}
        setQuestions={setQuestions}
        onSortEnd={onSortEnd}
        onSaveAsTemplateClick={handleSaveAsTemplateClick}
        onAddQuestionFromListClick={handleAddQuestionFromListClick}
        onAddOwnQuestionClick={handleAddOwnQuestionClick}
        onAddEmployeeClick={handleAddEmployeeClick}
        onCancelCreateIterviewClick={handleCancelCreateInterviewClick}
        onSaveAndContinueClick={handleSaveAndContinueClick}
        onSaveAndQuitClick={handleSaveAndQuitClick}
        onEditQuestionClick={handleEditQuestionClick}
        onDeleteQuestionClick={(params) =>
          handleDeleteQuestionClick(params, true)
        }
      />
      <InterviewModal
        {...interviewModalData}
        t={t}
        isInterviewModalOpen={isInterviewModalOpen}
        onClose={closeInterviewModal}
        onAddQuestionClick={handleAddQuestionClick}
        onDeleteQuestionClick={(params) =>
          handleDeleteQuestionClick(params, false)
        }
        groupData={normalizeGroupData(folders)}
      />
      <QuestionModal
        {...questionModalData}
        t={t}
        isQuestionModalOpen={isQuestionModalOpen}
        setQuestions={setQuestions}
        setSelectedQuestions={setSelectedQuestions}
        onClose={closeQuestionModal}
      />
      <ConfirmModal
        {...modalData}
        t={t}
        isOpen={isConfirmModalOpen}
        onClose={closeConfirmModal}
      />
      <EmployeesModal
        {...employeeModalData}
        isOpen={isEmployeesModalOpen}
        t={t}
        addEmployeeInterviewPage
        setEmployees={setEmployees}
        openEmployeesCompareModal={openEmployeesCompareModal}
        onClose={closeEmployeesModal}
      />
      <EmployeesCompareModal
        {...employeesCompareModalData}
        isOpen={isEmployeesCompareModalOpen}
        setEmployees={setEmployees}
        onClose={closeEmployeesCompareModal}
      />
      <SaveAsTemplateModal
        t={t}
        isOpen={isSaveAsTemplateModalOpen}
        templates={templates}
        selectedQuestions={selectedQuestions}
        onClose={closeSaveAsTemplateModal}
      />
    </MainLayout>
  );
};

export default React.memo(InterviewPageContainer);
