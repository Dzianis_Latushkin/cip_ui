import * as Yup from 'yup';
import { separateTemplates } from './TemplatePage.helpers';

export const getValidationSchema = ({ t, templates, templateId }) =>
  Yup.object().shape({
    title: Yup.string()
      .when(['shared'], {
        is: (shared) => !shared,
        then: Yup.string()
          .required(t('titleValidationError'))
          .test('isValid', t('titleSpacesError'), (value) => {
            if (value) {
              return value.replace(/\s/g, '').length;
            }

            return true;
          })
          .test('isValid', t('templateTitleError'), (value) => {
            const { myTemplates } = separateTemplates(templates);

            const othersTemplates = myTemplates.filter(
              (template) => template._id !== templateId,
            );

            if (
              value &&
              othersTemplates.some(
                (item) =>
                  item.title.trim().toLowerCase() ===
                  value.trim().toLowerCase(),
              )
            ) {
              return false;
            }

            return true;
          }),
      })
      .when(['shared'], {
        is: (shared) => shared,
        then: Yup.string()
          .required(t('titleValidationError'))
          .test('isValid', t('templateTitleError'), (value) => {
            if (value) {
              return value.replace(/\s/g, '').length;
            }

            return true;
          })
          .test('isValid', t('templateTitleError'), (value) => {
            const { sharedTemplates } = separateTemplates(templates);
            const othersTemplates = sharedTemplates.filter(
              (template) => template._id !== templateId,
            );

            if (
              value &&
              othersTemplates.some(
                (item) =>
                  item.title.trim().toLowerCase() ===
                  value.trim().toLowerCase(),
              )
            ) {
              return false;
            }

            return true;
          }),
      }),
  });

export const getInitialValues = (t) => {
  return {
    title: t('defaultTemplateTitle'),
    shared: false,
  };
};
