import React from 'react';
import { Formik, Form, Field } from 'formik';
import cn from 'classnames';

import { TEMPLATE_PAGE_STATUS } from 'constants/common';

import {
  getInitialValues,
  getValidationSchema,
} from './TemplatePage.formConfig';

import {
  convertEditDataToForm,
  onPreventKeyDown,
} from './TemplatePage.helpers';

import { timeEstimation } from 'helpers/timeEstimation';

import { Icon } from 'components/shared/Icon';
import { Button } from 'components/shared/Button';
import { Question } from 'components/Question';
import { SortableList } from 'components/SortableList';
import { Typography } from 'components/shared/Typography';
import { ErrorMessage } from 'components/shared/ErrorMessage';

import { InputField, CheckboxField } from 'components/FormikFields';

import styles from './TemplatePage.styles.scss';

const TemplatePage = ({
  t,
  loadingSaveTemplate,
  status,
  templateId,
  template,
  templates,
  selectedQuestions,
  onSortEnd,
  onAddQuestionFromListClick,
  onAddOwnQuestionClick,
  onCheckUnsavedTemplateData,
  onSubmit,
  onEditQuestionClick,
  onDeleteQuestionClick,
}) => (
  <div className={styles.page}>
    <Formik
      validationSchema={getValidationSchema({
        t,
        templates,
        templateId,
      })}
      validateOnMount
      initialValues={
        status === TEMPLATE_PAGE_STATUS.CREATE_TEMPLATE
          ? getInitialValues(t)
          : convertEditDataToForm(template)
      }
      onSubmit={onSubmit}
    >
      {({ values, errors, setFieldError }) => (
        <Form>
          <div className={styles.titleContainer}>
            {status === TEMPLATE_PAGE_STATUS.VIEW_TEMPLATE ? (
              <div className={styles.title}>
                {values.title ? values.title : t('defaultTitle')}
              </div>
            ) : (
              <div className={styles.title}>
                <span className={styles.hiddenValue}>
                  {values.title ? values.title : t('defaultTitle')}
                </span>
                <Field
                  name="title"
                  placeholder={t('defaultTitle')}
                  component={InputField}
                  formControlProps={{ className: styles.formControl }}
                  className={
                    errors.title
                      ? cn(styles.errorInput, styles.titleEditInput)
                      : styles.titleEditInput
                  }
                  onClick={(event) => event.target.select()}
                  onKeyDown={onPreventKeyDown}
                  endAdornment={
                    <Icon name="pencil" className={styles.titleEditIcon} />
                  }
                  endAdornmentClassName={styles.endAdornment}
                />
              </div>
            )}
            {errors.title && (
              <ErrorMessage className={styles.errorMessage}>
                {errors.title}
              </ErrorMessage>
            )}
          </div>
          {status !== TEMPLATE_PAGE_STATUS.VIEW_TEMPLATE && (
            <div className={styles.assigneeTopButtonsContainer}>
              <div>
                <div className={styles.buttonBottomContainer}>
                  <Button
                    onClick={() => {
                      onAddQuestionFromListClick(setFieldError);
                    }}
                    className={styles.button}
                    variant="outlined"
                  >
                    {t('addExistedQuestionButton')}
                  </Button>
                  <Button
                    className={styles.button}
                    onClick={onAddOwnQuestionClick}
                    variant="outlined"
                  >
                    {t('addNewQuestionButton')}
                  </Button>
                </div>
              </div>

              <div className={styles.buttonTopContainer}>
                <Button
                  className={styles.button}
                  variant="outlined"
                  onClick={() => onCheckUnsavedTemplateData(values)}
                >
                  {t('cancelButton')}
                </Button>

                <Button
                  type="submit"
                  disabled={loadingSaveTemplate}
                  loading={loadingSaveTemplate}
                  className={styles.button}
                >
                  {t('saveTemplateButton')}
                </Button>
                <Field
                  id="sharedTemplateCheckboxId"
                  name="shared"
                  component={CheckboxField}
                  label={'Save as shared'}
                  values={values}
                />
              </div>
            </div>
          )}

          {errors.questionsError && (
            <ErrorMessage>{errors.questionsError}</ErrorMessage>
          )}
          {selectedQuestions.length > 0 && (
            <div className={styles.questionContainer}>
              <div className={styles.timeBar}>
                <Typography className={styles.timeBarText}>
                  {'There are '}
                  {selectedQuestions.length}
                  {' question(s). It takes about '}
                  {timeEstimation(selectedQuestions)}
                </Typography>
              </div>

              {status !== TEMPLATE_PAGE_STATUS.VIEW_TEMPLATE ? (
                <SortableList
                  useDragHandle
                  items={selectedQuestions}
                  onSortEnd={onSortEnd}
                  onEditQuestionClick={onEditQuestionClick}
                  onDeleteQuestionClick={onDeleteQuestionClick}
                  renderItem={({ item, ...restProps }) => {
                    return (
                      <Question
                        key={item._id}
                        id={item._id}
                        groupId={item.groupId}
                        question={item.question}
                        comment={item.comment}
                        {...restProps}
                        className={styles.interviewQuestion}
                      />
                    );
                  }}
                />
              ) : (
                selectedQuestions.map((question) => (
                  <Question
                    key={question._id}
                    id={question._id}
                    withoutActions={
                      status === TEMPLATE_PAGE_STATUS.VIEW_TEMPLATE
                    }
                    groupId={question.groupId}
                    question={question.question}
                    comment={question.comment}
                    onEditQuestionClick={onEditQuestionClick}
                    onDeleteQuestionClick={onDeleteQuestionClick}
                    className={styles.interviewViewQuestion}
                  />
                ))
              )}
            </div>
          )}
        </Form>
      )}
    </Formik>
  </div>
);

export default React.memo(TemplatePage);
