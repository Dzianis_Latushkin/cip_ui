import React, { useState, useEffect } from 'react';
import { useTranslation } from 'react-i18next';
import { useNavigate, useParams } from 'react-router-dom';

import { TEMPLATE_PAGE_STATUS } from 'constants/common';

import * as api from 'api/requests/requests';

import { convertDataToApi } from './TemplatePage.helpers';
import { showToast } from 'helpers/showToast';
import { normalizeGroupData } from 'helpers/normalizeGroupData';
import { moveStorableItem } from 'helpers/moveStorableItem';

import { useModal } from 'hooks/useModal';
import { useFetch } from 'hooks/useFetch';

import { MainLayout } from 'components/Layout';
import { ConfirmModal } from 'components/ConfirmModal';

import { QuestionModal } from './components/QuestionModal';
import { InterviewModal } from './components/InterviewModal';

import TemplatePage from './TemplatePage';

const TemplatePageContainer = ({ status }) => {
  const { t } = useTranslation('interviewPage');
  const [tNotifications] = useTranslation('notifications');

  const navigate = useNavigate();
  const { templateId } = useParams();

  const [selectedQuestions, setSelectedQuestions] = useState([]);
  const [loadingSaveTemplate, setLoadingTemplate] = useState(false);

  const [
    isInterviewModalOpen,
    openInterviewModal,
    closeInterviewModal,
    interviewModalData,
  ] = useModal({});

  const [
    isQuestionModalOpen,
    openQuestionModal,
    closeQuestionModal,
    questionModalData,
  ] = useModal({});

  const [isConfirmModalOpen, openConfirmModal, closeConfirmModal, modalData] =
    useModal({});

  const {
    data: { folders },
    loading: foldersLoading,
    setState: setQuestions,
  } = useFetch(
    {
      defaultData: [],
      fetcher: api.getAllFolders,
      stopRequest: status === TEMPLATE_PAGE_STATUS.VIEW_TEMPLATE,
    },
    {},
  );

  const {
    data: { interview: template },
    loading: templateLoading,
  } = useFetch(
    {
      defaultData: [],
      fetcher: api.getOneInterview,
      stopRequest: status === TEMPLATE_PAGE_STATUS.CREATE_TEMPLATE,
    },
    { id: templateId, status: '?status=template' },
  );

  const { data: templates, loading: templatesLoading } = useFetch(
    {
      defaultData: { interviews: [], sharedTemplates: [] },
      fetcher: api.getManagersInterviews,
      stopRequest: status === TEMPLATE_PAGE_STATUS.VIEW_TEMPLATE,
    },
    '?status=template',
  );

  const [lastSavedValues, setLastSavedValues] = useState({
    title: `${templateId ? t('defaultTemplateTitle') : t('defaultTitle')}`,
    employee: { option: null },
  });

  const [lastAmountOfQuestions, setLastAmountOfQuestions] = useState(0);

  useEffect(() => {
    if (template && folders && status === TEMPLATE_PAGE_STATUS.EDIT_TEMPLATE) {
      setSelectedQuestions(template.questions);

      setLastAmountOfQuestions(template.questions.length);
      setLastSavedValues({
        title: template.title,
      });

      setQuestions((prevState) => {
        return {
          ...prevState,
          data: {
            folders: prevState.data.folders.map((nextFolder) => {
              return {
                ...nextFolder,
                questions: nextFolder.questions.map((nextQuestion) => {
                  if (
                    template.questions.some(
                      (question) =>
                        question.folderQuestionId === nextQuestion._id,
                    )
                  ) {
                    return { ...nextQuestion, isSelected: true };
                  }

                  return nextQuestion;
                }),
              };
            }),
          },
        };
      });
    }

    if (template && status === TEMPLATE_PAGE_STATUS.VIEW_TEMPLATE) {
      setSelectedQuestions(template.questions);
    }
  }, [foldersLoading, templateLoading]);

  const handleAddQuestionFromListClick = (setFieldError) => {
    openInterviewModal({ setFieldError });
  };

  const handleAddQuestionClick = (id, setFieldError) => {
    folders.forEach((folder) =>
      folder.questions.find((question) => {
        if (question._id === id) {
          setSelectedQuestions([
            ...selectedQuestions,
            { ...question, folderQuestionId: question._id },
          ]);
        }
      }),
    );

    setQuestions((prevState) => {
      return {
        ...prevState,
        data: {
          folders: prevState.data.folders.map((nextFolder) => {
            return {
              ...nextFolder,
              questions: nextFolder.questions.map((nextQuestion) => {
                if (nextQuestion._id === id) {
                  return { ...nextQuestion, isSelected: true };
                }

                return nextQuestion;
              }),
            };
          }),
        },
      };
    });
    setFieldError('questionsError', '');
  };

  const handleEditQuestionClick = (id) => {
    const editData = selectedQuestions.find((question) => question._id === id);
    openQuestionModal({
      edit: true,
      id,
      editData: editData,
    });
  };

  const handleDeleteQuestion = (id) => {
    const nextQuestions = selectedQuestions
      .filter((question) => question.folderQuestionId !== id)
      .filter((question) => question._id !== id);

    setSelectedQuestions(nextQuestions);
    setQuestions((prevState) => {
      return {
        ...prevState,
        data: {
          folders: prevState.data.folders.map((nextFolder) => {
            return {
              ...nextFolder,
              questions: nextFolder.questions.map((nextQuestion) => {
                if (nextQuestion._id === id) {
                  return { ...nextQuestion, isSelected: false };
                }

                return nextQuestion;
              }),
            };
          }),
        },
      };
    });

    closeConfirmModal();
  };

  const handleDeleteQuestionClick = (id, confirm) => {
    if (confirm) {
      openConfirmModal({
        titleText: t('confirmModalTitle'),
        contentText: t('confirmModalText'),
        onConfirmClick: () => handleDeleteQuestion(id),
        onCancelClick: () => closeConfirmModal(),
      });
    } else {
      handleDeleteQuestion(id);
    }
  };

  const handleSubmit = (values, actions) => {
    setLoadingTemplate(true);

    if (!selectedQuestions.length) {
      setLoadingTemplate(false);
      actions.setFieldError('questionsError', t('templateQuestionsError'));

      return;
    }

    const data = convertDataToApi(values, selectedQuestions);

    if (status === TEMPLATE_PAGE_STATUS.CREATE_TEMPLATE) {
      api
        .createInterview(data)
        .then((res) => {
          setLoadingTemplate(false);

          navigate('/templates');
        })
        .catch(() => {
          setLoadingTemplate(false);
        });
    }

    if (status === TEMPLATE_PAGE_STATUS.EDIT_TEMPLATE) {
      api
        .editInterview(templateId, data)
        .then((res) => {
          setLoadingTemplate(false);

          showToast(tNotifications('saveTemplateNotification'));

          navigate('/templates');
        })
        .catch(() => {
          setLoadingTemplate(false);
        });
    }

    setLastSavedValues(values);
    setLastAmountOfQuestions(selectedQuestions.length);
  };

  const handleAddOwnQuestionClick = () => {
    openQuestionModal();
  };

  const checkUnsavedTemplateData = (values) => {
    if (
      selectedQuestions.length !== lastAmountOfQuestions ||
      values.title !== lastSavedValues.title
    ) {
      openConfirmModal({
        titleText: t('confirmCancelCreationTitle'),
        contentText: t('confirmCancelCreationText'),
        onConfirmClick: () => {
          navigate('/templates');
        },
        onCancelClick: () => closeConfirmModal(),
      });
    } else {
      navigate('/templates');
    }
  };

  const questions = folders || [];

  const loading = foldersLoading || templateLoading || templatesLoading;

  const onSortEnd = ({ oldIndex, newIndex }) => {
    setSelectedQuestions((prevState) =>
      moveStorableItem(prevState, oldIndex, newIndex),
    );
  };

  return (
    <MainLayout loading={loading}>
      <TemplatePage
        t={t}
        loadingSaveTemplate={loadingSaveTemplate}
        status={status}
        templateId={templateId}
        template={template}
        templates={templates}
        selectedQuestions={selectedQuestions}
        onSortEnd={onSortEnd}
        onAddQuestionFromListClick={handleAddQuestionFromListClick}
        onAddOwnQuestionClick={handleAddOwnQuestionClick}
        onCheckUnsavedTemplateData={checkUnsavedTemplateData}
        onSubmit={handleSubmit}
        onEditQuestionClick={handleEditQuestionClick}
        onDeleteQuestionClick={(params) =>
          handleDeleteQuestionClick(params, true)
        }
      />
      <InterviewModal
        {...interviewModalData}
        t={t}
        isInterviewModalOpen={isInterviewModalOpen}
        onClose={closeInterviewModal}
        onAddQuestionClick={handleAddQuestionClick}
        onDeleteQuestionClick={(params) =>
          handleDeleteQuestionClick(params, false)
        }
        groupData={normalizeGroupData(questions)}
      />
      <QuestionModal
        {...questionModalData}
        t={t}
        isQuestionModalOpen={isQuestionModalOpen}
        setQuestions={setQuestions}
        setSelectedQuestions={setSelectedQuestions}
        onClose={closeQuestionModal}
      />
      <ConfirmModal
        {...modalData}
        t={t}
        isOpen={isConfirmModalOpen}
        onClose={closeConfirmModal}
      />
    </MainLayout>
  );
};

export default React.memo(TemplatePageContainer);
