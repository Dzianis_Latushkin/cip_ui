import React from 'react';
import cn from 'classnames';

import { Group } from 'components/Group';
import { Question } from 'components/Question';

import styles from './InterviewModal.styles.scss';

export const groupRender = ({
  groupData,
  setFieldError,
  onAddQuestionClick,
  onDeleteQuestionClick,
}) =>
  groupData.map((group, index) => {
    if (group.folders) {
      return (
        <Group
          key={group._id}
          id={group._id}
          groupName={group.name}
          openedClassName={cn(styles.openedFirst, {
            [styles.opened]: group.parentId,
          })}
          closedClassName={cn(styles.closedFirst, {
            [styles.closed]: group.parentId,
          })}
          isInterviewModal
        >
          {groupRender({
            groupData: group.folders,
            onAddQuestionClick: onAddQuestionClick,
            onDeleteQuestionClick: onDeleteQuestionClick,
            setFieldError,
          })}
          {group.questions.map((item) => (
            <Question
              key={item._id}
              isInterviewModal
              isSelected={item.isSelected}
              id={item._id}
              groupId={group._id}
              question={item.question}
              comment={item.comment}
              setFieldError={setFieldError}
              onAddQuestionClick={onAddQuestionClick}
              onDeleteQuestionClick={onDeleteQuestionClick}
            />
          ))}
        </Group>
      );
    } else {
      return (
        <Group
          key={group._id}
          isInterviewModal
          id={group._id}
          groupName={group.name}
          openedClassName={cn(styles.openedFirst, {
            [styles.opened]: group.parentId,
          })}
          closedClassName={cn(styles.closedFirst, {
            [styles.closed]: group.parentId,
          })}
        >
          {group.questions.map((item) => (
            <Question
              key={item._id}
              isInterviewModal
              isSelected={item.isSelected}
              id={item._id}
              groupId={group._id}
              question={item.question}
              comment={item.comment}
              onAddQuestionClick={onAddQuestionClick}
              onDeleteQuestionClick={onDeleteQuestionClick}
              setFieldError={setFieldError}
            />
          ))}
        </Group>
      );
    }
  });
