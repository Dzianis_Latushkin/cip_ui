import React from 'react';
import { v4 as uuid } from 'uuid';

import QuestionModal from './QuestionModal';

const QuestionModalContainer = ({
  t,
  edit,
  id,
  editData,
  onClose,
  setSelectedQuestions,
  setQuestions,
  ...restProps
}) => {
  const handleSubmit = (values) => {
    const { title, comment } = values;

    if (id) {
      setSelectedQuestions((prevState) => {
        const nextQuestions = prevState.map((question) => {
          if (question._id === id) {
            const nextTitle = question.question;
            const nextComment = question.comment;

            if (nextTitle === title && nextComment === comment) {
              return question;
            } else {
              setQuestions((prevQuestionsState) => {
                return {
                  ...prevQuestionsState,
                  data: {
                    folders: prevQuestionsState.data.folders.map(
                      (nextFolder) => {
                        return {
                          ...nextFolder,
                          questions: nextFolder.questions.map(
                            (nextQuestion) => {
                              if (nextQuestion._id === id) {
                                return { ...nextQuestion, isSelected: false };
                              }

                              return nextQuestion;
                            },
                          ),
                        };
                      },
                    ),
                  },
                };
              });

              return { comment, question: title, _id: uuid() };
            }
          }

          return question;
        });

        return nextQuestions;
      });
    } else {
      setSelectedQuestions((prevState) => {
        return [...prevState, { comment, question: title, _id: uuid() }];
      });
    }

    onClose();
  };

  return (
    <QuestionModal
      t={t}
      edit={edit}
      editData={editData}
      onClose={onClose}
      onSubmit={handleSubmit}
      {...restProps}
    />
  );
};

export default React.memo(QuestionModalContainer);
