import * as Yup from 'yup';

export const getValidationSchema = (t) =>
  Yup.object().shape({
    title: Yup.string()
      .required(t('questionModal.titleValidationError'))
      .test('isValid', t('questionModal.titleSpacesError'), (value) => {
        if (value) {
          return value.replace(/\s/g, '').length;
        }

        return true;
      }),
    comment: Yup.string().test(
      'isValid',
      t('questionModal.commentValidationError'),
      (value) => {
        if (value) {
          return value.replace(/\s/g, '').length;
        }

        return true;
      },
    ),
  });

export const initialValues = {
  title: '',
  comment: '',
};
