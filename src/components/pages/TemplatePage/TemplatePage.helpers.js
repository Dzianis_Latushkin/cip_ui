export const convertEditDataToForm = (template) => ({
  title: template?.title,
  shared: template?.shared,
});

export const convertDataToApi = (value, selectedQuestions) => ({
  title: value.title,
  status: 'template',
  employee: null,
  questions: selectedQuestions,
  shared: value.shared,
});

export const onPreventKeyDown = (keyEvent) => {
  if ((keyEvent.charCode || keyEvent.keyCode) === 13) {
    keyEvent.preventDefault();
    keyEvent.target.blur();
  }
};

export const separateTemplates = ({ interviews, sharedTemplates }) => {
  const myTemplates = interviews.filter((templateItem) => !templateItem.shared);
  const otherTemplates = [
    ...sharedTemplates,
    ...interviews.filter((templateItem) => templateItem.shared),
  ];

  return { myTemplates, sharedTemplates: otherTemplates };
};
