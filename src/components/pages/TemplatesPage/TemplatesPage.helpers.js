export const divideInterviews = (interviews) => {
  const planned = interviews.filter(
    (interview) => interview.status === 'planned',
  );

  return { planned };
};
