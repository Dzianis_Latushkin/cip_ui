import React from 'react';
import cn from 'classnames';
import { Tooltip } from 'react-tippy';

import { Icon } from 'components/shared/Icon';
import { Typography } from 'components/shared/Typography';

import styles from './Template.styles.scss';

const Template = ({
  t,
  navigate,
  template,
  templateList,
  onDeleteTemplateClick,
  onDuplicateTemplateClick,
  isOtherSharedTemplate,
}) => (
  <div
    className={cn(styles.template)}
    onClick={() => {
      navigate(`/template/${template._id}`);
    }}
  >
    <div className={styles.templateContainer}>
      <Icon name="template" className={styles.templateIcon} />
      <div className={styles.templateInformation}>
        <Typography variant="body" className={styles.templateContent}>
          {template.title}
        </Typography>

        {isOtherSharedTemplate && (
          <Typography variant="body" className={styles.templateCreator}>
            Created by: {template.creator?.name}
          </Typography>
        )}
      </div>
    </div>

    <div className={styles.buttonContainer}>
      <Tooltip
        html={
          <div className={styles.tooltip}>
            <Typography className={styles.tooltipText}>
              {t('template.createFromTemplate')}
            </Typography>
          </div>
        }
        position="top-end"
        trigger="mouseenter"
        delay={[1000, 1]}
      >
        <Icon
          name="newInterview"
          className={styles.icon}
          onClick={(e) => {
            e.stopPropagation();
            navigate(`/interview/create?templateId=${template._id}`);
          }}
        />
      </Tooltip>
      {!isOtherSharedTemplate && (
        <>
          <Tooltip
            html={
              <div className={styles.tooltip}>
                <Typography className={styles.tooltipText}>
                  {t('template.editTemplate')}
                </Typography>
              </div>
            }
            position="top-end"
            trigger="mouseenter"
            delay={[1000, 1]}
          >
            <Icon
              name="pencil"
              className={styles.icon}
              onClick={(e) => {
                e.stopPropagation();
                navigate(`/template/${template._id}/edit`);
              }}
            />
          </Tooltip>

          <Tooltip
            html={
              <div className={styles.tooltip}>
                <Typography className={styles.tooltipText}>
                  {t('template.deleteTemplate')}
                </Typography>
              </div>
            }
            position="top-end"
            trigger="mouseenter"
            delay={[1000, 1]}
          >
            <Icon
              name="delete"
              className={cn(styles.icon, styles.removeIcon)}
              onClick={(e) => {
                e.stopPropagation();
                onDeleteTemplateClick(template._id);
              }}
            />
          </Tooltip>
        </>
      )}
      <Tooltip
        html={
          <div className={styles.tooltip}>
            <Typography className={styles.tooltipText}>
              {t('template.copyTemplate')}
            </Typography>
          </div>
        }
        position="top-end"
        trigger="mouseenter"
        delay={[1000, 1]}
      >
        <Icon
          name="copy"
          className={cn(styles.icon, styles.removeIcon)}
          onClick={(e) => {
            e.stopPropagation();
            onDuplicateTemplateClick(template._id, templateList);
          }}
        />
      </Tooltip>
    </div>
  </div>
);

export default React.memo(Template);
