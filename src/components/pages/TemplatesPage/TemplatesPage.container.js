import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { useTranslation } from 'react-i18next';

import * as api from 'api/requests/requests';

import { showToast } from 'helpers/showToast';

import { changeTitle } from 'helpers/changeTitleInterviewsCard';
import { moveStorableItem } from 'helpers/moveStorableItem';

import { useFetch } from 'hooks/useFetch';
import { useModal } from 'hooks/useModal';

import { MainLayout } from 'components/Layout';
import { ConfirmModal } from 'components/ConfirmModal';

import TemplatesPage from './TemplatesPage';

const TemplatesPageContainer = () => {
  const { t } = useTranslation('templatesPage');
  const [tNotifications] = useTranslation('notifications');
  const navigate = useNavigate();

  const [confirmLoading, setConfrimLoading] = useState(false);

  const [isConfirmModalOpen, openConfirmModal, closeConfirmModal, modalData] =
    useModal({});

  const {
    data: {
      interviews: { myTemplates, privateSharedTemplates, otherSharedTemplates },
    },
    loading,
    setState: setTemplates,
  } = useFetch(
    {
      defaultData: {
        interviews: {
          myTemplates: [],
          privateSharedTemplates: [],
          otherSharedTemplates: [],
        },
      },
      fetcher: api.getManagersInterviews,
      immediate: true,
      onSuccess: (response) => {
        setTemplates((prevState) => ({
          ...prevState,
          data: {
            interviews: {
              myTemplates: response.interviews
                .filter((template) => !template.shared)
                .sort(
                  ({ order: orderOne }, { order: orderTwo }) =>
                    orderOne - orderTwo,
                ),
              privateSharedTemplates: response.interviews
                .reverse()
                .filter((template) => template.shared),
              otherSharedTemplates: response.sharedTemplates,
            },
          },
        }));
      },
    },
    '?status=template',
  );

  const handleDeleteTemplateClick = (templateId) => {
    openConfirmModal({
      titleText: t('confirmModalTitle'),
      contentText: t('confirmModalContent'),
      onConfirmClick: () => handleDeleteTemplate(templateId),
      onCancelClick: () => closeConfirmModal(),
    });
  };

  const handleDeleteTemplate = (templateId) => {
    setConfrimLoading(true);

    api
      .deleteInterview(templateId)
      .then((res) => {
        setTemplates((prevState) => {
          return {
            ...prevState,
            data: {
              ...prevState.data,
              interviews: {
                ...prevState.data.interviews,
                myTemplates: prevState.data.interviews.myTemplates.filter(
                  (template) => template._id !== templateId,
                ),
                privateSharedTemplates:
                  prevState.data.interviews.privateSharedTemplates.filter(
                    (template) => template._id !== templateId,
                  ),
              },
            },
          };
        });

        showToast(tNotifications('deleteTemplateNotification'));
        setConfrimLoading(false);
        closeConfirmModal();
      })
      .catch(() => {
        setConfrimLoading(false);
      });
  };

  const handleDuplicateTemplateClick = (templateId, templateList) => {
    openConfirmModal({
      titleText: t('confirmTemplateModalTitle'),
      contentText: t('confirmTemplateModalContent'),
      onConfirmClick: () => handleDuplicateTemplate(templateId, templateList),
      onCancelClick: () => closeConfirmModal(),
    });
  };

  const handleDuplicateTemplate = (id, templateList) => {
    const nextTemplate = templateList.find((template) => template._id === id);
    const nextTitle = changeTitle(
      nextTemplate.title,
      myTemplates,
      nextTemplate,
    );

    setConfrimLoading(true);

    api
      .createInterview({
        title: nextTitle,
        status: 'template',
        questions: nextTemplate.questions,
        share: false,
      })
      .then((res) => {
        setTemplates((prevState) => {
          return {
            ...prevState,
            data: {
              ...prevState.data,
              interviews: {
                ...prevState.data.interviews,
                myTemplates: [
                  ...prevState.data.interviews.myTemplates,
                  res.data.interview,
                ],
              },
            },
          };
        });
      })
      .catch();

    setConfrimLoading(false);
    closeConfirmModal();
  };

  const onSortEnd = ({ oldIndex, newIndex }) => {
    setTemplates((prevState) => {
      return {
        ...prevState,
        data: {
          ...prevState.data,
          interviews: {
            ...prevState.data.interviews,
            myTemplates: moveStorableItem(
              prevState.data.interviews.myTemplates,
              oldIndex,
              newIndex,
            ),
          },
        },
      };
    });

    api
      .interviewOrderUpdate(myTemplates[oldIndex]._id, {
        newOrder: newIndex + 1,
      })
      .catch((err) => {
        console.log(err);
      });
  };

  return (
    <MainLayout loading={loading} pageTitle={t('pageTitle')}>
      <TemplatesPage
        t={t}
        navigate={navigate}
        myTemplates={myTemplates}
        otherSharedTemplates={otherSharedTemplates}
        privateSharedTemplates={privateSharedTemplates}
        onSortEnd={onSortEnd}
        onDeleteTemplateClick={handleDeleteTemplateClick}
        onDuplicateTemplateClick={handleDuplicateTemplate}
        onDuplicateSharedTemplateClick={handleDuplicateTemplateClick}
      />
      <ConfirmModal
        {...modalData}
        loading={confirmLoading}
        isOpen={isConfirmModalOpen}
        onClose={closeConfirmModal}
      />
    </MainLayout>
  );
};

export default React.memo(TemplatesPageContainer);
