import React from 'react';

import { Icon } from 'components/shared/Icon';
import { Button } from 'components/shared/Button';
import { Tabs, Tab } from 'components/shared/Tabs';
import { SortableList } from 'components/SortableList';
import { Typography } from 'components/shared/Typography';

import { Template } from './components/Template';

import styles from './TemplatesPage.styles.scss';

export const noTemplateMessage = (message, t) => (
  <div className={styles.infoMessage}>
    <div className={styles.infoMessageIconWrap}>
      <Icon name="info" className={styles.infoMessageIcon} />
    </div>
    <Typography variant="h3" component="h3" className={styles.infoMessageText}>
      {t(message)}
    </Typography>
  </div>
);

const TemplatesPage = ({
  loading,
  navigate,
  myTemplates,
  privateSharedTemplates,
  otherSharedTemplates,
  t,
  onDeleteTemplateClick,
  onDuplicateTemplateClick,
  onDuplicateSharedTemplateClick,
  onSortEnd,
}) => (
  <div>
    <div className={styles.buttonBar}>
      <Button
        className={styles.button}
        onClick={() => {
          navigate('/template/create');
        }}
      >
        {t('addTemplateButton')}
      </Button>
    </div>
    <Tabs size="large">
      <Tab label={t('myTemplatesTab')}>
        <div className={styles.templatesList}>
          {myTemplates && myTemplates.length > 0 ? (
            <SortableList
              useDragHandle
              items={myTemplates}
              onSortEnd={onSortEnd}
              onDeleteTemplateClick={onDeleteTemplateClick}
              onDuplicateTemplateClick={onDuplicateTemplateClick}
              renderItem={({ item, ...restProps }) => {
                return (
                  <Template
                    key={item._id}
                    t={t}
                    navigate={navigate}
                    template={item}
                    templateList={myTemplates}
                    disabled={true}
                    {...restProps}
                  />
                );
              }}
            />
          ) : (
            noTemplateMessage('noTemplatesText', t)
          )}
        </div>
      </Tab>

      <Tab label={t('sharedTemplatesTab')}>
        <div className={styles.sharedTemplatesList}>
          <div className={styles.privateSharedTemplates}>
            <Typography
              variant="h3"
              component="h3"
              className={styles.sharedTemplateTitle}
            >
              {t('privateSharedTemplatesTitle')}
            </Typography>
            {privateSharedTemplates && privateSharedTemplates.length > 0
              ? privateSharedTemplates.map(
                  (template) =>
                    template.shared && (
                      <Template
                        key={template._id}
                        t={t}
                        navigate={navigate}
                        template={template}
                        templateList={privateSharedTemplates}
                        onDeleteTemplateClick={onDeleteTemplateClick}
                        onDuplicateTemplateClick={
                          onDuplicateSharedTemplateClick
                        }
                      />
                    ),
                )
              : noTemplateMessage('noSharedTemplatesText', t)}
          </div>

          <div className={styles.otherSharedTemplates}>
            <Typography
              variant="h3"
              component="h3"
              className={styles.sharedTemplateTitle}
            >
              {t('otherSharedTemplatesTitle')}
            </Typography>
            {otherSharedTemplates && otherSharedTemplates.length > 0
              ? otherSharedTemplates.map(
                  (template) =>
                    template.shared && (
                      <Template
                        key={template._id}
                        t={t}
                        navigate={navigate}
                        template={template}
                        templateList={otherSharedTemplates}
                        isOtherSharedTemplate
                        onDeleteTemplateClick={onDeleteTemplateClick}
                        onDuplicateTemplateClick={
                          onDuplicateSharedTemplateClick
                        }
                      />
                    ),
                )
              : noTemplateMessage('noSharedTemplatesText', t)}
          </div>
        </div>
      </Tab>
    </Tabs>
  </div>
);

export default React.memo(TemplatesPage);
