import React from 'react';
import { useTranslation } from 'react-i18next';

import { Icon } from 'components/shared/Icon';
import { Typography } from 'components/shared/Typography';

import { MainLayout } from 'components/Layout';

import styles from './RestrictedPage.styles.scss';

const RestrictedPage = () => {
  const { t } = useTranslation('restrictedPage');

  return (
    <MainLayout>
      <div className={styles.container}>
        <div className={styles.imgWrap}>
          <Icon name="lock" className={styles.imgLock} />
          <Icon name="screen" className={styles.imgScreen} />
        </div>
        <div className={styles.message}>
          <Typography
            variant="h1"
            component="h1"
            className={styles.messageTitle}
          >
            {t('title')}
          </Typography>
          <Typography variant="h3" component="h3">
            {t('description')}
          </Typography>
        </div>
      </div>
    </MainLayout>
  );
};

export default RestrictedPage;
