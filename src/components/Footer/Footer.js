import React from 'react';
import { useTranslation } from 'react-i18next';

import { Typography } from 'components/shared/Typography';

import styles from './Footer.styles.scss';

const Footer = () => {
  const { t } = useTranslation('footer');

  return (
    <div className={styles.footer}>
      <Typography variant="subtitle" className={styles.text}>
        {t('copyright')}
      </Typography>
    </div>
  );
};

export default Footer;
