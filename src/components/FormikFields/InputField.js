import React from 'react';
import { getIn } from 'formik';

import { Input } from 'components/shared/Input';
import { FormControl } from 'components/shared/FormControl';

const InputField = ({
  classNames,
  errorClassName,
  label,
  errorId,
  field,
  form,
  formControlProps,
  onBlur,
  ...restProps
}) => {
  const error = getIn(form.errors, field.name);

  const touched = form.touched[field.name];

  const hasError = touched && !!error;

  const handleBlur = (event) => {
    field.onBlur(event);

    if (onBlur) {
      onBlur(event, field.value);
    }
  };

  return (
    <FormControl
      className={classNames.formControl}
      {...formControlProps}
      label={label}
      errorId={errorId}
      error={error}
      touched={touched}
      errorClassName={errorClassName}
    >
      <Input
        {...field}
        {...restProps}
        variant={hasError ? 'error' : 'default'}
        onBlur={handleBlur}
      />
    </FormControl>
  );
};

InputField.defaultProps = {
  classNames: {},
};

export default React.memo(InputField);
