import React from 'react';

import { Radio } from 'components/shared/Radio';

const RadioField = ({
  haveError,
  field,
  form,
  value,
  onChange,
  ...restProps
}) => {
  const handleChange = (event) => {
    form.setFieldValue(field.name, event.target.value);

    if (onChange) {
      onChange(event.target.value);
    }
  };

  return (
    <Radio
      {...restProps}
      checked={field.value === value}
      value={value}
      onChange={handleChange}
    />
  );
};

export default React.memo(RadioField);
