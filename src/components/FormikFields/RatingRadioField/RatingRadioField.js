import React from 'react';
import cn from 'classnames';
import { getIn } from 'formik';
import { useTranslation } from 'react-i18next';

import { RatingRadio } from 'components/shared/RatingRadio';
import { FormControl } from 'components/shared/FormControl';
import { Typography } from 'components/shared/Typography';

import styles from './RatingRadioField.styles.scss';

const RatingRadioField = ({
  classNames,
  size,
  ratings,
  label,
  field,
  form,
  onAfterChange,
  formControlClassName,
  ...restProps
}) => {
  const { t } = useTranslation('ratingRadioField');

  const error = getIn(form.errors, field.name);
  const touched = form.touched[field.name];

  const handleChange = (event) => {
    const value = event.target.value;

    form.setFieldValue(field.name, value);

    if (onAfterChange) {
      onAfterChange(value);
    }
  };

  return (
    <FormControl
      className={cn(
        styles.control,
        classNames.formControl,
        formControlClassName,
      )}
      label={label}
      error={error}
      touched={touched}
    >
      <div className={styles.ratingsWrapper}>
        <Typography variant="body" className={styles.ratingTitle}>
          {t('answerRateLabel')}
        </Typography>
        <div className={styles.ratingsRadioWrapper}>
          {ratings.map((item) => (
            <RatingRadio
              key={item.value}
              {...field}
              {...restProps}
              checked={item.value === field.value}
              value={item.value}
              onChange={handleChange}
            />
          ))}

          {field.value && (
            <Typography variant="body" className={styles.ratingComment}>
              {ratings[field.value - 1].comment}
            </Typography>
          )}
        </div>
      </div>
    </FormControl>
  );
};

RatingRadioField.defaultProps = {
  classNames: {},
};

export default React.memo(RatingRadioField);
