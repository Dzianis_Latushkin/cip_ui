import React from 'react';

import { Checkbox } from 'components/shared/Checkbox';

const CheckboxField = ({ field, form, value, onChange, id, ...restProps }) => {
  const handleChange = (event) => {
    form.setFieldValue(field.name, event.target.checked);

    if (onChange) {
      onChange(event.target.checked);
    }
  };

  return (
    <Checkbox
      {...restProps}
      id={id}
      value={value}
      onChange={handleChange}
      checked={field.value}
    />
  );
};

export default React.memo(CheckboxField);
