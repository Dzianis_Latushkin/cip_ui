import React from 'react';
import cn from 'classnames';

import { Loading } from 'components/shared/Loading';
import { Typography } from 'components/shared/Typography';
import { ScrollWrapper } from 'components/shared/ScrollWrapper';

import { SideBar } from 'components/SideBar';
import { ConnectionLost } from 'components/ConnectionLost';

import styles from './MainLayout.styles.scss';

const MainLayout = ({
  isOnline,
  loading,
  pageRef,
  className,
  titleClassName,
  pageTitle,
  children,
  openAppFeedbackModal,
}) => {
  return (
    <div className={cn(styles.layout, className)}>
      <SideBar openAppFeedbackModal={openAppFeedbackModal} />
      <ScrollWrapper className={styles.scroll}>
        <div className={styles.inner} ref={pageRef}>
          {loading ? (
            <Loading variant="primary" size={30} className={styles.loading} />
          ) : (
            <>
              {pageTitle && (
                <div className={styles.title}>
                  <Typography variant="h1" component="h1">
                    {pageTitle}
                  </Typography>
                </div>
              )}

              <div className={styles.main}>
                {isOnline && children}
                {!isOnline && <ConnectionLost />}
              </div>
            </>
          )}
        </div>
      </ScrollWrapper>
    </div>
  );
};

export default React.memo(MainLayout);
