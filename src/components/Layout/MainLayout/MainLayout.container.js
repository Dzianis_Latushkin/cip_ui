import React from 'react';

import { useModal } from 'hooks/useModal';
import { useNetwork } from 'hooks/useNetwork';

import { AppFeedbackModal } from 'components/AppFeedbackModal';

import MainLayout from './MainLayout';

const MainLayoutContainer = ({
  pageRef,
  className,
  titleClassName,
  pageTitle,
  children,
  onClose,
  ...restProps
}) => {
  const [isAppFeedbackModalOpen, openAppFeedbackModal, closeAppFeedbackModal] =
    useModal({});

  const isOnline = useNetwork();

  return (
    <>
      <MainLayout
        isOnline={isOnline}
        pageRef={pageRef}
        className={className}
        titleClassName={titleClassName}
        pageTitle={pageTitle}
        children={children}
        openAppFeedbackModal={openAppFeedbackModal}
        {...restProps}
      />
      <AppFeedbackModal
        isAppFeedbackModalOpen={isAppFeedbackModalOpen}
        onClose={closeAppFeedbackModal}
      />
    </>
  );
};

export default React.memo(MainLayoutContainer);
