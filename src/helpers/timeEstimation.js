import { INTERVIEWS_MINUTES_PER_QUESTION } from 'constants/common';

export const timeEstimation = (arr) => {
  const minTime = arr.length * INTERVIEWS_MINUTES_PER_QUESTION.MINIMUM;
  const maxTime = arr.length * INTERVIEWS_MINUTES_PER_QUESTION.MAXIMUM;

  const timeConvert = (n) => {
    const num = n;
    const hours = num / 60;
    const rhours = Math.floor(hours);
    const minutes = (hours - rhours) * 60;
    const rminutes = Math.round(minutes);

    return (rhours ? rhours + 'h ' : null) + rminutes + 'min';
  };

  return `(${timeConvert(minTime)} - ${timeConvert(maxTime)})`;
};
