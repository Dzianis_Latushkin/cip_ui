export const changeTitle = (title, planed, nextInterview) => {
  const titlesCounter = [];

  if (title.match(/Copy( \d+)?/g)) {
    planed.forEach(
      (interview) =>
        interview.title.includes(
          `${title.replace(/\(Copy( \d+)?\)/g, '(Copy ')}`,
        ) &&
        titlesCounter.push(
          +interview.title.match(/Copy( \d+)?/g)[0].match(/\d+/g)[0],
        ),
    );

    if (!titlesCounter.length) {
      return title.replace(/Copy( \d+)?/g, 'Copy 2');
    } else {
      return title.replace(
        /Copy( \d+)?/g,
        `Copy ${Math.max(...titlesCounter) + 1}`,
      );
    }
  } else {
    planed.forEach(
      (interview) =>
        interview.title.includes(`${title}`) &&
        interview.title.match(/Copy( \d+)?/g) &&
        titlesCounter.push(
          !interview.title.match(/Copy( \d+)?/g)[0].match(/\d+/g)
            ? 2
            : +interview.title.match(/Copy( \d+)?/g)[0].match(/\d+/g)[0],
        ),
    );

    if (!titlesCounter.length) {
      return `${nextInterview.title}(Copy)`;
    } else {
      return `${nextInterview.title}(Copy ${Math.max(...titlesCounter) + 1})`;
    }
  }
};
