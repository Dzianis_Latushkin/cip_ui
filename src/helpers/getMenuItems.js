import { ROUTES } from 'constants/routes';
import { ROLES } from 'constants/common';

export const getMenuItems = ({ t, role }) => {
  switch (role) {
    case ROLES.ADMIN: {
      return [
        {
          key: 'managers',
          icon: 'users',
          title: t('managers'),
          to: ROUTES.ADMIN.MANAGERS_PAGE,
        },
        {
          key: 'questions',
          icon: 'question',
          title: t('questions'),
          to: ROUTES.ADMIN.QUESTIONS_PAGE,
        },
        {
          key: 'projects',
          icon: 'projectInterview',
          title: t('projects'),
          to: ROUTES.ADMIN.PROJECTS_PAGE,
        },
      ];
    }
    case ROLES.MANAGER: {
      return [
        {
          key: 'employees',
          icon: 'users',
          title: t('employees'),
          to: ROUTES.MANAGER.EMPLOYEES_PAGE,
        },
        {
          key: 'interviews',
          icon: 'chat',
          title: t('interviews'),
          to: ROUTES.MANAGER.INTERVIEWS_PAGE,
        },
        {
          key: 'archives',
          icon: 'archive',
          title: t('interviewArchive'),
          to: ROUTES.MANAGER.ARCHIVE_PAGE,
        },
        {
          key: 'templates',
          icon: 'template',
          title: t('interviewTemplates'),
          to: ROUTES.MANAGER.TEMPLATES_PAGE,
        },
        {
          key: 'projectInterviews',
          icon: 'projectInterview',
          title: t('projectInterviews'),
          to: ROUTES.PROJECT_INTERVIEWS_PAGE,
        },
        {
          key: 'projectsHistory',
          icon: 'projectHistory',
          title: t('projectsHistory'),
          to: ROUTES.PROJECTS_HISTORY_PAGE,
        },
        {
          key: 'settings',
          icon: 'settings',
          title: t('settings'),
          to: ROUTES.SETTINGS_PAGE,
        },
      ];
    }
    case ROLES.EMPLOYEE: {
      return [
        {
          key: 'interviews',
          icon: 'chat',
          title: t('interviews'),
          to: ROUTES.EMPLOYEE.EMPLOYEE_INTERVIEWS_PAGE,
        },
        {
          key: 'projectInterviews',
          icon: 'projectInterview',
          title: t('projectInterviews'),
          to: ROUTES.PROJECT_INTERVIEWS_PAGE,
        },
        {
          key: 'projectsHistory',
          icon: 'projectHistory',
          title: t('projectsHistory'),
          to: ROUTES.PROJECTS_HISTORY_PAGE,
        },
        {
          key: 'settings',
          icon: 'settings',
          title: t('settings'),
          to: ROUTES.SETTINGS_PAGE,
        },
      ];
    }
    default: {
      return [];
    }
  }
};
