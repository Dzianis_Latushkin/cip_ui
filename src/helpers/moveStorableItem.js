export const moveStorableItem = (array, from, to) => {
  const nextArray = [...array];

  const startIndex = from < 0 ? nextArray.length + from : from;

  if (startIndex >= 0 && startIndex < nextArray.length) {
    const endIndex = to < 0 ? nextArray.length + to : to;

    const [item] = nextArray.splice(from, 1);

    nextArray.splice(endIndex, 0, item);
  }

  return nextArray;
};
