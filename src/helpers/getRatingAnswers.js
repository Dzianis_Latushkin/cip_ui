export const getRatingAnswers = (t) => [
  { value: '1', comment: t('onePointComment') },
  {
    value: '2',
    comment: t('twoPointsComment'),
  },
  { value: '3', comment: t('threePointsComment') },
  { value: '4', comment: t('fourPointsComment') },
  {
    value: '5',
    comment: t('fivePointsComment'),
  },
];
