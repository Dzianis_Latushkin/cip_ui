export const elapsedTime = (time) => {
  const num = time;
  const hours = num / 60 / 60;
  const rhours = Math.floor(hours);
  const minutes = (hours - rhours) * 60;
  const rminutes = Math.round(minutes);
  const seconds = (minutes - rminutes) * 60;
  const rseconds = Math.round(seconds);

  return (
    (rhours ? rhours + 'h ' : '') +
    (rminutes ? rminutes + 'min ' : '') +
    (seconds ? rseconds + 'sec ' : '')
  );
};
