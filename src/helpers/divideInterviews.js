export const divideInterviews = (interviews, interviewsPageType) => {
  const planned = interviews.filter(
    (interview) => interview.status === 'planned',
  );
  const completed = interviews.filter(
    (interview) => interview.status === 'completed',
  );
  const archived = interviews.filter(
    (interview) => interview.status === 'archived',
  );

  switch (interviewsPageType) {
    case 'INTERVIEWS':
      return { planned, completed };
    case 'ARCHIVE':
      return { planned, archived };
    default:
      return { planned };
  }
};
