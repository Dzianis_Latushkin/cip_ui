import { ROLES } from 'constants/common';

export const getPrimaryRoute = (role) => {
  if (role === ROLES.ADMIN) {
    return '/managers';
  }

  if (role === ROLES.MANAGER) {
    return '/employees';
  }

  if (role === ROLES.EMPLOYEE) {
    return '/results';
  }
};
