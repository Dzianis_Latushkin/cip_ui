import _cloneDeep from 'lodash/cloneDeep';

export const normalizeGroupData = (foldersData) => {
  const nextFolderData = _cloneDeep(foldersData);

  return nextFolderData
    .map((folder) => {
      if (folder.parentId) {
        let parentsFolder = nextFolderData.find(
          (searchFolder) => searchFolder._id === folder.parentId,
        );

        if (parentsFolder) {
          if (parentsFolder.folders) {
            parentsFolder.folders.push(folder);
          } else {
            parentsFolder.folders = [];
            parentsFolder.folders.push(folder);
          }
        }
      } else {
        return folder;
      }
    })
    .filter((folder) => folder);
};
