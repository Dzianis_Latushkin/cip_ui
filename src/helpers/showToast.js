import { toast } from 'react-toastify';

export const showToast = (text) => {
  toast(text, {
    closeOnClick: true,
    position: toast.POSITION.BOTTOM_RIGHT,
    autoClose: 3000,
    // closeButton: () => null,
  });
};
