const SET_USER = 'SET_USER';
const LOGOUT_USER = 'LOGOUT_USER';

import { CIP_TOKEN_KEY } from 'constants/token';

const defaultState = {
  currentUser: {},
  isAuth: false,
};

export default function userReducer(state = defaultState, { type, payload }) {
  switch (type) {
    case SET_USER:
      return {
        ...state,
        currentUser: { ...payload },
        isAuth: true,
      };
    case LOGOUT_USER:
      return {
        ...state,
        currentUser: {},
        isAuth: false,
      };

    default:
      return state;
  }
}

export const setUser = (dispatch, data) => {
  if (data.token) {
    localStorage.setItem(CIP_TOKEN_KEY, data.token);
  }

  dispatch({ type: SET_USER, payload: data.user });
};

export const logoutUser = (dispatch) => {
  localStorage.removeItem(CIP_TOKEN_KEY);

  dispatch({ type: LOGOUT_USER });
};
