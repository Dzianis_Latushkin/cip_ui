import i18n from 'i18next';
import { initReactI18next } from 'react-i18next';

import translations from './locales/translation.json';

function checkLanguage(obj, lang) {
  let clone = {};
  for (let prop in obj) {
    if (
      typeof obj[prop] === 'object' &&
      Object.keys(obj[prop]).includes(lang)
    ) {
      clone[prop] = obj[prop][lang];
    } else if (typeof obj[prop] === 'object') {
      clone[prop] = checkLanguage(obj[prop], lang);
    }
  }

  return clone;
}

i18n.use(initReactI18next).init({
  lng: 'en',
  fallbackLng: 'en',
  interpolation: {
    escapeValue: false,
  },
  resources: {
    en: checkLanguage(translations, 'en'),
  },
});

export default i18n;
