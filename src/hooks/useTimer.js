import { useState, useRef, useEffect } from 'react';

export const useTimer = ({ defaultSeconds = 0, defaultStarted = false }) => {
  const [secondsPassed, setSecondsPassed] = useState(defaultSeconds);
  const [started, setStarted] = useState(defaultStarted);

  const useInterval = (callback, delay) => {
    const savedCallback = useRef();

    useEffect(() => {
      savedCallback.current = callback;
    });

    useEffect(() => {
      const tick = () => {
        savedCallback.current();
      };

      if (delay !== null && started) {
        const id = setInterval(tick, delay);

        return () => clearInterval(id);
      }
    }, [delay, started]);
  };

  useInterval(() => {
    setSecondsPassed(secondsPassed + 1);
  }, 1000);

  const startTimer = (seconds) => {
    setSecondsPassed(seconds);
    setStarted(true);
  };

  const stopTimer = () => {
    setStarted(false);
  };

  const setSeconds = (seconds) => {
    setSecondsPassed(seconds);
  };

  return [secondsPassed, { startTimer, stopTimer, setSeconds }];
};
