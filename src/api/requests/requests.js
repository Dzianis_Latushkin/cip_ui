import { api } from 'api/utils/api';

//========================================= USER ========================================= //

export const authUser = async () => {
  return await api.get('/users/login-by-token');
};

export const getUsers = async () => {
  return await api.get('/users/my');
};

export const getUser = async (email) => {
  return await api.get(`/users/one?email=${email}`);
};

export const loginUser = async (body) => {
  return await api.post('/users/login', body);
};

export const addUser = async (data) => {
  return await api.post('/users', data);
};

export const editEmployee = async (id, data) => {
  return await api.put(`/users/${id}`, data);
};

export const changeUserPassword = async (id, data) => {
  return await api.patch('/users/password', data);
};

export const deleteUser = async (id) => {
  return await api.patch(`/users/mark-deleted/${id}`);
};

export const removeEmployee = async (id) => {
  return await api.patch(`/users/remove/${id}`);
};

//========================================= FOLDER ========================================= //

export const getAllFolders = async () => {
  return await api.get('/folders');
};

export const addFolder = async (data) => {
  return await api.post('/folders', data);
};

export const editFolder = async (id, data) => {
  return await api.put(`/folders/${id}`, data);
};

export const getOneFolder = async (id) => {
  return await api.get(`/folders/${id}`);
};

export const deleteFolder = async (id) => {
  return await api.delete(`/folders/${id}`);
};

//========================================= QUESTION ========================================= //

export const getAllQuestions = async () => {
  return await api.get('/questions');
};

export const addQuestion = async (data) => {
  return await api.post('/questions', data);
};

export const editQuestion = async (id, data) => {
  return await api.put(`/questions/${id}`, data);
};

export const getOneQuestions = async (id) => {
  return await api.get(`/questions/${id}`);
};

export const deleteQuestion = async (id) => {
  return await api.delete(`/questions/${id}`);
};

//========================================= INTERVIEW ========================================= //

export const getManagersInterviews = async (status) => {
  return await api.get(`/interviews/my${status}`);
};

export const getOneInterview = async ({ id, status }) => {
  return await api.get(`/interviews/my/${id}${status ? status : ''}`);
};

export const createInterview = async (data) => {
  return await api.post('/interviews', data);
};

export const interviewOrderUpdate = async (id, data) => {
  return await api.patch(`/interviews/${id}/order`, data);
};

export const editInterview = async (id, data) => {
  return await api.put(`/interviews/${id}`, data);
};

export const deleteInterview = async (id) => {
  return await api.delete(`/interviews/${id}`);
};

//========================================= FEEDBACK ========================================= //

export const getFeedback = async (id) => {
  return await api.get(`/feedbacks/${id}`);
};

export const createFeedback = async (data) => {
  return await api.post('/feedbacks', data);
};

export const getFeedbackList = async (id) => {
  return await api.get(`/feedbacks?employeeId=${id}`);
};

export const updateFeedback = async (id, data) => {
  return await api.put(`/feedbacks/${id}`, data);
};

//========================================= REPORT ========================================= //

export const postReport = async (data) => {
  return await api.post('/reports', data);
};

//========================================= PROJECTS ========================================= //

export const addProject = async (data) => {
  return await api.post('/projects', data);
};

export const getProjects = async () => {
  return await api.get('/projects?sort=newest&limit=100000');
};

export const editProject = async (id, data) => {
  return await api.put(`/projects/${id}`, data);
};

export const deleteProject = async (id) => {
  return await api.patch(`/projects/${id}`);
};

export const getProjectsHistory = async ({ page, searchValue = '' }) => {
  return await api.get(
    `/projects/?limit=100&page=${page}&sort=interviews&titleSearch=${searchValue}`,
  );
};

//========================================= PROJECT INTERVIEWS ========================================= //

export const getProjectInterviews = async () => {
  return await api.get('/projectInterviews/employees/all');
};

export const getMyProjectInterviews = async () => {
  return await api.get('/projectInterviews');
};

export const getAllProjectInterviews = async (id, page, statuses = '') => {
  return await api.get(
    `/projectInterviews/project/${id}?limit=20&page=${page}${statuses}`,
  );
};

export const postProjectInterviews = async (data) => {
  return await api.post('/projectInterviews', data);
};

export const getProjectInterviewOne = async ({ id }) => {
  return await api.get(`/projectInterviews/${id}`);
};

export const getEmployeeProjectInterviewOne = async ({ id }) => {
  return await api.get(`/projectInterviews/employee/${id}`);
};

export const deleteProjectInterviews = async (id) => {
  return await api.delete(`/projectInterviews/${id}`);
};

export const postTrophyProjectInterviews = async (id) => {
  return await api.post(`/projectInterviews/${id}`);
};

export const putEditProjectInterview = async (id, data) => {
  return await api.put(`/projectInterviews/${id}`, data);
};
