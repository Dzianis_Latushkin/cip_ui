import axios from 'axios';
import { API_URL } from 'constants/env.js';

import { errorsHandler } from './errorsHandler';

export const api = axios.create({
  baseURL: API_URL,
});

const checkTokenInterceptor = (request) => {
  const token = localStorage.getItem('cip_token');
  if (token) {
    request.headers.Authorization = token;
  }

  return request;
};

const response = (res) => res;

const checkErrors = (err) => {
  console.log('err :', errorsHandler(err));

  return Promise.reject(errorsHandler(err));
};

api.interceptors.request.use(checkTokenInterceptor);
api.interceptors.response.use(response, checkErrors);
