import i18next from 'i18next';
import { getErrors } from 'constants/errors';

export const errorsHandler = (err) => {
  if (err.response) {
    const error = err.response.data.code;
    const errorMessage = err.response.data.message;

    return getErrors(i18next.t)[error]
      ? getErrors(i18next.t)[error]
      : errorMessage
      ? errorMessage
      : i18next.t('errorsHandler:unknownError');
  } else if (err.request) {
    const online = window.navigator.onLine;
    if (!online) {
      return i18next.t('errorsHandler:internetConnectionError');
    }

    return i18next.t('errorsHandler:noResponseError');
  } else {
    return i18next.t('errorsHandler:unknownError');
  }
};
