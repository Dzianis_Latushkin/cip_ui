/**
 * MODULES
 */
import webpack from 'webpack';
import path from 'path';
import chalk from 'chalk';
import ProgressBarPlugin from 'progress-bar-webpack-plugin';
import HtmlWebpackPlugin from 'html-webpack-plugin';
import { CleanWebpackPlugin } from 'clean-webpack-plugin';
import MiniCssExtractPlugin from 'mini-css-extract-plugin';
import DotenvWebpackPlugin from 'dotenv-webpack';
import CaseSensitivePathsPlugin from 'case-sensitive-paths-webpack-plugin';
import { ESBuildMinifyPlugin } from 'esbuild-loader';
import FaviconsWebpackPlugin from 'favicons-webpack-plugin';
import CopyPlugin from 'copy-webpack-plugin';

const isEnvDevelopment = process.env.NODE_ENV === 'development';
const isEnvProduction = process.env.NODE_ENV === 'production';

const shouldUseSourceMap = process.env.GENERATE_SOURCEMAP !== 'false';

const imageInlineSizeLimit = parseInt(
  process.env.IMAGE_INLINE_SIZE_LIMIT || '10000',
  10,
);

const getStyleLoaders = (cssOptions, preProcessor) => {
  const loaders = [
    isEnvDevelopment && 'style-loader',
    isEnvProduction && {
      loader: MiniCssExtractPlugin.loader,
    },
    {
      loader: 'css-loader',
      options: cssOptions,
    },
    {
      loader: 'postcss-loader',
      options: {
        postcssOptions: {
          ident: 'postcss',
        },
        sourceMap: isEnvProduction && shouldUseSourceMap,
      },
    },
  ].filter(Boolean);

  if (preProcessor) {
    loaders.push(
      {
        loader: 'resolve-url-loader',
        options: {
          sourceMap: isEnvProduction && shouldUseSourceMap,
        },
      },
      {
        loader: preProcessor,
        options: {
          sourceMap: true,
        },
      },
    );
  }

  return loaders;
};

/**
 * CONFIG
 * Reference: https://webpack.js.org/configuration/
 * This is the object where all configuration gets set
 */
const config = {};

config.target = 'web';

/**
 * MODE
 * Reference: https://webpack.js.org/configuration/mode/
 * Providing the mode configuration option tells webpack
 * to use its built-in optimizations accordingly.
 */
config.mode = isEnvProduction
  ? 'production'
  : isEnvDevelopment && 'development';

/**
 * DEVTOOL
 * Reference: https://webpack.js.org/configuration/devtool/
 * Type of sourcemap to use per build type
 */
if (isEnvDevelopment) {
  config.devtool = 'cheap-module-source-map';
} else {
  config.devtool = 'source-map';
}

/**
 * RESOLVE
 * Reference: https://webpack.js.org/configuration/resolve/#resolve
 * Configure how modules are resolved.
 */
config.resolve = {
  modules: ['src', 'node_modules'],
  extensions: ['.js', '.jsx', '.json', '.ts'],
  alias: {
    components: path.resolve(__dirname, './src/components'),
    assets: path.resolve(__dirname, './src/assets'),
    store: path.resolve(__dirname, './src/store'),
    constants: path.resolve(__dirname, './src/constants'),
    hooks: path.resolve(__dirname, './src/hooks'),
    context: path.resolve(__dirname, './src/context'),
    api: path.resolve(__dirname, './src/api'),
    reducers: path.resolve(__dirname, './src/reducers'),
    helpers: path.resolve(__dirname, './src/helpers'),
  },
};

/**
 * ENTRY
 * Reference: https://webpack.js.org/configuration/entry-context/
 * Entry point of the application
 */
config.entry = {
  main: path.resolve(__dirname, './src/index'),
};

/**
 * OUTPUT
 * Reference: https://webpack.js.org/configuration/output/
 * Output files of the application
 */
config.output = {
  // Absolute output directory
  path: path.resolve(__dirname, './build'),

  // Output path from the view of the page
  // Uses webpack-dev-server in development
  publicPath: '/',

  // Filename for entry points
  // Only adds hash in production mode
  filename: isEnvDevelopment
    ? 'static/js/[name].js'
    : 'static/js/[name].[chunkhash].js',

  // There are also additional JS chunk
  // files if you use code splitting.
  chunkFilename: isEnvDevelopment
    ? 'static/js/[name].chunk.js'
    : 'static/js/[name].[chunkhash].chunk.js',
};

/**
 * LOADERS
 * Reference: https://webpack.js.org/concepts/loaders/
 * This handles most of the magic responsible for converting modules
 */
config.module = {
  rules: [
    {
      // ESLINT LOADER
      // Reference: https://github.com/MoOx/eslint-loader
      // Check js files for code errors
      enforce: 'pre',
      test: /\.js$/,
      loader: 'eslint-loader',
      include: path.resolve(__dirname, './src'),
      options: {
        cache: true,
        fix: true,
      },
    },
    {
      // BABEL LOADER
      // Reference: https://github.com/babel/babel-loader
      // Transpile .js files using babel-loader
      // Compiles ES6 and ES7 into ES5 code
      test: /\.js$/,
      loader: 'esbuild-loader',
      include: path.resolve(__dirname, './src'),
      options: {
        loader: 'jsx',
      },
    },
    {
      // CSS LOADER
      // Reference: https://github.com/webpack-contrib/css-loader
      // Allow loading css through js
      //
      // POSTCSS LOADER
      // Reference: https://github.com/postcss/postcss-loader
      // Postprocess your css with PostCSS plugins
      //
      // STYLE LOADER
      // Reference: https://github.com/webpack-contrib/style-loader
      // Adds CSS to the DOM by injecting a <style> tag
      //
      // SASS LOADER
      // Reference: https://github.com/jtangelder/sass-loader
      // Compiles Sass to CSS
      test: /\.scss$/,
      use: getStyleLoaders(
        {
          importLoaders: 3,
          sourceMap: isEnvProduction && shouldUseSourceMap,
          modules: {
            localIdentName: isEnvProduction
              ? '[hash:base64:5]'
              : '[name]-[local]--[hash:base64:5]',
          },
        },
        'sass-loader',
      ),
    },
    {
      // CSS LOADER
      // Reference: https://github.com/webpack-contrib/css-loader
      // Allow loading css through js
      test: /\.css$/,
      use: getStyleLoaders({
        importLoaders: 1,
        sourceMap: isEnvProduction && shouldUseSourceMap,
      }),
    },
    {
      // SVGR LOADER
      // Reference: https://github.com/smooth-code/svgr
      // SVGR transforms SVG into ready to use components.
      test: /.svg$/,
      use: [
        {
          loader: '@svgr/webpack',
          options: {
            svgoConfig: {
              plugins: {
                removeViewBox: false,
              },
            },
          },
        },
      ],
    },
    {
      test: [/\.bmp$/, /\.gif$/, /\.jpe?g$/, /\.png$/],
      type: 'asset',
      parser: {
        dataUrlCondition: {
          maxSize: imageInlineSizeLimit,
        },
      },
    },
    {
      exclude: [
        /\.(js|jsx)$/,
        /\.scss$/,
        /\.html$/,
        /\.json$/,
        /\.css$/,
        /\.svg$/,
      ],
      type: 'asset',
    },
  ],
};

config.devServer = {
  client: {
    overlay: false, //show compile warnings
  },
  historyApiFallback: true,
  hot: true,
  open: true,
  compress: true,
  port: 3000,
};

/**
 * Plugins
 * Reference: https://webpack.js.org/configuration/plugins/
 */
config.plugins = [];

config.plugins.push(new FaviconsWebpackPlugin('./src/assets/img/favicon.svg'));

// CopyWebpackPlugin
// Reference: https://webpack.js.org/plugins/copy-webpack-plugin/
// Copies individual files or entire directories, which already exist, to the build directory.
config.plugins.push(
  new CopyPlugin({
    patterns: [
      {
        from: path.resolve(__dirname, 'src/assets/icons'),
        to: path.resolve(__dirname, 'build/assets/icons'),
      },
    ],
  }),
);

// HTML WEBPACK PLUGIN
// Reference: https://github.com/ampedandwired/html-webpack-plugin
// Skip rendering index.html in test mode
config.plugins.push(
  new HtmlWebpackPlugin(
    Object.assign(
      {},
      { inject: true, template: './public/index.html' },
      isEnvProduction
        ? {
            minify: {
              removeComments: true,
              collapseWhitespace: true,
              removeRedundantAttributes: true,
              useShortDoctype: true,
              removeEmptyAttributes: true,
              removeStyleLinkTypeAttributes: true,
              keepClosingSlash: true,
              minifyJS: true,
              minifyCSS: true,
              minifyURLs: true,
            },
          }
        : undefined,
    ),
  ),
);

// CASE SENSITIVE PATHS WEBPACK PLUGIN
// Reference: https://github.com/Urthen/case-sensitive-paths-webpack-plugin
// This Webpack plugin enforces the entire path of all required modules match the exact case of the actual path on disk.
if (isEnvDevelopment) {
  config.plugins.push(new CaseSensitivePathsPlugin());
}

// MINI CSS EXTRACT WEBPACK PLUGIN
// Reference: https://github.com/webpack-contrib/mini-css-extract-plugin
// This plugin extracts CSS into separate files.
if (isEnvProduction) {
  config.plugins.push(
    new MiniCssExtractPlugin({
      filename: 'static/styles/[name].[contenthash:8].css',
      chunkFilename: 'static/styles/[name].[contenthash:8].chunk.css',
    }),
  );
}

// DOTENV WEBPACK PLUGIN
// Reference: https://github.com/mrsteele/dotenv-webpack
// A secure webpack plugin that supports dotenv and other
// environment variables and only exposes what you choose and use.
config.plugins.push(
  new DotenvWebpackPlugin({
    path: process.env.ENVFILE,
  }),
);

// CLEAN WEBPACK PLUGIN
// Reference: https://github.com/johnagan/clean-webpack-plugin
// A webpack plugin to remove/clean your build folder(s).
config.plugins.push(new CleanWebpackPlugin());

// PROGRESS BAR WEBPACK PLUGIN
// Reference: https://github.com/clessg/progress-bar-webpack-plugin
// Add progress bar to webpack build
config.plugins.push(
  new ProgressBarPlugin({
    format: `build [:bar] ${chalk.green.bold(':percent')} (:elapsed seconds)`,
    clear: false,
  }),
);

config.plugins.push(new webpack.ids.HashedModuleIdsPlugin());

config.optimization = {
  runtimeChunk: 'single',
  splitChunks: {
    chunks: 'all',
    maxInitialRequests: Infinity,
    minSize: 0,
    cacheGroups: {
      vendor: {
        test: /[\\/]node_modules[\\/]/,
        name(module) {
          const packageContext = module.context.match(
            /[\\/]node_modules[\\/](.*?)([\\/]|$)/,
          );

          if (packageContext && packageContext[1]) {
            const packageName = packageContext[1];

            return `npm.${packageName.replace('@', '')}`;
          }

          return null;
        },
      },
    },
  },
  minimize: isEnvProduction,
  minimizer: [new ESBuildMinifyPlugin({ css: true })],
};

export default config;
