module.exports = {
  modulePaths: ['src'],
  moduleDirectories: ['node_modules'],
  verbose: true,
  clearMocks: true,
  collectCoverage: true,
  setupFilesAfterEnv: ['<rootDir>/.jest/setupTest.js'],
  setupFiles: ['<rootDir>/.jest/register-context.js'],
  snapshotSerializers: ['./node_modules/enzyme-to-json/serializer'],
  transform: {
    '^.+\\.js$': 'babel-jest',
  },
  moduleNameMapper: {
    '\\.(jpg|jpeg|png|gif|eot|otf|webp|svg|ttf|woff|woff2|mp4|webm|wav|mp3|m4a|aac|oga)$':
      '<rootDir>/__mocks__/fileMock.js',
    '\\.(css|scss)$': 'identity-obj-proxy',
    '^@components(.*)$': '<rootDir>/src/components',
    '^@assets(.*)$': '<rootDir>/src/assets',
    '^@store(.*)$': '<rootDir>/src/store',
    '^@constants(.*)$': '<rootDir>/src/constants',
    '^@hooks(.*)$': '<rootDir>/src/hooks',
    '^@context(.*)$': '<rootDir>/src/context',
    '^@api(.*)$': '<rootDir>/src/api',
    '^@reducers(.*)$': '<rootDir>/src/reducers',
    '^@helpers(.*)$': '<rootDir>/src/helpers',
  },
};
